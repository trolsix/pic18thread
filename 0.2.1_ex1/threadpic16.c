/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/
/*-----------------------------------------------

 pic18**** thread

-----------------------------------------------*/

#ifdef __SDCC_pic16	
#include <pic16/pic18fregs.h>
#endif

#include <stdint.h>

#include "Bheadb.h"

/*-----------------------------------------------*/

#include "threadpic16stckconf.h"
#include "threadpic16conf.h"
#include "threadpic16.h"

/*-----------------------------------------------*/

extern uint16_t timer[2];
void startthvoid1end(void);

/*-----------------------------------------------*/

#if THREAD_USE_LOW_PRIORITY==0
void  tch_int (void) __interrupt 1 __naked {
#else
void  tch_int (void) __interrupt 2 __naked {
#endif
	__asm
	goto _tch_int_jump
	__endasm;
}

/*-----------------------------------------------*/
/* for space placed this into flash in low cost version*/

#if STACKADR_IN_FLASH == 0
#error config STACKADR_IN_FLASH - now stack can be only in flash
#if _THREADNUM==2
uint16_t thstckadr[_THREADNUM] =  { THSTACADR00, THSTACADR01 };
#endif
#if _THREADNUM==3
uint16_t thstckadr[_THREADNUM] =  { THSTACADR00, THSTACADR01, THSTACADR02 };
#endif
#else
uint8_t thstckadr;
#endif

//uint8_t thnumregsave[_THREADNUM];
//uint8_t thdump[_THREADNUM];
//uint8_t threturn[_THREADNUM];
//uint8_t thnumber;

#if _THREADNUM<2
static uint8_t thdump00[DNMPLACES01];
#endif
#if _THREADNUM==2
static uint8_t thdump00[DNMPLACES02];
#endif
#if _THREADNUM==3
static uint8_t thdump00[DNMPLACES03];
#endif
#if _THREADNUM==4
static uint8_t thdump00[DNMPLACES04];
#endif
#if _THREADNUM==5
static uint8_t thdump00[DNMPLACES05];
#endif

static uint8_t thactiv;
static uint8_t thstate[_THREADNUM];
static uint8_t thflag;

/*
0 irq semaphores
1 SAVE or RESTORE in irq
6 copy intcon state
7 copy intcon state
*/

//static uint8_t stksav;
static uint8_t registcount;

#if STACK_FOR_IRQ == 1
static uint16_t save_point_stack;
#endif

/*-----------------------------------------------*/

static uint8_t setset ( void ) __naked;
void RSoutP (unsigned char a) _MYMODFUN_;

/*-----------------------------------------------*/
/*
	testt with stack for irqh
*/
void  tch_int_jump (void) __naked {

	__asm
	#if STACK_FOR_IRQ == 1
		bcf _INTCON,7
		nop
		bcf _T1CON,0
		MOVFF	_TMR1L, _timer
		MOVFF	_TMR1H, _timer+1
		bsf _T1CON,0
	#endif
		MOVFF	_STATUS, _POSTDEC1
		MOVFF	_BSR, _POSTDEC1
		MOVWF	_POSTDEC1
		MOVFF	_FSR2L, _POSTDEC1
		MOVFF	_FSR2H, _POSTDEC1
		MOVFF	_FSR0L, _POSTDEC1
		MOVFF	_FSR0H, _POSTDEC1
		MOVFF	_PRODL, _POSTDEC1
		MOVFF	_PRODH, _POSTDEC1
			
	#if STACK_FOR_IRQ == 1
		;beckup pointer
		MOVFF	_FSR1L, _save_point_stack
		MOVFF	_FSR1H, _save_point_stack+1
		;set new
		MOVLW LOW(THSTACADRIH)
		movwf _FSR1L
		MOVLW HIGH(THSTACADRIH)
		movwf _FSR1H
		bsf _INTCON,7
		
	#endif
	__endasm;


	/* time for threads */
	if(INTCONbits.TMR0IF) {
		INTCONbits.TMR0IF = 0;
		
		/* save kontest */
		__asm
			;BANKSEL _thactiv
			movff _thactiv, _PRODH
		
			call _setset ;W <- NUMPARAM00
			BANKSEL _registcount
			;sublw 9
			addlw (256-9)
			movwf _registcount

			bcf _INTCON,7
			nop
			MOVFF _STKPTR, _POSTINC2
		#if STACK_FOR_IRQ == 0
			MOVFF _FSR1L, _POSTINC2
			MOVFF _FSR1H, _POSTINC2
		#endif
			bsf _INTCON,7
		
		
		#if STACK_FOR_IRQ == 1
			MOVFF _save_point_stack, _POSTINC2
			MOVFF _save_point_stack+1, _POSTINC2
		#endif
			MOVFF _PCLATH, _POSTINC2
			MOVFF _PCLATU, _POSTINC2
		#if SAVE_TBLCONT==1
			MOVFF _TABLAT, _POSTINC2
			MOVFF _TBLPTRL, _POSTINC2
			MOVFF _TBLPTRH, _POSTINC2
			MOVFF _TBLPTRU, _POSTINC2
		#endif

			clrf _FSR0L
			clrf _FSR0H
		#if SPEEDUPSAVE>1
			rrncf _registcount, F, B
		#endif
		#if SPEEDUPSAVE>2
			rrncf _registcount, F, B
		#endif
		#if SPEEDUPSAVE>4
			rrncf _registcount, F, B
		#endif

;loop 5*16=80 7*8=56 11*4=44 19*2=38
			
			movf _registcount,F,B
			bz save_reg_loop_10
			save_reg_loop:
			MOVFF	_POSTINC0, _POSTINC2
		#if SPEEDUPSAVE>1
			MOVFF	_POSTINC0, _POSTINC2
		#endif
		#if SPEEDUPSAVE>2
			MOVFF	_POSTINC0, _POSTINC2
			MOVFF	_POSTINC0, _POSTINC2
		#endif
		#if SPEEDUPSAVE>4
			MOVFF	_POSTINC0, _POSTINC2
			MOVFF	_POSTINC0, _POSTINC2
			MOVFF	_POSTINC0, _POSTINC2
			MOVFF	_POSTINC0, _POSTINC2
		#endif
			decfsz _registcount,F,B
			goto save_reg_loop
			
			save_reg_loop_10:			
			clrwdt
			
		__endasm;

		while(1){
			if ( ++thactiv >= _THREADNUM ) {
				thactiv = 0;
				break;
			}
			if ( thstate[thactiv] == THST_RUN ) break;
		}
		thflag |= 0x02;
	}
	

	if(PIR1bits.TMR1IF) {
		PIR1bits.TMR1IF=0;
	}
	
	/* ****************************************************** */
	
#if FUN_IRQ_THREAD_ALLOWED==1	

	/* in this place you can past call void function, example:

	switch_test();

	*/






	
	
	
#endif

	/* ****************************************************** */	

	if( thflag & 0x02 ) {
		thflag &= ~0x02;
	
	/* restore kontest */
	__asm
		movff _thactiv, _PRODH
		call _setset
		BANKSEL _registcount
		;sublw 9
		addlw (256-9)
		movwf _registcount
	__endasm;


#if THREAD_USE_LOW_PRIORITY==1
	__asm
		bcf _INTCON,7
		nop
	__endasm;
	#endif
#if STACK_FOR_IRQ == 0
	__asm
		MOVFF _POSTINC2, _STKPTR
		MOVFF _POSTINC2, _FSR1L
		MOVFF _POSTINC2, _FSR1H
	__endasm;
#endif
#if STACK_FOR_IRQ == 1
	__asm
		MOVFF _POSTINC2, _STKPTR
		MOVFF _POSTINC2, _save_point_stack
		MOVFF _POSTINC2, _save_point_stack+1
	__endasm;
#endif	
#if THREAD_USE_LOW_PRIORITY==1
	__asm
		bsf _INTCON,7
	__endasm;
#endif

	__asm	
		MOVFF _POSTINC2, _PCLATH
		MOVFF _POSTINC2, _PCLATU
		
	#if SAVE_TBLCONT==1
		MOVFF _POSTINC2, _TABLAT
		MOVFF _POSTINC2, _TBLPTRL
		MOVFF _POSTINC2, _TBLPTRH
		MOVFF _POSTINC2, _TBLPTRU
	#endif
		
		clrf _FSR0L
		clrf _FSR0H
		#if SPEEDUPSAVE>1
			rrncf _registcount, F, B
		#endif
		#if SPEEDUPSAVE>2
			rrncf _registcount, F, B
		#endif
		#if SPEEDUPSAVE>4
			rrncf _registcount, F, B
		#endif
		
;loop 5*16=80 7*8+1=57 11*4+2=46 19*2+3=41
			movf _registcount,F,B
			bz restore_reg_loop_10
			restore_reg_loop:
			MOVFF	_POSTINC2, _POSTINC0
		#if SPEEDUPSAVE>1
			MOVFF	_POSTINC2, _POSTINC0
		#endif
		#if SPEEDUPSAVE>2
			MOVFF	_POSTINC2, _POSTINC0
			MOVFF	_POSTINC2, _POSTINC0
		#endif
		#if SPEEDUPSAVE>4
			MOVFF	_POSTINC2, _POSTINC0
			MOVFF	_POSTINC2, _POSTINC0
			MOVFF	_POSTINC2, _POSTINC0
			MOVFF	_POSTINC2, _POSTINC0
		#endif
			decfsz _registcount,F
			goto restore_reg_loop
			restore_reg_loop_10:
			
	__endasm;
	}
	

	__asm
	#if STACK_FOR_IRQ==1 && THREAD_USE_LOW_PRIORITY==1
		bcf _INTCON,7, A
		nop
		;restore stack
		MOVFF _save_point_stack, _FSR1L
		MOVFF _save_point_stack+1, _FSR1H
	#endif
	
		MOVFF _PREINC1, _PRODH
		MOVFF _PREINC1, _PRODL
		MOVFF _PREINC1, _FSR0H
		MOVFF _PREINC1, _FSR0L
		MOVFF _PREINC1, _FSR2H
		MOVFF _PREINC1, _FSR2L
		MOVF  _PREINC1, W
		MOVFF _PREINC1, _BSR
		MOVFF _PREINC1, _STATUS
		
		bcf _T1CON,0
		MOVFF	_TMR1L, _timer+2
		MOVFF	_TMR1H, _timer+3
		bsf _T1CON,0
		
	#if STACK_FOR_IRQ==1 && THREAD_USE_LOW_PRIORITY==1
		bsf _INTCON,7
	#endif
	
		RETFIE
	__endasm;
}

/*-----------------------------------------------*/

void initth ( void ) {

	thactiv = 0;
	thflag = 0;
	
	thstate[0] = 0xFF;
#if _THREADNUM>1
	thstate[1] = 0xFF;
#endif
#if _THREADNUM>2
	thstate[2] = 0xFF;
#endif
#if _THREADNUM>3
	thstate[3] = 0xFF;
#endif
#if _THREADNUM>4
	thstate[4] = 0xFF;
#endif
	
//timer 0, prescaler 8
//	T0CON = _TMR0ON | _T0PS1 | _T08BIT;
#if _PRESCALER_T0 == 16
//timer 0, prescaler 16 irqcykl 4096 588 dla 2MHz
	T0CON = _TMR0ON | _T0PS1 | _T0PS0 | _T08BIT;
#endif
#if _PRESCALER_T0 == 32
//timer 0, prescaler 32 irqcykl 8192 244 dla 2MHz
	T0CON = _TMR0ON | _T0PS2 | _T08BIT;	
#endif
#if _PRESCALER_T0 == 64
//timer 0, prescaler 64 irqcykl 16384 122 dla 2MHz
	T0CON = _TMR0ON | _T0PS2 | _T0PS0 | _T08BIT;
#endif
#if _PRESCALER_T0 == 128
//timer 0, prescaler 128 irqcykl 32k 60 dla 2MHz
	T0CON = _TMR0ON | _T0PS2 | _T0PS1 | _T08BIT;
#endif
#if _PRESCALER_T0 == 256
//timer 0, prescaler 256 irqcykl 64k 30 dla 2MHz
	T0CON = _TMR0ON | _T0PS2 | _T0PS1 | _T0PS0 | _T08BIT;
#endif

	INTCONbits.TMR0IE = 1;
#if THREAD_USE_LOW_PRIORITY==1
	INTCON2bits.TMR0IP = 0; //low prioryty
#endif
}

/*-----------------------------------------------*/

uint8_t threadstop ( uint8_t nth ) _MYMODFUN_ {
	if ( nth > _THREADNUM ) return 0xFF;
	if ( nth == 0 ) return 0xFF;
	/* thread with bit 7 are not change */
	if ( thstate[nth] & 0x80 ) return 0xFF;
	thstate[nth] = THST_STOP;
	return 0;
}


uint8_t threadrun ( uint8_t nth ) _MYMODFUN_ __naked {
	
	nth;
	
	__asm
		movwf _FSR0L
		clrf _FSR0H, A
		movlw _THREADNUM
		SUBWF	FSR0L, W, A
		bnc thrun_010
		thrun_008:
		SETF	_WREG
		return
		
		thrun_010:
		MOVLW LOW(_thstate)
		ADDWF _FSR0L, F, A
		MOVLW HIGH(_thstate)
		ADDWFC _FSR0H, F, A
		btfsc _INDF0, 7, A
		goto thrun_008
		
		MOVLW THST_RUN
		MOVWF INDF0
		CLRF WREG
		return
	__endasm;
}

/*
uint8_t threadrun ( uint8_t nth ) _MYMODFUN_ {
	if ( nth > _THREADNUM ) return 0xFF;
	// thread with bit 7 are not for run
	if ( thstate[nth] & 0x80 ) return 0xFF;
	thstate[nth] = THST_RUN;
	return 0;
}*/

/*-----------------------------------------------*/

void irq_on ( void ) {
#if THREAD_USE_LOW_PRIORITY==1
	RCONbits.IPEN = 1;
#endif
	__asm
		bsf _INTCON,6,A
		bsf _INTCON,7,A
	__endasm;
}

/*-----------------------------------------------*/

void thstop_endfun ( void ) __naked {

	/* return value */
	__asm
;		MOVWF _POSTDEC1
;		MOVLW	LOW(_threturn)
;		MOVWF	_FSR2L
;		MOVLW	HIGH(_threturn)
;		MOVWF	_FSR2H
;		BANKSEL	_thactiv
;		MOVF	_thactiv, W, B
;		ADDWF _FSR2L,F
;		MOVLW 0
;		ADDWFC _FSR2H,F
;		MOVFF _PREINC1,_INDF2
	__endasm;
	
	/* set state stop */
	__asm
		MOVLW	LOW(_thstate)
		MOVWF	_FSR2L
		MOVLW	HIGH(_thstate)
		MOVWF	_FSR2H
		BANKSEL	_thactiv ;number thread activ
		MOVF	_thactiv, W, B
		ADDWF _FSR2L,F,A
		CLRW
		ADDWFC _FSR2H,F
		MOVLW THST_END
		MOVWF _INDF2
	__endasm;

	END_TH_FUN();
}

/*-----------------------------------------------*/

void irq_off ( void ) __naked {
	__asm
;version with stack
;		movff _INTCON, _POSTDEC1
;		bcf _INTCON,7,A
;		bcf _INTCON,6,A
	
;		BANKSEL _thflag
;		movf _PREINC1, F
;		bsf _thflag,6,B
;		bsf _thflag,7,B
		
;		btfss _INDF1,7,A
;		bcf _thflag,7,B
;		btfss _INDF1,6,A
;		bcf _thflag,6,B

;		RETURN
	__endasm;
	
	__asm
;version with PRODL
		movff _INTCON, _PRODL
		bcf _INTCON,7,A
		bcf _INTCON,6,A
		
		BANKSEL _thflag
		bsf _thflag,6,B
		bsf _thflag,7,B
		
		btfss _PRODL,7,A
		bcf _thflag,7,B
		btfss _PRODL,6,A
		bcf _thflag,6,B

		RETURN
	__endasm;
}

/*-----------------------------------------------*/

void irq_restore ( void ) __naked {
	__asm
		BANKSEL _thflag
		btfsc _thflag,6,B
		bsf _INTCON,6,A
		btfsc _thflag,7,B
		bsf _INTCON,7,A
		RETURN
	__endasm;
}

/*-----------------------------------------------*/

uint8_t setset ( void ) __naked {
	/* this destroy PRODH */
	/* get number bytes to save in WREG */
	__asm
		MOVLW	LOW(_thdump00) ;dumb adres
		MOVWF _FSR2L
		MOVLW	HIGH(_thdump00)
		MOVWF _FSR2H
		MOVLW	LOW(NUMPARAM00)
		movf _PRODH, F
		bz FINDEND

#if _THREADNUM>1
	FIND01:
		;MOVLW LOW(NUMPARAM00)
		ADDWF _FSR2L, F
		MOVLW	HIGH(NUMPARAM00)
		ADDWFC _FSR2H, F
		MOVLW	LOW(NUMPARAM01)
		decf _PRODH, F
		bz FINDEND
#endif
#if _THREADNUM>2
	FIND02:
		;MOVLW LOW(NUMPARAM01)
		ADDWF _FSR2L, F
		MOVLW	HIGH(NUMPARAM01)
		ADDWFC _FSR2H, F
		MOVLW	LOW(NUMPARAM02)
		decf _PRODH, F
		bz FINDEND
#endif
#if _THREADNUM>3
	FIND03:
		;MOVLW LOW(NUMPARAM02)
		ADDWF _FSR2L, F
		MOVLW	HIGH(NUMPARAM02)
		ADDWFC _FSR2H, F
		MOVLW	LOW(NUMPARAM03)
		decf _PRODH, F
		bz FINDEND
#endif
#if _THREADNUM>4
	FIND04:
		;MOVLW LOW(NUMPARAM03)
		ADDWF _FSR2L, F
		MOVLW	HIGH(NUMPARAM03)
		ADDWFC _FSR2H, F
		MOVLW	LOW(NUMPARAM04)
		decf _PRODH, F, A
		bz FINDEND
#endif

	FINDEND:
	return
	__endasm;
}


uint8_t startthvoidnum ( void ) __naked {

	__asm
	extern _startthvoid1end
	
	;movlw 5
	;movff _PLUSW2,_PRODH ;number thread
	
	;other way but count FSR
	movlw 6
	movff _PLUSW1,_PRODH ;number thread

	clrw
	cpfsgt _PRODH, A   ;if F>W skip
	goto TH_DONT_START ;dont start 0 or create new thread
	decf _PRODH, F, A
	bz thst1
	decf _PRODH, F, A
	bz thst2
	decf _PRODH, F, A
	bz thst3
	decf _PRODH, F, A
	bz thst4

	TH_DONT_START:
	SETF	_WREG
	goto startthvoidnumend
	
#if _THREADNUM>1
	thst1:
	MOVLW	LOW(THSTACADR01) ;software stack
	MOVWF _FSR0L
	MOVLW	HIGH(THSTACADR01) ;if stack in flash
	MOVWF _FSR0H
	MOVLW	THSTACKPOINT01 ;hardware stack
	goto stackisset
#endif
	thst2:
#if _THREADNUM>2
	MOVLW	LOW(THSTACADR02)
	MOVWF _FSR0L
	MOVLW	HIGH(THSTACADR02)
	MOVWF _FSR0H
	MOVLW	THSTACKPOINT02
	goto stackisset
#endif
	thst3:
#if _THREADNUM>3
	MOVLW	LOW(THSTACADR03)
	MOVWF _FSR0L
	MOVLW	HIGH(THSTACADR03)
	MOVWF _FSR0H
	MOVLW	THSTACKPOINT03
	goto stackisset
#endif
	thst4:
#if _THREADNUM>4
	MOVLW	LOW(THSTACADR04)
	MOVWF _FSR0L
	MOVLW	HIGH(THSTACADR04)
	MOVWF _FSR0H
	MOVLW	THSTACKPOINT04
#endif


	stackisset:
	call _irq_off
	MOVFF	_STKPTR, _POSTDEC1
	MOVWF	_STKPTR

;pop return adres
	MOVLW	LOW(_thstop_endfun)
	MOVWF	_TOSL
	MOVLW	HIGH(_thstop_endfun)
	MOVWF	_TOSH
	MOVLW	UPPER(_thstop_endfun)
	MOVWF	_TOSU
	INCF	_STKPTR, F
	
;pinter tu fun
	movf _PREINC2,W
	movf _PREINC2,W
	movwf _TOSL
	movf _PREINC2,W
	movwf _TOSH
	movf _PREINC2,W
	movwf _TOSU
	movff _PREINC2,_PRODH ;number thread

;save thnumber
	MOVFF _STKPTR, _PRODL ;remerber stack
	MOVFF	_PREINC1, _STKPTR
	call _irq_restore ;return from set stack
	
;set FSR2
	call _setset

;pop hardware stack
	MOVFF _PRODL, _POSTINC2
	
;pop point to stack and frame point high
	MOVLW 9
	SUBWF _FSR0L,F
	;clrw
	;SUBWFB _FSR0H,F ;dont need if stack and baund size 256
	MOVFF _FSR0L, _POSTINC2
	MOVFF _FSR0H, _INDF2
	MOVLW	5
	MOVFF _FSR0H, _PLUSW0

;state thnumber
	movlw 6
	movff _PLUSW1,  _FSR0L ;number thread
	;movff _PRODL, _FSR0L
	clrf _FSR0H
	MOVLW	LOW(_thstate)
	ADDWF _FSR0L, F
	MOVLW	HIGH(_thstate)
	ADDWFC _FSR0H, F
	movlw THST_RUN
	movwf _INDF0

	movlw 6
	movf _PLUSW1, W ;number thread

startthvoidnumend:
	goto _startthvoid1end
	__endasm;
	
}

/*-----------------------------------------------*/
