/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/
/*-----------------------------------------------
       pic18f1220 thread example
-----------------------------------------------*/

#include "Bheadb.h"

/*-----------------------------------------------*/

#include <stdio.h>
#include <stdint.h>

#include "threadpic16.h"
#include "threadpic16conf.h"

/*-----------------------------------------------*/
/*    config   fuse  hardware uC                 */
/*-----------------------------------------------*/

#include "Bconf.h"

/*-----------------------------------------------*/

void RSoutP (unsigned char a) _MYMODFUN_;
void CLRWDT(void);
void binbcd1 ( char * tbl, uint32_t a07 ) __reentrant;
void initport (void);
static void RSouttbl ( unsigned char siz, const unsigned char *tbl );

/*-----------------------------------------------*/

static char tbl[11];
volatile uint8_t thrend;
uint16_t timer[2];
uint16_t timthr[2];

/*-----------------------------------------------*/

volatile uint8_t wsrsbufin;
volatile uint8_t wsrsbufou;
volatile char rsbuf[16];
volatile static uint32_t tickf2;
static uint32_t tickf1;

/*-----------------------------------------------*/
/* example */

uint8_t myfun1 ( void ) {
	while(1) {
		if ( !( ++tickf1 & 0x0FFFF ) ) {
			INTCON &= ~0x40;
			RSoutP ('T');
			RSoutP ('1');
			RSoutP (':');
			if(tickf1&0x80000) RSoutP ('1');
			else RSoutP ('0');
			if(tickf1&0x40000) RSoutP ('1');
			else RSoutP ('0');
			if(tickf1&0x20000) RSoutP ('1');
			else RSoutP ('0');
			if(tickf1&0x10000) RSoutP ('1');
			else RSoutP ('0');
			RSoutP (' ');
			INTCON |= 0x40;
		}
		if ( tickf1&0x80000 ) {
			INTCON &= ~0x40;
			RSoutP ('T');
			RSoutP ('1');
			RSoutP (' ');
			RSoutP ('E');
			RSoutP ('N');
			RSoutP ('D');
			RSoutP ('\n');
			INTCON |= 0x40;
			break;
		}
	}
	return 1;
}

uint8_t myfun2 ( void ) {
	while(1){
		INTCON &= ~0x40;
		if ( !( ++tickf2 & 0xFFFF ) ) {
			RSoutP ('T');
			RSoutP ('2');
			RSoutP (' ');
		}
		INTCON |= 0x40;
		if(thrend) break;
	}
	return 2;
}

/*
if you don't called thread function from code
but only from thread, then can be naked
but be carefully how many register save in contest
*/
uint8_t myfun3 ( void ) __naked {
	while(1){
		INTCON &= ~0x40;
		if ( !( ++tickf2 & 0xFFFF ) ) {
			timthr[1] = (timer[1] - timer[0])<<1; //presc t1 x2
			RSoutP ('T');
			RSoutP ('3');
			RSoutP (' ');
		}
		INTCON |= 0x40;
		if(thrend) break;
	}
	
	//return 3;
	//for naked function return in asm
	__asm
		movlw 0x03
		return
	__endasm;
}

uint8_t myfun4 ( void ) {
	while(1){
		INTCON &= ~0x40;
		if ( !( ++tickf2 & 0xFFFF ) ) {
			RSoutP ('T');
			RSoutP ('4');
			RSoutP (' ');
		}
		INTCON |= 0x40;
		if(thrend) break;
	}
	return 3;
}


/*-----------------------------------------------*/

void RSoutP (unsigned char a) _MYMODFUN_ {
	uint8_t howfill;

	while(1){
		howfill = (wsrsbufin-wsrsbufou)&0x0F;
		if ( howfill < 13 ) break;
	}
	rsbuf[wsrsbufin] = a;
	wsrsbufin = (++wsrsbufin) & 0x0F;
	PIE1bits.TXIE = 1;
	TXSTAbits.TXEN = 1; //rson
}

/*-----------------------------------------------*/

static void RSouttbl ( unsigned char siz, const unsigned char *tblf ) {
	if ( !siz ) {
		while(1) {
			RSoutP ( *tblf );
			++tblf;
			if ( *tblf == 0 ) break;
		}
		return;
	}
	while(siz) RSoutP ( tblf[--siz] );
}

/*-----------------------------------------------*/

const char tstop[10]   = "TH1 STOP\n";
const char tstart[11]  = "TH1 START\n";
const char t1eg[11]    = "TH1 EGAIN\n";
const char t0start[9]  = "\n\nSTART\n";
const char spaces[8]   = "       ";

extern uint8_t thflag;

void * getadr (void);
	
int main ( void ) {
	
	uint32_t tick;
	
	TMR0L = 0;
	TMR0H = 0;
	TMR1L = 0;
	TMR1H = 0;
	
	initport();
	initth();

	CLRWDT();
	
	tickf1 = 1;
	tickf2 = 0;
	tick = 0;
	wsrsbufin = 0;
	wsrsbufou = 0;
	thrend = 0;

	RSouttbl (0,t0start);

	irq_on();
	
	INTCON &= ~0x40;
	
	//thret = startthvoid ( &myfun1 );
	thrend = startthvoidn ( &myfun1, 1 );
	//if(thrend==1) RSoutP ('1');
	//else RSoutP ('B');

	thrend = startthvoidn ( &myfun2, 2 );
	//if(thrend==2) RSoutP ('2');
	//else  RSoutP ('B');
	
	thrend = startthvoidn ( &myfun3, 3 );
	//if(thrend==3) RSoutP ('3');
	//else  RSoutP ('B');
	
	thrend = startthvoidn ( &myfun4, 4 );
	//if(thrend==4) RSoutP ('4');
	//else  RSoutP ('B');
	
	RSoutP ('\n');
	
	INTCON |= 0x40;
	
	thrend = 0;
	

	while(1) {
		
		++tick;
		binbcd1 ( tbl, tick );
		
		INTCON &= ~0x40;
		
		if ( 4000 == tick ) {
			RSouttbl ( 0, spaces );
			threadstop ( 1 );
			RSouttbl ( 0, tstop );
		}
		if ( 12000 == tick ) {
			RSouttbl ( 0, spaces );
			threadrun ( 1 );
			RSouttbl ( 0, tstart );
		}

		if ( 20000 == tick ) {
			RSouttbl ( 0, spaces );
			tickf1 -= 300000;
			RSouttbl ( 0, t1eg );
			startthvoidn ( &myfun1, 1 );
		}
		
		if ( 26000 == tick ) {
			/* make self ending fun */
			thrend = 1;
		}
		
		if ( 46000 == tick ) {
			/* egain */
			thrend = 0;
			startthvoidn ( &myfun2, 2 );
			startthvoidn ( &myfun3, 3 );
			startthvoidn ( &myfun4, 4 );
		}
		
		INTCON |= 0x40;
		
		if ( (0x7FF & tick) == 0) {
			INTCON &= ~0x40;
			RSoutP ('T');
			RSoutP ('0');
			RSoutP (' ');
			RSouttbl ( 9, tbl );
			RSoutP ('\n');
			INTCON |= 0x40;
			
			INTCON &= ~0x40;
			RSoutP ('T');
			RSoutP ('I');
			RSoutP ('C');
			RSoutP ('K');
			RSoutP (' ');
			binbcd1 ( tbl, tickf2 );
			RSouttbl ( 10, tbl );
			RSoutP ('\n');
			INTCON |= 0x40;
			
		}

		if ( (0xFFF & tick) == 110 ) {
			INTCON &= ~0x40;
			timthr[0] = (timer[1] - timer[0])<<1; //presc t1 x2
			RSoutP ('C');
			RSoutP ('Y');
			RSoutP ('C');
			RSoutP ('L');
			RSoutP ('E');
			RSoutP (' ');
			binbcd1 ( tbl, timthr[0]);
			RSouttbl ( 5, tbl );
			RSoutP (' ');
			binbcd1 ( tbl, timthr[1]);
			RSouttbl ( 5, tbl );
			RSoutP ('\n');
			INTCON |= 0x40;
		}
		
		
		
	}
	
//	while(1);
}


