/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/

/* -----------------------------------------------

  asm analyzer

----------------------------------------------- */

#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

#include "ptcutil.h"

/* ---------------------------------------------------------- */
/*                                                            */
/*                                                            */
/*                                                            */
/* ---------------------------------------------------------- */

static char buf[1024];

char *linetmp;
size_t linesiz = 1024;

uint32_t stepcykl;

/* ---------------------------------------------------------- */
/*                                                            */
/*                                                            */
/*                                                            */
/* ---------------------------------------------------------- */


int main(int argc, char *args[]) {
	
	FILE * plik;
	uint8_t maxreg, maxregfile, maxregfun;
	uint8_t tmp[2];
	char *pstru;	
	char name[64];
	char what[64];
	char what2[64];
	uint32_t value;
	
	/* find register and max */
	if((args[1][0] =='-') && (args[1][1] =='r')){
		maxreg = 0;
		args++;
		while (1){
			args++;
			if(*args==NULL) break;
			plik = fopen ( *args, "r" );
			if(plik==NULL){
				fprintf( stderr, "bad open: %s\n" , *args );
				return 1;
			}
			
			maxregfile = 0;
			
			while(1) {
				
				if( 0 > getline ( &linetmp , &linesiz, plik ) ) break;
				if ( getwathever (buf, linetmp ) == 0 ) continue;
				
				tmp[1] = 0;
				
				/* name regsiter r0x00 */
				if((buf[0] == 'r')&&(buf[1] == '0')&&(buf[2] == 'x')){
					
					//fprintf( stderr, "%s\n" , buf );
					
					tmp[0] = hashexdigit ( buf[3] );
					tmp[1] = hashexdigit ( buf[4] );
					if(tmp[0]>15) continue;
					if(tmp[1]>15) continue;
					tmp[1] += tmp[0]<<4;
					//fprintf( stderr, "0x%02x\n" , tmp[1] );
				}
				
				tmp[1] += 1; //00 mens one reg
				if ( maxregfile < tmp[1] ) maxregfile = tmp[1];
			}
			
			if ( maxreg < maxregfile ) maxreg = maxregfile;
			/* max reg bound to 2  */
			if(maxreg&0x01) maxreg++;
			fprintf( stdout, "%32s reg %3u max %3u\n" , *args, maxregfile, maxreg );
			fclose(plik);
		}
		
		/* test max register NUMBREGSAVE in threadpic16conf.h */
		
		plik = fopen ( "threadpic16conf.h", "r" );
		if(plik==NULL){
			fprintf( stderr, "bad open: %s\n" , "threadpic16conf.h" );
			return 1;
		}
			
		
		while(1) {
			char def[64];
			char what[64];
			char *pstru;
			//int16_t length;
			
			if( 0 > getline ( &linetmp , &linesiz, plik ) ) break;
			
			pstru = getnwathever (def, linetmp, sizeof(def) );
			if(pstru==NULL) continue;
			pstru = getnwathever (what, pstru, sizeof(what) );
			if(pstru==NULL) continue;
			
			if ( 1 != sscanf (pstru, "%u", &value) ) continue;
			
			if ( strcmp( "#define", def ) ) continue;
			if ( strcmp( "NUMBREGSAVE", what ) ) continue;
			
			fprintf( stderr, "%s %s %u\n" , def, what, value );
			
			//snprintf ( "%s %s %u", def, what, &value );
		}
		
		
		fclose(plik);
		return 0;
	}
	
	
	/* _RStim	res	1 */
	/* find pointer in funkcjon */
	if((args[1][0] =='-') && (args[1][1] =='s')){
		uint8_t debufcount;
		uint16_t numberdec, numberdecmax, numberdecmaxall;
		uint16_t resram, resstack, ressum;
		
		numberdecmaxall = 0;
		numberdec = 0;
		numberdecmax = 0;
		debufcount = 0;
		maxreg = 0;
		ressum = 0;
		
		args++;
		
		fprintf( stderr, "          code_name              ");
		fprintf( stderr, "ramstack reg  ram sumram stackram\n");
		
		while (1){
			uint8_t finnmovlw, valmovlw, findcall, findcode, findgptput;
			
			args++;
			if(*args==NULL) break;
			plik = fopen ( *args, "r" );
			if(plik==NULL){
				fprintf( stderr, "bad open: %s\n" , *args );
				return 1;
			}
			
			maxregfile = 0;
			numberdec = 0;
			numberdecmax = 0;
			findcode = 0;
			
			name[0] = 0;
			what[0] = 0;
			what2[0] = 0;
			pstru = NULL;
			resram = 0;
			resstack = 0;
			
			while(1) {
				
				if( 0 > getline ( &linetmp , &linesiz, plik ) ) break;
				
				HEHEGOTO:
				
				name[0] = 0;
				what[0] = 0;
				what2[0] = 0;
				
				pstru = getnwathever (name, linetmp, sizeof(name) );
				if(pstru==NULL) continue;
				pstru = getnwathever (what, pstru, sizeof(what) );
				if(pstru==NULL) continue;
				if ( name[0] == ';' ) continue;
				
				/* count reserved memory */
				/* _stack and _stack_end */
				if ( 0 == strcmp( "res", what ) ) {
					pstru = getnumnwathever (&value, pstru, sizeof(value) );
					if(pstru==NULL) continue;
					if ( 0==strcmp( "_stack", name ) ) {
						resstack += value;
						continue;
					}
					if ( 0==strcmp( "_stack_end", name ) ) {
						resstack += value;
						continue;
					}
					if ( 0==strncmp( "r0x", name, 3 ) ) continue;
					//fprintf( stderr, "%s %u\n" , name, value );
					resram += value;
					ressum += value;
				}

				
				
//				if ( name[0] == ';' ) continue;
				if ( strcmp( "code", what ) ) continue;
				
				fprintf( stderr, "%32s " , name );
				if(numberdecmaxall<numberdecmax) numberdecmaxall = numberdecmax;
				
				numberdec = 0;
				numberdecmax = 0;
				maxregfun = 0;
				findcall = 0;
				
				/*
				if(findcode) { fprintf( stderr, "%3u %3u\n", numberdecmax, numberdecmaxall );
				} else fprintf( stderr, "\n");
				*/				
				//findcode = 1;
				
				/* find POSTDEC1 or _POSTDEC1 */
				while(1) {
					uint8_t tsym;
					
					if( 0 > getline ( &linetmp , &linesiz, plik ) ) break;
					
					HEHEGOTO2:
					
					name[0] = 0;
					what[0] = 0;
					what2[0] = 0;
					finnmovlw = 0;
					//findcall = 0;
					findgptput = 0;
					valmovlw = 0;
					
					pstru = getnwathever (name, linetmp, sizeof(name) );
					if(pstru==NULL) continue;
					
					if ( 0 == strcmp( "end", name ) ) {
						fprintf( stderr, "%5u", numberdecmax );
						fprintf( stderr, " %5u", maxregfun );
						fprintf( stderr, " %5u %5u %5u\n", resram, ressum, resstack );
						goto HEHEGOTO;
						
					}
					
					pstru = getsymnwathever (what, pstru, sizeof(what) );
					if(pstru==NULL) continue;
					
					if ( 0 == strcmp( "code", what ) ) {
						//fprintf( stderr, "%4u", numberdecmax, numberdecmaxall );
						fprintf( stderr, "%5u", numberdecmax );
						//fprintf( stderr, " %5u %5u", maxregfun, maxregfile );
						fprintf( stderr, " %5u", maxregfun );
						fprintf( stderr, " %5u %5u %5u\n", resram, ressum, resstack );
						goto HEHEGOTO;
					}
					
					pstru = getsymnwathever (what2, pstru, sizeof(what2) );
					//fprintf( stderr, "%s %s %s\n" , name, what, what2 );
					
					/* add change for big letters ? */
					
					
					/* test for regiters */
					
					if ( 0 == strcmp( "MOVFF", name ) ) {
						if ( 0 == strncmp( "r0x", what, 3 ) ) {
							uint16_t nreg;
							pstru = getnumnwathever (&nreg, &what[1], sizeof(nreg) );
							//fprintf( stderr, "%s %s %u\n" , name, what, nreg );
							nreg += 1;
							if (maxregfun<nreg) maxregfun = nreg;
							if (maxregfile<maxregfun) maxregfile = maxregfun;
						}
					}
					
					/* if what is posdec inc numberdec */
					tsym = 0;
					if ( 0 == strcmp( "POSTDEC1", what ) ) tsym = 1;
					if ( 0 == strcmp( "_POSTDEC1", what ) ) tsym = 1;
					if ( 0 == strcmp( "POSTDEC1", what2 ) ) tsym = 1;
					if ( 0 == strcmp( "_POSTDEC1", what2 ) ) tsym = 1;
					
					if(tsym) {
						++numberdec;
						if(numberdecmax<numberdec) numberdecmax = numberdec;
						if(numberdecmaxall<numberdecmax) numberdecmaxall = numberdecmax;
						if(debufcount)
							fprintf( stderr, "BZ: %3u %3u %3u %s %s %s\n" ,
							numberdecmaxall, numberdecmax, numberdec, name, what, what2 );
					}
					
					/* find call or pcl */
					if ( 0 == strcmp( "CALL", name ) ) findcall = 1;
					
					if ( ( 0==strcmp( "MOVWF",name) ) && (0==strcmp("PCL",what)) )
						findcall = 1;
					
					if (findcall) {
						//if(numberdec==0) continue;
						if(debufcount)
							fprintf( stderr, "find call %s numdec %3u\n", what, numberdec );
						
						/* CALL	__gptrput1 one less register */
						if ( 0 == strcmp( "__gptrput1", what ) ) findgptput = 1;
						if ( 0 == strcmp( "__gptrput2", what ) ) findgptput = 1;
						if ( 0 == strcmp( "__gptrput3", what ) ) findgptput = 1;
						if ( 0 == strcmp( "__gptrput4", what ) ) findgptput = 1;
						
						if(findgptput) {
							findcall = 0;
							findgptput = 0;
							if(numberdec) --numberdec;
							if(debufcount) fprintf( stderr, "find gptr\n" );
							continue;
						}
						
						if( 0 > getline ( &linetmp , &linesiz, plik ) ) break;
						
						pstru = getnwathever (name, linetmp, sizeof(name) );
						if(pstru==NULL) goto HEHEGOTO2;
						if ( strcmp( "MOVLW", name ) ) goto HEHEGOTO2;
						if(debufcount) fprintf( stderr, "find movlw\n"  );
						
						pstru = getnumnwathever (&valmovlw, pstru, sizeof(valmovlw) );
						if(pstru==NULL) goto HEHEGOTO2;
						if(debufcount) fprintf( stderr, "%u\n" , valmovlw );
						
						if( 0 > getline ( &linetmp , &linesiz, plik ) ) break;
						pstru = getnwathever (name, linetmp, sizeof(name) );
						if ( strcmp( "ADDWF", name ) ) goto HEHEGOTO2;
						if(debufcount) fprintf( stderr, "find addwf\n"  );
						pstru = getsymnwathever (what, pstru, sizeof(what) );
						if(pstru==NULL) goto HEHEGOTO2;
						
						/* is modyfing fsr after call */
						if ( strcmp( "FSR1L", what ) ) goto HEHEGOTO2;
						if(debufcount) fprintf( stderr, "find fsr1\n"  );
						if(valmovlw<=numberdec) numberdec -= valmovlw;
					}
					
					
					
					

				}
				
				
			}
			
			fclose(plik);
			
		}
		return 0;
	}
	
	
	/* make config for asembler
	read from threadpic16onf.h
	writne to thpconf.def
	
	rewrite all THSTACADRxx to _THSTACADRxx
	
	_MAXTHRRUN       equ   0x02
	_THSTACKPOINT01  equ   0x10 ;for 2 thread
	;_THSTACKPOINT01  equ   0x0b ;for 3 threads

	; _THNUMPARAM = NUMBREGSAVE + 11
	; for save TBL
	_THNUMPARAM equ (16+11)

	; for not save TBL
	;_THNUMPARAM equ (16+7)
	*/
	
	/* read number from  */
	if((args[1][0] =='-') && (args[1][1] =='c') && (args[1][2] =='a') ){
		FILE *pfile2;
		
		uint16_t _THREADNUM;
		uint16_t NUMBREGSAVE;
		uint16_t SAVE_TBLCONT;
		
		_THREADNUM = 0xFFFF;
		NUMBREGSAVE = 0xFFFF;
		SAVE_TBLCONT= 0xFFFF;
		
		pfile2 = fopen ( "threadpic16conf.h" ,"r" );
		if(pfile2==NULL) {
			fprintf( stderr, "bad open: %s\n" , "threadpic16conf.h" );
			return 2;
		}
		plik = fopen ( "thpconf.def" ,"w" );
		if(plik==NULL) {
			fprintf( stderr, "bad open: %s\n" , "thpconf.def" );
			return 2;
		}
		
		value = 0;

		fprintf( plik, ";---------------------------------------------\n" );
		fprintf( plik, ";file thpconf.def automatic generic by pictc  \n" );
		fprintf( plik, ";---------------------------------------------\n" );
		fprintf( plik, "\n" );
		
		/* find #define _THREADNUM 2 and set stack pointer */
		/* find #define NUMBREGSAVE 16 */
		while(1){
			if( 0 > getline ( &linetmp , &linesiz, pfile2 ) ) return 3;
			pstru = getnwathever (name, linetmp, sizeof(name) );
//			fprintf( stderr, "%s\n" , name);
			if ( strcmp( "#define", name ) ) continue;
			pstru = getnwathever (name, pstru, sizeof(name) );
//			fprintf( stderr, "%s\n" , name );
			
			if ( 0 == strcmp( "NUMBREGSAVE", name ) ) {
				pstru = getnumnwathever (&NUMBREGSAVE, pstru, sizeof(NUMBREGSAVE) );
				if(pstru==NULL) return 3;
				//fprintf( plik, "_MAXTHRRUN       equ  0x%02x\n" , value );
				fprintf( stderr, "%s %u\n" , name, NUMBREGSAVE );
			}
			
			if ( 0 == strcmp( "SAVE_TBLCONT", name ) ) {
				pstru = getnumnwathever (&SAVE_TBLCONT, pstru, sizeof(SAVE_TBLCONT) );
				if(pstru==NULL) return 3;
				fprintf( stderr, "%s %u\n" , name, SAVE_TBLCONT );
			}
			
			
			/*  */
			if ( 0 == strcmp( "_THREADNUM", name ) ) {
				pstru = getnumnwathever (&_THREADNUM, pstru, sizeof(_THREADNUM) );
				if(pstru==NULL) return 3;
				fprintf( stderr, "%s %u\n" , name, _THREADNUM );
				/*
				fprintf( plik, "_MAXTHRRUN       equ  0x%02x\n" , value );
				if(value==1){
					}
				if(value==2){
					}
				if(value==3){
				}
				fprintf( plik, "_MAXTHRRUN       equ  0x%02x\n" , value );
				fprintf( stderr, "%s %u\n" , name, value );
				*/
			}
			
			if ( (_THREADNUM!=0xFFFF) && (NUMBREGSAVE!=0xFFFF) && (SAVE_TBLCONT!=0xFFFF) )
				break;
		}
		
		if(_THREADNUM>3) return 4;
		if(SAVE_TBLCONT>1) return 4;
			
		fprintf( plik, "_MAXTHRRUN            equ  0x%02x\n" , _THREADNUM );
		
		if(SAVE_TBLCONT==0)
			fprintf( plik, "_THNUMPARAM           equ  0x%02x\n" , NUMBREGSAVE + 5);
		if(SAVE_TBLCONT==1)
			fprintf( plik, "_THNUMPARAM           equ  0x%02x\n" , NUMBREGSAVE + 9);
		
		if(_THREADNUM==1) {
			fprintf( plik, "_THSTACKPOINT01       equ  0x10\n");
			//future fprintf( plik, "_THSTACKPOINT02       equ  0x15\n");
		}
		if(_THREADNUM==2) {
			fprintf( plik, "_THSTACKPOINT01       equ  0x10\n");
		}
		if(_THREADNUM==3) {
			fprintf( plik, "_THSTACKPOINT01       equ  0x0b\n");
		}
		
		
		fprintf( plik, "\n");
		
		fclose(pfile2);
		pfile2 = fopen ( "threadpic16stckconf.h" ,"r" );
		if(pfile2==NULL) {
			fprintf( stderr, "bad open: %s\n" , "threadpic16stckconf.h" );
			return 2;
		}
		
		while(1){
			if( 0 > getline ( &linetmp , &linesiz, pfile2 ) ) return 0;
			pstru = getnwathever (name, linetmp, sizeof(name) );
//			fprintf( stderr, "%s\n" , name);
			if ( strcmp( "#define", name ) ) continue;
			pstru = getnwathever (name, pstru, sizeof(name) );
			if ( 0 == strncmp( "THSTACADR", name, 9 ) ) {
				fprintf( plik, "_%s", name);
				fprintf( plik, "          equ  ");
				/* copy all */
				while(1){
					if ( *pstru == '\n' ) break;
					if ( *pstru == 0 ) break;
					fprintf( plik, "%c", *pstru++ );
				}
				fprintf( plik, "\n");
				
				fprintf( stderr, "%s %u\n" , name, NUMBREGSAVE );
				continue;
			}
		}
			

		
	}
	
	
	/*  make automatic stack for pic18 read from linker script */
	/*
	find max DATABANK and read END
	DATABANK   NAME=gpr0       START=0x80              END=0xFF
	
	with all 256 bytes
	*/
	if((args[1][0] =='-') && (args[1][1] =='c') && (args[1][2] =='s') ){
		FILE *pfile2;
		uint32_t mstart;
		uint32_t mend;
		uint16_t numbgpr;
		uint32_t mstart1;
		uint32_t mend1;
		uint16_t numbgpr1;
	
		pfile2 = fopen ( args[2] ,"r" );
		if(pfile2==NULL) {
			fprintf( stderr, "bad open: %s\n" , args[2] );
			return 2;
		}
		
		mstart = 0;
		mend = 0;
		numbgpr = 0;
		mstart1 = 0;
		mend1 = 0;
		numbgpr1 = 0;
		
		while(1){
			if( 0 > getline ( &linetmp , &linesiz, pfile2 ) ) break;
			pstru = getnwathever (name, linetmp, sizeof(name) );
			//fprintf( stderr, "%s\n" , name);
			if ( strcmp( "DATABANK", name ) ) continue;
			
			pstru = getsymnwathever (name, pstru, sizeof(name) );
			//fprintf( stderr, "%s\n" , name);
			if ( strcmp( "NAME", name ) ) continue;
			
			pstru = getsymnwathever (name, pstru, sizeof(name) );
			//fprintf( stderr, "%s\n" , name);
			if ( strncmp( "gpr", name, 3 ) ) continue;
			if( name[3] <'0' || name[3] >'9' ) continue;
			//pstru = getnumnwathever (&numbgpr, pstru, sizeof(numbgpr) );
			//if(pstru==NULL) continue;
			//fprintf( stderr, "gpr %u\n" , numbgpr);
			
			pstru = getsymnwathever (what, pstru, sizeof(what) );
			//fprintf( stderr, "%s\n" , what);
			if ( strcmp( "START", what ) ) continue;
			pstru = getnumnwathever (&mstart, pstru, sizeof(mstart) );
			//fprintf( stderr, "0x%04x\n" , mstart);
			if(pstru==NULL) continue;
			
			pstru = getsymnwathever (what, pstru, sizeof(what) );
			//fprintf( stderr, "%s\n" , what);
			if ( strcmp( "END", what ) ) continue;
			pstru = getnumnwathever (&mend, pstru, sizeof(mend) );
			//fprintf( stderr, "0x%04x\n" , mend);
			if(pstru==NULL) continue;
			
			if(mstart<mend) mend -= mstart;
			else continue;
			
			pstru = getnumnwathever (&numbgpr, name+3, sizeof(numbgpr) );
			
			if ( mend >= mend1 ) {
				mstart1 = mstart;
				mend1 = mend;
				numbgpr1 = numbgpr;
			}
			
			/* length end-start+1 */
		}
		
		fprintf( stderr, "pictc: stack start %4u siz %4u end %4u numbgpr %2u\n" , \
			mstart1, mend1+1, mstart1 + mend1, numbgpr1 );
		
	}
	
	
	
	
	return 0;
}



/* ---------------------------------------------------------- */
/*            end file                                        */
/* ---------------------------------------------------------- */
