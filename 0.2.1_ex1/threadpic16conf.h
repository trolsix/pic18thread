
/*-----------------------------------------------*/

/* number threads */
#define _THREADNUM 5

/* if 1 all stack will be safe   */
/* another stack will be divided beetwen thread */
#define SAVE_HARDW_STACK 0

/* if stack adr wille be in flash save   ram */
/* but threads stack will be always the same */
#define STACKADR_IN_FLASH 1

/* speedup save-restore contest x 1,2,4,8 */
#define SPEEDUPSAVE   1

/* how many register save */
/* this must be aligned to SPEEDUPSAVE */

/* set for each thread */
#define NUMBREGSAVE00 16
#define NUMBREGSAVE01 0
#define NUMBREGSAVE02 0
#define NUMBREGSAVE03 4
#define NUMBREGSAVE04 0

/* prescaler T0 16 32 64 128 256 */
#define _PRESCALER_T0 64

/* irq switch stack to THSTACADRIH and save ram for other stack */
#define STACK_FOR_IRQ 1

/*-----------------------------------------------*/
/*  the way thread ends  */

//#define END_TH_FUN() while(1)
#define END_TH_FUN() {while(1) {INTCONbits.TMR0IF=1;}}

/*-----------------------------------------------*/
/*     very dangerous     better dont change    */

/* body of places allowed past void function */
#define FUN_IRQ_THREAD_ALLOWED 1

/* save tbl ragister or not */
#define SAVE_TBLCONT 1

/* dont change this if you dont know what do
this change thread priority
*/
#define THREAD_USE_LOW_PRIORITY 1

/*-----------------------------------------------*/
/* dont change below */
/*-----------------------------------------------*/

#if _THREADNUM==1
#define THSTACKPOINT01 0
#endif

#if _THREADNUM==2
#define THSTACKPOINT01 16
#define THSTACKPOINT02 31
#endif

#if _THREADNUM==3
#define THSTACKPOINT01 10
#define THSTACKPOINT02 20
#endif

#if _THREADNUM==4
#define THSTACKPOINT01 8
#define THSTACKPOINT02 16
#define THSTACKPOINT03 24
#endif
#if _THREADNUM==5
#define THSTACKPOINT01 8
#define THSTACKPOINT02 13
#define THSTACKPOINT03 18
#define THSTACKPOINT04 24
#endif

#if SAVE_TBLCONT==1
#define ADDREGSTACK 9
#endif
#if SAVE_TBLCONT==0
#define ADDREGSTACK 5
#endif

/* future set for each thread */
#define NUMPARAM00 (NUMBREGSAVE00+ADDREGSTACK)
#define NUMPARAM01 (NUMBREGSAVE01+ADDREGSTACK)
#define NUMPARAM02 (NUMBREGSAVE02+ADDREGSTACK)
#define NUMPARAM03 (NUMBREGSAVE03+ADDREGSTACK)
#define NUMPARAM04 (NUMBREGSAVE04+ADDREGSTACK)

#define DNMPLACES00 0
#define DNMPLACES01 NUMPARAM00
#define DNMPLACES02 (DNMPLACES01+NUMPARAM01)
#define DNMPLACES03 (DNMPLACES02+NUMPARAM02)
#define DNMPLACES04 (DNMPLACES03+NUMPARAM03)
#define DNMPLACES05 (DNMPLACES04+NUMPARAM04)

/*-----------------------------------------------*/

void irq_onasm (void);

/*-----------------------------------------------*/

