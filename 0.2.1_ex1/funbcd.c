/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/

#include <stdint.h>

/*-----------------------------------------------*/

static const uint32_t dectbl[9] = {  1,
                         10,       100,
                       1000,     10000,
                     100000,   1000000,
                   10000000, 100000000 };

void binbcd1 ( char * tbl, uint32_t ul ) __reentrant {
	uint32_t in1, in2;
	unsigned char wsz, a;

	tbl[10] = 0;
	tbl[9] = 0;	

	while ( ul > 999999999 ) {
		++tbl[9];
		ul -= 1000000000;
	}
	tbl[9] += '0';	
	in1 = ul;
	for( wsz=8; wsz&0xFF; --wsz ) {
		in2 = dectbl[wsz];
		a = 0;
		while (1) {
			in1 -= in2;
			if ( in1 & 0x80000000 )
				break;
			++a;
		}
		in1 += in2;
		tbl[wsz] = a+'0';
	}
	
	tbl[0] = in1+'0';
}

/*-----------------------------------------------*/

