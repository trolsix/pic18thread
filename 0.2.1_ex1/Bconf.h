/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/

/*-----------------------------------------------*/

#if defined(_PIC18F1220_H_) || defined(__PIC18F1220_H__)
#pragma config OSC=INTIO2
#pragma config FSCM=OFF
#pragma config IESO=OFF
#pragma config PWRT=ON
#pragma config BOR=OFF
#pragma config WDT=OFF
#pragma config WDTPS=8192
#pragma config MCLRE=ON
#pragma config STVR=ON
#pragma config LVP=OFF
#pragma config DEBUG=OFF
#pragma config CP0=OFF
#pragma config CP1=OFF
#pragma config CPB=OFF
#pragma config CPD=OFF
#pragma config WRT0=OFF
#pragma config WRT1=OFF
#pragma config EBTR1=OFF
#pragma config EBTRB=OFF
#pragma config WRTB=OFF
#pragma config WRTC=OFF
#pragma config WRTD=OFF
#endif

#if defined(_PIC18F2420_H_) || defined(__PIC18F2420_H__)
#pragma config XINST=OFF
//#pragma config OSC=INTIO2
//#pragma config FSCM=OFF
#pragma config IESO=OFF
#pragma config PWRT=ON
//#pragma config BOR=OFF
#pragma config WDT=OFF
#pragma config WDTPS=8192
#pragma config MCLRE=ON
//#pragma config STVR=ON
#pragma config LVP=OFF
#pragma config DEBUG=OFF
#pragma config CP0=OFF
#pragma config CP1=OFF
#pragma config CPB=OFF
#pragma config CPD=OFF
#pragma config WRT0=OFF
#pragma config WRT1=OFF
#pragma config EBTR1=OFF
#pragma config EBTRB=OFF
#pragma config WRTB=OFF
#pragma config WRTC=OFF
#pragma config WRTD=OFF
#endif

#if defined(_PIC18F26K22_H_) || defined(__PIC18F26K22_H__)
#pragma config XINST=OFF
#pragma config IESO=OFF
#pragma config WDTPS=8192
#pragma config MCLRE=INTMCLR
#pragma config LVP=OFF
#pragma config DEBUG=OFF
#pragma config CP0=OFF
#pragma config CP1=OFF
#pragma config CPB=OFF
#pragma config CPD=OFF
#pragma config WRT0=OFF
#pragma config WRT1=OFF
#pragma config EBTR1=OFF
#pragma config EBTRB=OFF
#pragma config WRTB=OFF
#pragma config WRTC=OFF
#pragma config WRTD=OFF
#endif


/*-----------------------------------------------*/
