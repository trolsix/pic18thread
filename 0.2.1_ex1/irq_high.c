/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/

/*-----------------------------------------------*/

#ifdef __SDCC_pic16	
#include <pic16/pic18fregs.h>
#endif

#include <stdint.h>

/*-----------------------------------------------*/

extern volatile uint8_t wsrsbufin;
extern volatile uint8_t wsrsbufou;
extern volatile char rsbuf[16];

/*-----------------------------------------------*/
/* 1 high 2 low*/
//https://sourceforge.net/p/sdcc/bugs/2202/

#define THREAD_USE_LOW_PRIORITY 1
#if THREAD_USE_LOW_PRIORITY==1

void tc_int (void) __shadowregs __interrupt 1 {

	if(PIR1bits.TMR1IF) {
		PIR1bits.TMR1IF=0;
	}
	
	//if ( ( PIE1bits.TXIE ) && ( PIR1bits.TXIF ) ) {
	if ( PIR1bits.TXIF ) {
		/* bufor empty disble */
		if ( wsrsbufin == wsrsbufou ) {
			//PIE1bits.TXIE = 0;
			//TXSTAbits.TXEN = 0; //rsoff
			PIE1bits.TXIE = 0;
		} else {
			TXREG = rsbuf[wsrsbufou];
			wsrsbufou = (++wsrsbufou)&0x0F;
		}
	}
}

#endif

