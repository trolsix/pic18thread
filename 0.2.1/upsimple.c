/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/
/*-----------------------------------------------
       pic18f1220 thread example
-----------------------------------------------*/

#include "Bheadb.h"

/*-----------------------------------------------*/

#include <stdio.h>
#include <stdint.h>

#include "threadpic16.h"
#include "threadpic16conf.h"

/*-----------------------------------------------*/
/*    config   fuse  hardware uC                 */
/*-----------------------------------------------*/

#include "Bconf.h"

/*-----------------------------------------------*/

void CLRWDT(void);

/*-----------------------------------------------*/
/* example */
/*
uint8_t myfun1 ( void ) {
	return 1;
}*/

//for naked funkcjon return in asm
/*
uint8_t myfun2 ( void ) __naked {

	
	__asm
		movlw 0x03
		return
	__endasm;
}*/
	
int main ( void ) {
	
	
	//initport();
	//initth();

	CLRWDT();
	
	//irq_on();

	//thrend = startthvoidn ( &myfun1, 1 );
	
	//thrend = startthvoidn ( &myfun2, 2 );
	
	while(1) {
		
	}

}


