
------------

Test version for pic18f25q10

------------

Set path in mekfile to sdcc and gputils

------------

Microcontroler pic18f25q10 has no support in sdccc 3.6.0
For example, has differ read eeprom then other typical pic18fxxxx.

------------

all config erased - Fosc = 4MHz

------------

This is workaround make only for test.

1. change libdev18f1220.lib to libdev18f1220_cut2.lib
and removed part register with pic18df1220

2. add pic18f25q10_c1.c and pic18f25q10.h witch has definition register
and with non existing register eeprom for compability.

3. Compile like 18f25k50

4. Add macro -D_$(CHIP)

5. In file Bheadb.h is change __PIC18F25K50_H__ macro

------------






