#<meta http-equiv="content-type" content="text/html; charset=utf-8">
#<?xml version='1.0' encoding='utf-8'?>

------------

Wersja testowa na pic18f25q10

------------

W pliku makefile należy podac własne ścieżki do sdcc i gputils

------------

config jest w stanie skasownym - Fosc = 4MHz

------------

sdcc 3.6.0 nie obsługuje standardowo pic18f25q10

Nie jest on kompatybilny choćby z powodu innego zapisu eeprom
niż typowe pice z serii pic18fxxxx

Na chwile obecną jest to tylko test. Aby się uruchomił:

1. zmieniłem  biblioteke libdev18f1220.lib na libdev18f1220_cut2.lib
kasując część dotyczącą rejestrów.

2. pliki pic18f25q10_c1.c i pic18f25q10.h z rejestrami są kompilowane 
i dołaczane do linkera. Są tam definicje z nieistniejacymi rejestrami eeprom 

3. Projekt kompiluje się jako 18f25k50

4. Do kompilacji dodawane jest makro -D_$(CHIP)

5. następuje podmiana sybmbolu __PIC18F25K50_H__ w pliku Bheadb.h


Jest to workaround, wystarczy aby przetestować.

------------


