/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/
/*
stty -F /dev/ttyS0 raw
stty -F /dev/ttyS0 -echo -echoe -echok -echonl -noflsh -xcase -tostop -echoprt -echoctl -echoke
*/
/*-----------------------------------------------
       thread example
-----------------------------------------------*/

#include "Bheadb.h"

/*-----------------------------------------------*/

#include <stdio.h>
#include <stdint.h>

#include "nothing.h"
#include "threadpic16.h"
#include "threadpic16conf.h"
#include "semaphorespic16.h"
#include "mutexpic16.h"

#include "threadpic16stckconf.h"

/*-----------------------------------------------*/
/*    config   fuse  hardware uC                 */
/*-----------------------------------------------*/

#include "Bconf.h"

/*-----------------------------------------------*/

void setrs ( void);
void RSoutP (unsigned char a) _MYMODFUN_;
void RSoutB (unsigned char a) _MYMODFUN_;
void initport (void);

/*-----------------------------------------------*/

void RSouttbl ( unsigned char siz, const unsigned char *tbl ) __reentrant;

/*-----------------------------------------------*/

//void binbcd1 ( char * tbl, uint32_t a07 ) __reentrant;
uint8_t binbcd1 ( void );
void sendhex (unsigned char a) _MYMODFUN_;

/*-----------------------------------------------*/

sem_t sembcd;
sem_t semmain;
//mutex_t mut1;

/*-----------------------------------------------*/

uint16_t timer[2];
static char tbl[11];
static uint16_t temp[3];
static uint16_t uval0[3];
volatile uint8_t thrend;
uint16_t timthr;
uint32_t timsum[2];

/*-----------------------------------------------*/

#if DUMPSTACKH || DUMPSATCKS
uint8_t stack_dump[93];
#endif

/*-----------------------------------------------*/

static uint32_t tickf1[3];

/*-----------------------------------------------*/

/*-----------------------------------------------*/
/* example */

extern uint8_t thactiv;
extern volatile uint8_t thstate[];
extern volatile uint16_t thsem[];

static uint8_t myfun1 ( void ) __naked {
	
	while(1) {
		uint8_t wskdata;
	
		//wskdata = 0;
		
		if(thactiv==1) wskdata = 0;
		else if(thactiv==3) { wskdata = 1; }
		else if(thactiv==4) { wskdata = 2; }
		else { RSoutP (thactiv+'0'); break;} //have not data for this	
	
		if ( !( tickf1[wskdata] & 0x0FFF ) ) {
			//irq off save stack in low irq
			//and make atomic for system
			INTCON &= ~0x40;
			RSoutP ('T');
			RSoutP (thactiv+'0');
			RSoutP ('\n');
			//RSoutP (' ');
			INTCON |= 0x40;
		}
		
		tickf1[wskdata] += 1;
		if(thrend) break;
	}
	
	INTCON &= ~0x40;
	RSoutP ('-');
	RSoutP (thactiv+'0');
	RSoutP ('-');
	INTCON |= 0x40;
	__asm
		movlw 0x00
		RETURN
	__endasm;
	//return 1;
}

/*-----------------------------------------------*/

#if defined(__PIC18F25Q10_H__) || defined(_PIC18F25Q10_H_)

void make4meas (void){
	PIR1 &= ~0x02;
	ADCNT = 0;
	ADCON0 |= _ADGO;
	/*
	while(ADCON0 & _ADGO);
	ADCON0 |= _ADGO;
	while(ADCON0 & _ADGO);
	ADCON0 |= _ADGO;
	while(ADCON0 & _ADGO);
	ADCON0 |= _ADGO;
	while(ADCON0 & _ADGO);*/
	//should flag set
	while(!(PIR1 & 0x02));//thresflag
}

#endif

/*-----------------------------------------------*/

//static uint8_t myfun2 ( int a ) __reentrant __naked { //bad because lost stack
//static uint8_t myfun2 ( int a ) __reentrant {
static uint8_t myfun2 ( void ) __naked {
	
#if defined(__PIC18F25Q10_H__) || defined(_PIC18F25Q10_H_)

	
	//u reference
	FVRCON = 0b10100001; //enable fvr enable buffer adc 1V
	//FVRCON = 0b10100011; //enable fvr enable buffer adc 4V
	//FVRCON = 0b10110011; //enable fvr enable buffer adc 4V
	
	//FOSC / 16 4MHZ->4us
	ADCLK = 0b000100;
	//ADREF = 0x03; //internal fvr
	ADACQ = 50; //200us for temperatoe sensor
	ADCAP = 0;//more capacitanse
	//adon fosc right adj 
	//ADCON0 = 0b10000100;
	//adon retriged fosc right adj 
	ADCON0 = 0b11000100;
	ADCON2 = 0x24; //div 4 - low pass
	//ADCON2 = 0x22; //div 4 - average
	ADCON3 = 0x0F; //irq at threshold and clr ADGO
	ADRPT = 4; //test after 4 samples
	
	uval0[2] = 0;
	temp[2] = 0;
	//wait for enable fvr
	while (!(FVRCON & 0x40 ));
	
	while(1) {

		ADREF = 0x00; //vdd
		ADPCH = 0b111101; //temp
		ADACC = temp[2];
		
		make4meas ();
		INTCON &= ~0x40;//atomic
		temp[0] = ADRES;
		temp[1] = ADFLTR;
		temp[2] = ADACC;
		INTCON |= 0x40;
		
		ADREF = 0x03; //internal fvr
		ADPCH = 0; //ra0
		ADACC = uval0[2];
		
		make4meas ();
		INTCON &= ~0x40; //atomic
		uval0[0] = ADRES;
		uval0[1] = ADFLTR;
		uval0[2] = ADACC;
		INTCON |= 0x40;
	
		++tickf1[2];
		
		/* set interrupt because dont need massive conversion */

		PIR0 |= 1<<5;
	}
#else

	while(1) {
		uval0[0] += 1;
		uval0[1] += 2;
		uval0[2] += 3;
		
		temp[0] += 4;
		temp[1] += 5;
		temp[2] += 6;
		
		++tickf1[2];
	}
	//INTCONbits.TMR0IF=1;

#endif
	
	//return 0;
	__asm
		movlw 0x00
		RETURN
	__endasm;
	
}	
/*-----------------------------------------------*/

void RSoutB (unsigned char a) _MYMODFUN_ {
	uint8_t i;
	
	for ( i=0x80; i; i >>= 1 ) {
		if(a&i) RSoutP ('1');
		else RSoutP ('0');
	}
	RSoutP (' ');
}

/*-----------------------------------------------*/
/* send stack with portion  50 */
#if DUMPSATCKS
void sendmem ( uint8_t * wsk ) {
	uint8_t i;
	
	INTCON &= ~0x80;
	for(i=0;i<50;++i ) {
		stack_dump[i] = *wsk;
		--wsk;
	}
	INTCON |= 0x80;
	
	//send
	//RSoutP (10);
	for(i=0;i<50;){
		sendhex (stack_dump[i]);
		RSoutP (' ');
		++i;
		if(0==(i&0x1F))RSoutP (10);
	}
	RSoutP (10);
}
#endif
/*-----------------------------------------------*/

extern uint32_t ul;
extern char * tbll;

#define BINASI(PARAM1,PARAM2) INTCON &= ~0x40; ul=PARAM2; tbll=PARAM1; \
rungetbcd()

/*-----------------------------------------------*/
/* call void funkcion is efective */

void waitsemmain ( void ) {
	sem_wait(&semmain);
}

void runbcd ( void ) {
	sem_post(&sembcd);
	INTCON|=0x40;
}

void rungetbcd ( void ) {
	sem_post(&sembcd);
	INTCON|=0x40;
	sem_wait(&semmain);
}

/*-----------------------------------------------*/
//const char tstop[10]   = "TH1 STOP\n";
//const char tstart[11]  = "TH1 START\n";
//const char t1eg[11]    = "TH1 EGAIN\n";

const char what[]    = "Example v 0.3.2\n\n";
const char t0start[9]  = "\n\nSTART\n";

//extern uint8_t thflag;

volatile unsigned char * wskchar;

int main ( void ) {
	
	static uint32_t tick;

	initport();
	setrs();
	
	TMR0L = 0;
	TMR1L = 0;
	TMR1H = 0;
	
	timsum[0] = 0;
	timsum[1] = 0;
	
	timer[0] = 0;

	//initport();
	initth();

	CLRWDT();

	tickf1[0] = 0;
	tickf1[1] = 0;
	tickf1[2] = 0;
	tick = 0;

	setrs();
	irq_on();
	INTCON &= ~0x40;
	
	RSouttbl (0,t0start);
	RSouttbl (0,what);
	
//	thrend = mutex_init ( &mut1, 0 );
	thrend = sem_init ( &semmain, 0 );
	thrend = sem_init ( &sembcd, 0 );
	INTCON &= ~0x40;
	
	//RSoutB (mut1.val);

	while(1) { //chwilka
		CLRWDT();
		++tick;
		if(tick>50000){
			tick = 0;
			break;
		}
	}
	
	
	thrend = 0;
	
	tbl[0] = startthvoidn ( &myfun1, 1 );
	if(tbl[0]==1) RSoutP ('1');
	else RSoutP ('B');

	tbl[0] = startthvoidn ( &binbcd1, 2 );
	if(tbl[0]==2) RSoutP ('2');
	else  RSoutP ('B');
	
	tbl[0] = startthvoidn ( &myfun1, 3 );
	if(tbl[0]==3) RSoutP ('3');
	else  RSoutP ('B');
	
	tbl[0] = startthvoidn ( &myfun2, 4 );
	if(tbl[0]==4) RSoutP ('4');
	else  RSoutP ('B');

	RSoutP ('\n');
	
	//INTCON |= 0x40;
	//mutex_lock ( &mut1 );
	
	while(1) {
		CLRWDT();
		#if DEBUG_ADRFSR1
		if(FSR1L!=(0xFF&THSTACADR00)) {
			INTCON &= ~0x40;
			RSoutP ('*');
			RSoutP ('0');
			RSoutP ('*');
			RSoutP ('\n');
			while(1);
		}
		#endif
		
		//RSoutP (':');
		
		++tick;
		//continue;
		
		//BINASI ( tbl, tick );
		#if TSTMUTEX
		if ( ( 2002 == tick )  ) {
			//INTCON &= ~0x40;
			//while(mut1.val);
			//if(mut1.val){
				RSoutP ('\n');
				//mut1.val = 1;
				RSoutB (mut1.val);
				//RSoutP (mut1.val+'0');
				mutex_lock ( &mut1 );
				RSoutB (mut1.val);
				//mutex_lock ( &mut1 );
				RSoutP ('L');
			//} else {
				RSoutP ('\n');
			//}
			//INTCON |= 0x40;
			//threadstop ( 1 );
			//RSouttbl ( 0, tstop );
				
		}
		
		if ( 12000 == tick ) {
			//INTCON &= ~0x40;
			mutex_unlock ( &mut1 );
			INTCON &= ~0x40;
			RSoutP ('M');
			RSoutP ('U');
			INTCON |= 0x40;
			//threadrun ( 1 );
			//RSouttbl ( 0, tstart );
		}
		#endif
		
		//dumpstackwithirq
		//a mamy amlo memeory a chcemy znac stos
		#define DUMPSTACKHWIRQ 0
		#define DUMPSTACKSFIRQ 0
		if ( (0x3FFF & tick) == 1) {
			#include "sutil/dumphwstacksend.h"
		}
		
		if ( (0x3FFF & tick) == 2) {
			#include "sutil/dumpsfstacksend.h"
		}
		
		if ( (0x3FFF & tick) == 10) {
			BINASI ( tbl, tick );
			INTCON &= ~0x40;
			RSoutP ('T'); RSoutP ('0'); RSoutP (' ');
			//RSoutP (mut1.val+'0');
			RSouttbl ( 10, tbl );
			RSoutP ('\n');
			INTCON |= 0x40;
			
			BINASI ( tbl, tickf1[0] );
			INTCON &= ~0x40;
			RSoutP ('Z'); RSoutP ('0'); RSoutP (' ');
			RSouttbl ( 10, tbl );
			RSoutP ('\n');
			INTCON |= 0x40;
			
			BINASI ( tbl, tickf1[1] );
			INTCON &= ~0x40;
			RSoutP ('Z'); RSoutP ('1'); RSoutP (' ');
			RSouttbl ( 10, tbl );
			RSoutP ('\n');
			INTCON |= 0x40;
			
			BINASI ( tbl, tickf1[2] );
			INTCON &= ~0x40;
			RSoutP ('Z'); RSoutP ('2'); RSoutP (' ');
			RSouttbl ( 10, tbl );
			RSoutP ('\n');
			INTCON |= 0x40;
		}
		
		if ( (0x1FFF & tick) == 13 ) {
			BINASI ( tbl, temp[0] );
			INTCON &= ~0x40;
			RSoutP ('\n');
			RSoutP ('T'); RSoutP ('E'); RSoutP ('M'); RSoutP ('P'); RSoutP (' ');
			RSouttbl ( 10, tbl );
			RSoutP ('\n');
			INTCON |= 0x40;
			
			BINASI ( tbl, temp[1] );
			INTCON &= ~0x40;
			RSoutP ('T'); RSoutP ('F'); RSoutP ('L'); RSoutP ('T'); RSoutP (' ');
			RSouttbl ( 10, tbl );
			RSoutP ('\n');
			
			BINASI ( tbl, temp[2] );
			INTCON &= ~0x40;
			RSoutP ('T'); RSoutP ('A'); RSoutP ('C'); RSoutP ('C'); RSoutP (' ');
			RSouttbl ( 10, tbl );
			RSoutP ('\n');
			
			BINASI ( tbl, uval0[0] );
			INTCON &= ~0x40;
			RSoutP ('U'); RSoutP ('R'); RSoutP ('A'); RSoutP ('0'); RSoutP (' ');
			RSouttbl ( 10, tbl );
			RSoutP ('\n');
			INTCON |= 0x40;
			
			BINASI ( tbl, uval0[1] );
			INTCON &= ~0x40;
			RSoutP ('U'); RSoutP ('F'); RSoutP ('L'); RSoutP ('T'); RSoutP (' ');
			RSouttbl ( 10, tbl );
			RSoutP ('\n');
			
			BINASI ( tbl, uval0[2] );
			INTCON &= ~0x40;
			RSoutP ('U'); RSoutP ('A'); RSoutP ('C'); RSoutP ('C'); RSoutP (' ');
			RSouttbl ( 10, tbl );
			RSoutP ('\n');
		}

		if ( (0x3FFF & tick) == 16 ) {
			BINASI ( tbl, timer[0]<<1 );
			INTCON &= ~0x40;
			RSoutP ('\n');
			RSoutP ('C'); RSoutP ('0'); RSoutP (' ');
			RSouttbl ( 5, tbl );
			RSoutP (' ');
			INTCON |= 0x40;
			
			BINASI ( tbl, timsum[0] );
			INTCON &= ~0x40;
			RSoutP (' ');
			RSouttbl ( 9, tbl );
			RSoutP (' ');
			INTCON |= 0x40;
			
			BINASI ( tbl, timsum[1]<<1 );
			INTCON &= ~0x40;
			RSoutP (' ');
			RSouttbl ( 10, tbl );
			RSoutP ('\n');
			INTCON |= 0x40;
		}
	
		
	}
	
//	while(1);
}

/*-----------------------------------------------*/

void sendhex (unsigned char a) _MYMODFUN_ {
	uint8_t tmp;
	
	tmp = a>>4;
	tmp &= 0x0F;
	if(tmp>9) tmp += 'a' - 10;
	else tmp += '0';
	RSoutP (tmp);
	
	tmp = a & 0x0F;
	if(tmp>9) tmp += 'a' - 10;
	else tmp += '0';
	RSoutP (tmp);
}


/*-----------------------------------------------*/

