/* ---------------------------------------------------------- */
/*                                                            */
/*                                                            */
/*                                                            */
/* ---------------------------------------------------------- */

#ifndef _PTCUTIL_H
#define _PTCUTIL_H      1005

unsigned char hashexdigit ( unsigned char tmp2 );
int readparam ( int argc, char * argv[] );
int readstr ( char * a, char * b, size_t s );
int szukaju ( char * a, char * b );
uint16_t getwathever (char *gdzie, char *co );
char * getnwathever (char *gdzie, void *co, int max );
char * getsymnwathever (char *gdzie, char *co, int max );
char * getnumnwathever ( void *, char * co, int max );

void setupper ( char * dst, char * src );

#endif
