/* -----------------------------------------------

  asm analyzer

----------------------------------------------- */

#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

#include "ptcutil.h"

/* ---------------------------------------------------------- */
/*                                                            */
/*                                                            */
/*                                                            */
/* ---------------------------------------------------------- */

static char buf[1024];

char *linetmp;
size_t linesiz = 1024;

uint32_t stepcykl;

/* ---------------------------------------------------------- */
/*                                                            */
/*                                                            */
/*                                                            */
/* ---------------------------------------------------------- */


uint8_t getres () {
	
	
	return 0;
}

/* ---------------------------------------------------------- */
/*                                                            */
/*                                                            */
/*                                                            */
/* ---------------------------------------------------------- */

enum { code, postdec, gptput };

#define CODESET() ( flag |= 1<<code )
#define CODECLR() ( flag &= ~(1<<code) )
#define CODEIS() (flag&(1<<code))

#define POSTDEC1SET() ( flag |= 1<<postdec )
#define POSTDEC1CLR() ( flag &= ~(1<<postdec) )
#define POSTDEC1IS() (flag&(1<<postdec))

#define GPTPUTSET() ( flag |= 1<<gptput )
#define GPTPUTCLR() ( flag &= ~(1<<gptput) )
#define GPTPUTIS() (flag&(1<<gptput))


/* ---------------------------------------------------------- */
/*                                                            */
/*                                                            */
/*                                                            */
/* ---------------------------------------------------------- */


uint8_t lookpicasm ( char *args[] ) {
	
	FILE * plik, *filelist;
	uint8_t maxregfun, maxregreg;
	uint8_t dbg;
	char *pstru;
	char codename[128];
	char name[128];
	char what[128];
	char what2[128];
	char afterupper[128];
	uint32_t value;
	uint16_t resram, resstack, ressum;
	uint8_t valw;
	uint16_t numberdec, numberdecmax, numberdecmaxall, stackh;
	uint32_t flag;
	
	uint8_t debufcount = 0;
	
	args++;
	dbg = 0;
	
	resram = 0;
	ressum = 0;
	stackh = 0;
	numberdecmaxall = 0;
	
	/* open list file */
	filelist = fopen ( "_listnamefun.txt", "w" );
	
	while (1) {
		
		args++;
		if(*args==NULL) break;
		plik = fopen ( *args, "r" );
		if(plik==NULL){
			fprintf( stderr, "bad open: %s\n" , *args );
			return 1;
		} else {
			fprintf( stderr, "open: %s\n" , *args );
		}
		
		
		CODECLR();
		
		resstack = 0;
		numberdec = 0;
		numberdecmax = 0;
		maxregfun = 0;
		maxregreg = 0;
		valw = 0;
		resram = 0;
		
		fprintf( stderr, "codename numberdecmax maxregreg maxregfun ");
		fprintf( stderr, "resram, ressum, resstack \n");
		
		while(1){
			
			if( 0 > getline ( &linetmp , &linesiz, plik ) ) break;
			
			name[0] = 0;
			what[0] = 0;
			what2[0] = 0;
			
			pstru = getnwathever (name, linetmp, sizeof(name) );
			//if(pstru) pstru = getnwathever (what, pstru, sizeof(what) );
			//if(pstru)
				pstru = getsymnwathever (what, pstru, sizeof(what) );
			//if(pstru)
				pstru = getsymnwathever (what2, pstru, sizeof(what2) );
			//if(pstru==NULL) continue;
			if ( name[0] == ';' ) continue;
			
			
			/* count reserved memory */
			/* _stack and _stack_end */
			if ( 0 == strcmp( "res", what ) ) {
				pstru = getnumnwathever (&value, what2, sizeof(value) );
				if(pstru==NULL) continue;
				if ( 0==strcmp( "_stack", name ) ) {
					resstack += value;
					continue;
				}
				if ( 0==strcmp( "_stack_end", name ) ) {
					resstack += value;
					continue;
				}
				if ( 0==strncmp( "r0x", name, 3 ) ) {
					uint16_t nreg;
					pstru = getnumnwathever (&nreg, &name[1], sizeof(nreg) );
					nreg += 1;
					if (nreg>maxregreg)maxregreg = nreg;
					continue;
				}
				
				resram += value;
				ressum += value;
				
				//fprintf( stderr, "_%s_%u_\n" , name, value );
			}
			
			
			if ( 0==strncmp( "r0x", what, 3 ) ) {
				uint16_t nreg;
				pstru = getnumnwathever (&nreg, &what[1], sizeof(nreg) );
				nreg += 1;
				if (nreg>maxregfun)maxregfun = nreg;
			}
			
			if ( 0==strncmp( "r0x", what2, 3 ) ) {
				uint16_t nreg;
				pstru = getnumnwathever (&nreg, &what2[1], sizeof(nreg) );
				nreg += 1;
				if (nreg>maxregfun)maxregfun = nreg;
			}
				
			/* code */
			if (  0 == strcmp( "code", what ) ) {
				
				if( CODEIS() ) {
					fprintf( stderr, "%40s", codename );
					fprintf( stderr, "%5u", numberdecmax );
					fprintf( stderr, " %5u", maxregreg );
					fprintf( stderr, " %5u", maxregfun );
					fprintf( stderr, " %5u %5u %5u\n", resram, ressum, resstack );
				}
				//fprintf( stderr, "_%s_\n" , name );

				CODESET();
				numberdec = 0;
				numberdecmax = 0;
				maxregfun = 0;
				valw = 0;
				
				getnwathever ( codename, name, sizeof(name) );
				
				if( filelist ) {
					fprintf( filelist, "m %s\n" , codename);
				}
				
				continue;
			}
			
			
			/* if what is posdec inc numberdec */
			POSTDEC1CLR();
			setupper ( afterupper, what2 );
			if ( 0 == strcmp( "POSTDEC1", afterupper ) ) POSTDEC1SET();
			if ( 0 == strcmp( "_POSTDEC1", afterupper ) ) POSTDEC1SET();
			setupper ( afterupper, what );
			if ( 0 == strcmp( "POSTDEC1", afterupper ) ) POSTDEC1SET();
			if ( 0 == strcmp( "_POSTDEC1", afterupper ) ) POSTDEC1SET();
			
			if(POSTDEC1IS()) {
				++numberdec;
				if(numberdecmax<numberdec) numberdecmax = numberdec;
				if(numberdecmaxall<numberdecmax) numberdecmaxall = numberdecmax;
				if(debufcount)
					fprintf( stderr, "BZ: %3u %3u %3u %s %s %s\n" ,
					numberdecmaxall, numberdecmax, numberdec, name, what, what2 );
				continue;
			}

			
			setupper ( afterupper, name );
			
			if ( 0 == strcmp( "GOTO", afterupper ) ) {
				if( filelist ) {
					fprintf( filelist, "g %s %u\n" , what, numberdec );
				}
				continue;
			}
			
			
			
			/* find call */
			if ( 0 == strcmp( "CALL", afterupper ) ) {
				//fprintf( stderr, "_%s %s %u\n" , name, what, numberdec );
				
				/* save what call */
				if( filelist ) {
					fprintf( filelist, "c %s %u\n" , what, numberdec );
				}
				
				GPTPUTCLR();
				if ( 0 == strcmp( "__gptrput1", what ) ) GPTPUTSET();
				else if ( 0 == strcmp( "__gptrput2", what ) ) GPTPUTSET();
				else if ( 0 == strcmp( "__gptrput3", what ) ) GPTPUTSET();
				else if ( 0 == strcmp( "__gptrput4", what ) ) GPTPUTSET();
				
				if(GPTPUTIS()) {
					if(numberdec) --numberdec;
					if(debufcount) fprintf( stderr, "find gptr\n" );
				}
				continue;
			}
			
			
			if ( 0 == strcmp( "MOVLW", afterupper ) ) {
				pstru = getnumnwathever (&valw, what, sizeof(valw) );
				if(debufcount) fprintf( stderr, "find movlw %u\n", valw  );
				continue;
			}
			
			
			if ( 0 == strcmp( "ADDWF", afterupper ) ) {
				if(debufcount) fprintf( stderr, "find fsr1 %s\n" , what );
				/* is modyfing fsr after call */
				setupper ( afterupper, what );
				if ( 0 == strcmp( "FSR1L", afterupper ) ) {
					if(valw<=numberdec) numberdec -= valw;
				}
				continue;
			}
			
			if ( 0 == strcmp( "SUBWF", afterupper ) ) {
				if(debufcount) fprintf( stderr, "find fsr1 %s %u\n" , what, valw );
				/* is modyfing fsr after call */
				setupper ( afterupper, what );
				if ( 0 == strcmp( "FSR1L", afterupper ) ) {
					numberdec += valw;
				}
				continue;
			}
			

			/* find jmp */
			if( ':' == name[strlen(name)-1] ) {
				name[strlen(name)-1] = 0;
				if(dbg&0x04) fprintf( stderr, "label: %s\n" , name );
				fprintf( filelist, "l %s %u %s\n" , name, numberdec, codename );
				continue;
			}
			
			if ( 0 == strcmp( "end", name ) ) {
				fprintf( stderr, "%40s", codename );
				fprintf( stderr, "%5u", numberdecmax );
				fprintf( stderr, " %5u", maxregreg );
				fprintf( stderr, " %5u", maxregfun );
				fprintf( stderr, " %5u %5u %5u\n", resram, ressum, resstack );
				break;
			}
			
		}
		
		
	}

	
	return 0;
}




/* ---------------------------------------------------------- */
/*                                                            */
/*                                                            */
/*                                                            */
/* ---------------------------------------------------------- */

int main(int argc, char *args[]) {
	
	char *pstru;	
	char name[64];
	char what[64];

	/* 
	incr stack - max
	decr stac
	register - max
	ram (max for files) - sumram
	stack ram
	name calling funcion

	*/
	
	if((args[1][0] =='-') && (args[1][1] =='s')){
		return (unsigned int) lookpicasm ( args );
	}


	/*  make automatic stack for pic18 read from linker script */
	/*
	find max DATABANK and read END
	DATABANK   NAME=gpr0       START=0x80              END=0xFF
	
	with all 256 bytes
	*/
	if((args[1][0] =='-') && (args[1][1] =='c') && (args[1][2] =='s') ){
		FILE *pfile2;
		uint32_t mstart;
		uint32_t mend;
		uint16_t numbgpr;
		uint32_t mstart1;
		uint32_t mend1;
		uint16_t numbgpr1;
	
		pfile2 = fopen ( args[2] ,"r" );
		if(pfile2==NULL) {
			fprintf( stderr, "bad open: %s\n" , args[2] );
			return 2;
		}
		
		mstart = 0;
		mend = 0;
		numbgpr = 0;
		mstart1 = 0;
		mend1 = 0;
		numbgpr1 = 0;
		
		while(1){
			if( 0 > getline ( &linetmp , &linesiz, pfile2 ) ) break;
			pstru = getnwathever (name, linetmp, sizeof(name) );
			//fprintf( stderr, "%s\n" , name);
			if ( strcmp( "DATABANK", name ) ) continue;
			
			pstru = getsymnwathever (name, pstru, sizeof(name) );
			//fprintf( stderr, "%s\n" , name);
			if ( strcmp( "NAME", name ) ) continue;
			
			pstru = getsymnwathever (name, pstru, sizeof(name) );
			//fprintf( stderr, "%s\n" , name);
			if ( strncmp( "gpr", name, 3 ) ) continue;
			if( name[3] <'0' || name[3] >'9' ) continue;
			//pstru = getnumnwathever (&numbgpr, pstru, sizeof(numbgpr) );
			//if(pstru==NULL) continue;
			//fprintf( stderr, "gpr %u\n" , numbgpr);
			
			pstru = getsymnwathever (what, pstru, sizeof(what) );
			//fprintf( stderr, "%s\n" , what);
			if ( strcmp( "START", what ) ) continue;
			pstru = getnumnwathever (&mstart, pstru, sizeof(mstart) );
			//fprintf( stderr, "0x%04x\n" , mstart);
			if(pstru==NULL) continue;
			
			pstru = getsymnwathever (what, pstru, sizeof(what) );
			//fprintf( stderr, "%s\n" , what);
			if ( strcmp( "END", what ) ) continue;
			pstru = getnumnwathever (&mend, pstru, sizeof(mend) );
			//fprintf( stderr, "0x%04x\n" , mend);
			if(pstru==NULL) continue;
			
			if(mstart<mend) mend -= mstart;
			else continue;
			
			pstru = getnumnwathever (&numbgpr, name+3, sizeof(numbgpr) );
			
			if ( mend >= mend1 ) {
				mstart1 = mstart;
				mend1 = mend;
				numbgpr1 = numbgpr;
			}
			
			/* length end-start+1 */
		}
		
		fprintf( stderr, "pictc: stack start %4u siz %4u end %4u numbgpr %2u\n" , \
			mstart1, mend1+1, mstart1 + mend1, numbgpr1 );
		
	}
	
	
	
	
	return 0;
}



/* ---------------------------------------------------------- */
/*            end file                                        */
/* ---------------------------------------------------------- */
