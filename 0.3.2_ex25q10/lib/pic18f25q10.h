/* ------------------------------------------------- */

#ifndef __PIC18F25Q10_H__
#define __PIC18F25Q10_H__

/* ------------------------------------------------- */
//fake dummy register - this workaround and not will work

extern __at(0x0F20) __sfr EEADR;
extern __at(0x0F20) __sfr EECON1;
extern __at(0x0F20) __sfr EECON2;
extern __at(0x0F20) __sfr EEDATA;
/*
typedef struct
  {
  unsigned RD                   : 1;
  unsigned WR                   : 1;
  unsigned WREN                 : 1;
  unsigned WRERR                : 1;
  unsigned FREE                 : 1;
  unsigned                      : 1;
  unsigned CFGS                 : 1;
  unsigned EEPGD                : 1;
  } __EECON1bits_t;
*/
//extern __at(0x0F20) volatile __EECON1bits_t EECON1bits;

/* ------------------------------------------------- */

extern __at(0x0F2C) __sfr FVRCON;

/* ------------------------------------------------- */

#define _ADGO 0x01

extern __at(0x0F51) __sfr ADACT;
extern __at(0x0F52) __sfr ADCLK;
extern __at(0x0F53) __sfr ADREF;
extern __at(0x0F54) __sfr ADCON1;
extern __at(0x0F55) __sfr ADCON2;
extern __at(0x0F56) __sfr ADCON3;
extern __at(0x0F57) __sfr ADACQ;
extern __at(0x0F58) __sfr ADCAP;
extern __at(0x0F59) __sfr ADPRE;
extern __at(0x0F5A) __sfr ADPCH;
extern __at(0x0F5B) __sfr ADCON0;
extern __at(0x0F5C) __sfr ADPREVL;
extern __at(0x0F5D) __sfr ADPREVH;
//extern __at(0x0F5E) __sfr ADRESL;
//extern __at(0x0F5F) __sfr ADRESH;
extern __at(0x0F60) __sfr ADSTAT;
extern __at(0x0F61) __sfr ADRPT;
extern __at(0x0F62) __sfr ADCNT;
extern __at(0x0F63) __sfr ADSTPTL;
extern __at(0x0F64) __sfr ADSTPTH;
extern __at(0x0F65) __sfr ADLTHL;
extern __at(0x0F66) __sfr ADLTHH;
extern __at(0x0F67) __sfr ADUTHL;
extern __at(0x0F68) __sfr ADUTHH;
extern __at(0x0F69) __sfr ADERRL;
extern __at(0x0F6A) __sfr ADERRH;
//extern __at(0x0F6B) __sfr ADACCL;
//extern __at(0x0F6C) __sfr ADACCH;
//extern __at(0x0F6D) __sfr ADFLTRL;
//extern __at(0x0F6E) __sfr ADFLTRH;


extern __at(0x0F5E) volatile unsigned int ADRES;
extern __at(0x0F6B) volatile unsigned int  ADACC;
extern __at(0x0F6D) volatile unsigned int ADFLTR;

/* ------------------------------------------------- */

typedef struct
  {
  unsigned TX9D                 : 1;
  unsigned TRMT                 : 1;
  unsigned BRGH                 : 1;
  unsigned SENDB                : 1;
  unsigned SYNC                 : 1;
  unsigned TXEN                 : 1;
  unsigned TX9                  : 1;
  unsigned CSRC                 : 1;
  } __TX1STAbits_t;

/* ------------------------------------------------- */

#define PPSLOCKED 0

/* ------------------------------------------------- */

  
extern __at(0x0FD1) __sfr TMR1CLK;
extern __at(0x0FCE) __sfr T1CON;
extern __at(0x0FCC) __sfr TMR1L;
extern __at(0x0FCD) __sfr TMR1H;
  
extern __at(0x0F9D) volatile __TX1STAbits_t TX1STAbits;
  
extern __at(0x0F87) __sfr TRISA;
extern __at(0x0F0B) __sfr WPUA;
extern __at(0x0F0C) __sfr ANSELA;
extern __at(0x0F82) __sfr LATA;
  
extern __at(0x0F88) __sfr TRISB;
extern __at(0x0F13) __sfr WPUB;
extern __at(0x0F14) __sfr ANSELB;
extern __at(0x0F83) __sfr LATB;

extern __at(0x0F89) __sfr TRISC;
extern __at(0x0F1B) __sfr WPUC;
extern __at(0x0F1C) __sfr ANSELC; //eedtata
extern __at(0x0F84) __sfr LATC;

extern __at(0x0F0B) __sfr WPUA;
  
extern __at(0x0FD2) __sfr TMR0L;
extern __at(0x0FD3) __sfr TMR0H;
extern __at(0x0FD4) __sfr T0CON0;
extern __at(0x0FD5) __sfr T0CON1;

extern __at(0x0ED4) __sfr OSCON2;

//----------------------------------

extern __at(0x0E9B) __sfr PPSLOCK;
extern __at(0x0EEA) __sfr RB0PPS;
extern __at(0x0EEB) __sfr RB1PPS;
extern __at(0x0EEC) __sfr RB2PPS;
extern __at(0x0EED) __sfr RB3PPS;
extern __at(0x0EEE) __sfr RB4PPS;

extern __at(0x0EB0) __sfr RX1PPS;

//----------------------------------

extern __at(0x0F98) __sfr RC1REG;
extern __at(0x0F99) __sfr TX1REG;
extern __at(0x0F9A) __sfr SP1BRGL;
extern __at(0x0F9B) __sfr SP1BRGH;
extern __at(0x0F9C) __sfr RC1STA;
extern __at(0x0F9D) __sfr TX1STA;
extern __at(0x0F9E) __sfr BAUD1CON;

//----------------------------------

extern __at(0x0EB5) __sfr IPR0;
extern __at(0x0EB6) __sfr IPR1;
extern __at(0x0EB7) __sfr IPR2;
extern __at(0x0EB8) __sfr IPR3;
extern __at(0x0EB9) __sfr IPR4;
extern __at(0x0EBA) __sfr IPR5;
extern __at(0x0EBB) __sfr IPR6;
extern __at(0x0EBC) __sfr IPR7;

extern __at(0x0EBD) __sfr PIE0;
extern __at(0x0EBE) __sfr PIE1;
extern __at(0x0EBF) __sfr PIE2;
extern __at(0x0EC0) __sfr PIE3;
extern __at(0x0EC1) __sfr PIE4;
extern __at(0x0EC2) __sfr PIE5;
extern __at(0x0EC3) __sfr PIE6;
extern __at(0x0EC4) __sfr PIE7;

extern __at(0x0EC5) __sfr PIR0;
extern __at(0x0EC6) __sfr PIR1;
extern __at(0x0EC7) __sfr PIR2;
extern __at(0x0EC8) __sfr PIR3;
extern __at(0x0EC9) __sfr PIR4;
extern __at(0x0ECA) __sfr PIR5;
extern __at(0x0ECB) __sfr PIR6;
extern __at(0x0ECC) __sfr PIR7;

extern __at(0x0FF2) __sfr INTCON;

//----------------------------------

extern __at(0x0FD8) __sfr STATUS;

extern __at(0x0FD9) __sfr FSR2L;
extern __at(0x0FDA) __sfr FSR2H;
extern __at(0x0FDB) __sfr PLUSW2;
extern __at(0x0FDC) __sfr PREINC2;
extern __at(0x0FDD) __sfr POSTDEC2;
extern __at(0x0FDE) __sfr POSTINC2;
extern __at(0x0FDF) __sfr INDF2;

extern __at(0x0FE0) __sfr BSR;

extern __at(0x0FE1) __sfr FSR1L;
extern __at(0x0FE2) __sfr FSR1H;
extern __at(0x0FE3) __sfr PLUSW1;
extern __at(0x0FE4) __sfr PREINC1;
extern __at(0x0FE5) __sfr POSTDEC1;
extern __at(0x0FE6) __sfr POSTINC1;
extern __at(0x0FE7) __sfr INDF1;

extern __at(0x0FE8) __sfr WREG;

extern __at(0x0FE9) __sfr FSR0L;
extern __at(0x0FEA) __sfr FSR0H;
extern __at(0x0FEB) __sfr PLUSW0;
extern __at(0x0FEC) __sfr PREINC0;
extern __at(0x0FED) __sfr POSTDEC0;
extern __at(0x0FEE) __sfr POSTINC0;
extern __at(0x0FEF) __sfr INDF0;

extern __at(0x0FF2) __sfr INTCON;

extern __at(0x0FF3) __sfr PROD;
extern __at(0x0FF3) __sfr PRODL;
extern __at(0x0FF4) __sfr PRODH;

extern __at(0x0FF5) __sfr TABLAT;
extern __at(0x0FF6) __sfr TBLPTR;
extern __at(0x0FF6) __sfr TBLPTRL;
extern __at(0x0FF7) __sfr TBLPTRH;
extern __at(0x0FF8) __sfr TBLPTRU;

extern __at(0x0FF9) __sfr PC;
extern __at(0x0FF9) __sfr PCL;
extern __at(0x0FFA) __sfr PCLATH;
extern __at(0x0FFB) __sfr PCLATU;
extern __at(0x0FFC) __sfr STKPTR;

extern __at(0x0FFD) __sfr TOS;
extern __at(0x0FFD) __sfr TOSL;
extern __at(0x0FFE) __sfr TOSH;
extern __at(0x0FFF) __sfr TOSU;

//----------------------------------


/*
extern __sfr TRISB;
extern __sfr ANSELB;
extern __sfr LATB;
extern __sfr TRISC;
extern __sfr ANSELC;
extern __sfr LATC;
extern __sfr INTCON;
extern __sfr PPSLOCK;
extern __sfr RB0PPS;
extern __sfr RB1PPS;
extern __sfr TMR0H;
extern __sfr T0CON0;
extern __sfr T0CON1;
extern __sfr OSCON2;
extern __sfr RX1PPS;
extern __sfr SP1BRGL;
extern __sfr SP1BRGH;
extern __sfr RC1STA;
extern __sfr TX1STA;
extern __sfr BAUD1CON;
extern __sfr RC1REG;
extern __sfr TX1REG;
extern __sfr PIE3;
extern __sfr PIR3;
extern __sfr IPR3;*/
//extern __sfr 

#endif // #ifndef __PIC18F25Q10_H__

