/*----------------------------------
 * This definitions of the PIC18Fxxxx MCU.
 *
 *----------------------------------*/

#include "pic18f25q10.h"

//----------------------------------
//fake dummy

__at(0x0F20) __sfr EEADR;
__at(0x0F20) __sfr EECON1;
__at(0x0F20) __sfr EECON2;
__at(0x0F20) __sfr EEDATA;
//__at(0x0F20) volatile __EECON1bits_t EECON1bits;


__at(0x0F2C) __sfr FVRCON;

//----------------------------------

__at(0x0F51) __sfr ADACT;
__at(0x0F52) __sfr ADCLK;
__at(0x0F53) __sfr ADREF;
__at(0x0F54) __sfr ADCON1;
__at(0x0F55) __sfr ADCON2;
__at(0x0F56) __sfr ADCON3;
__at(0x0F57) __sfr ADACQ;
__at(0x0F58) __sfr ADCAP;
__at(0x0F59) __sfr ADPRE;
__at(0x0F5A) __sfr ADPCH;
__at(0x0F5B) __sfr ADCON0;
__at(0x0F5C) __sfr ADPREVL;
__at(0x0F5D) __sfr ADPREVH;
//__at(0x0F5E) __sfr ADRESL;
//__at(0x0F5F) __sfr ADRESH;
__at(0x0F60) __sfr ADSTAT;
__at(0x0F61) __sfr ADRPT;
__at(0x0F62) __sfr ADCNT;
__at(0x0F63) __sfr ADSTPTL;
__at(0x0F64) __sfr ADSTPTH;
__at(0x0F65) __sfr ADLTHL;
__at(0x0F66) __sfr ADLTHH;
__at(0x0F67) __sfr ADUTHL;
__at(0x0F68) __sfr ADUTHH;
__at(0x0F69) __sfr ADERRL;
__at(0x0F6A) __sfr ADERRH;
//__at(0x0F6B) __sfr ADACCL;
//__at(0x0F6C) __sfr ADACCH;
//__at(0x0F6D) __sfr ADFLTRL;
//__at(0x0F6E) __sfr ADFLTRH;
//__at(0x0F62) __sfr AD;

__at(0x0F5E) volatile unsigned int ADRES;
__at(0x0F6B) volatile unsigned int ADACC;
__at(0x0F6D) volatile unsigned int ADFLTR;
//----------------------------------

__at(0x0FD1) __sfr TMR1CLK;
__at(0x0FCE) __sfr T1CON;
__at(0x0FCC) __sfr TMR1L;
__at(0x0FCD) __sfr TMR1H;

//----------------------------------

__at(0x0F87) __sfr TRISA;
__at(0x0F0B) __sfr WPUA;
__at(0x0F0C) __sfr ANSELA; //eedtata
__at(0x0F82) __sfr LATA;

__at(0x0F88) __sfr TRISB;
__at(0x0F13) __sfr WPUB;
__at(0x0F14) __sfr ANSELB; //eedtata
__at(0x0F83) __sfr LATB;

__at(0x0F89) __sfr TRISC;
__at(0x0F1B) __sfr WPUC;
__at(0x0F1C) __sfr ANSELC; //eedtata
__at(0x0F84) __sfr LATC;

__at(0x0FD2) __sfr TMR0L;
__at(0x0FD3) __sfr TMR0H;
__at(0x0FD4) __sfr T0CON0;
__at(0x0FD5) __sfr T0CON1;

__at(0x0ED4) __sfr OSCON2;

//__at(0x0FD5) __sfr 
//----------------------------------

__at(0x0E9B) __sfr PPSLOCK;
__at(0x0EEA) __sfr RB0PPS;
__at(0x0EEB) __sfr RB1PPS;
__at(0x0EEC) __sfr RB2PPS;
__at(0x0EED) __sfr RB3PPS;
__at(0x0EEE) __sfr RB4PPS;

__at(0x0EB0) __sfr RX1PPS;

//----------------------------------

__at(0x0F98) __sfr RC1REG;
__at(0x0F99) __sfr TX1REG;
__at(0x0F9A) __sfr SP1BRGL;
__at(0x0F9B) __sfr SP1BRGH;
__at(0x0F9C) __sfr RC1STA;
__at(0x0F9D) __sfr TX1STA;
__at(0x0F9D) volatile __TX1STAbits_t TX1STAbits;
__at(0x0F9E) __sfr BAUD1CON;

//----------------------------------

__at(0x0EB5) __sfr IPR0;
__at(0x0EB6) __sfr IPR1;
__at(0x0EB7) __sfr IPR2;
__at(0x0EB8) __sfr IPR3;
__at(0x0EB9) __sfr IPR4;
__at(0x0EBA) __sfr IPR5;
__at(0x0EBB) __sfr IPR6;
__at(0x0EBC) __sfr IPR7;

__at(0x0EBD) __sfr PIE0;
__at(0x0EBE) __sfr PIE1;
__at(0x0EBF) __sfr PIE2;
__at(0x0EC0) __sfr PIE3;
__at(0x0EC1) __sfr PIE4;
__at(0x0EC2) __sfr PIE5;
__at(0x0EC3) __sfr PIE6;
__at(0x0EC4) __sfr PIE7;

__at(0x0EC5) __sfr PIR0;
__at(0x0EC6) __sfr PIR1;
__at(0x0EC7) __sfr PIR2;
__at(0x0EC8) __sfr PIR3;
__at(0x0EC9) __sfr PIR4;
__at(0x0ECA) __sfr PIR5;
__at(0x0ECB) __sfr PIR6;
__at(0x0ECC) __sfr PIR7;

__at(0x0FF2) __sfr INTCON;

//----------------------------------

__at(0x0FD8) __sfr STATUS;

__at(0x0FD9) __sfr FSR2L;
__at(0x0FDA) __sfr FSR2H;
__at(0x0FDB) __sfr PLUSW2;
__at(0x0FDC) __sfr PREINC2;
__at(0x0FDD) __sfr POSTDEC2;
__at(0x0FDE) __sfr POSTINC2;
__at(0x0FDF) __sfr INDF2;

__at(0x0FE0) __sfr BSR;

__at(0x0FE1) __sfr FSR1L;
__at(0x0FE2) __sfr FSR1H;
__at(0x0FE3) __sfr PLUSW1;
__at(0x0FE4) __sfr PREINC1;
__at(0x0FE5) __sfr POSTDEC1;
__at(0x0FE6) __sfr POSTINC1;
__at(0x0FE7) __sfr INDF1;

__at(0x0FE8) __sfr WREG;

__at(0x0FE9) __sfr FSR0L;
__at(0x0FEA) __sfr FSR0H;
__at(0x0FEB) __sfr PLUSW0;
__at(0x0FEC) __sfr PREINC0;
__at(0x0FED) __sfr POSTDEC0;
__at(0x0FEE) __sfr POSTINC0;
__at(0x0FEF) __sfr INDF0;

//__at(0x0FF0) __sfr INTCON3;
//__at(0x0FF1) __sfr INTCON2;
__at(0x0FF2) __sfr INTCON;

__at(0x0FF3) __sfr PROD;
__at(0x0FF3) __sfr PRODL;
__at(0x0FF4) __sfr PRODH;

__at(0x0FF5) __sfr TABLAT;
__at(0x0FF6) __sfr TBLPTR;
__at(0x0FF6) __sfr TBLPTRL;
__at(0x0FF7) __sfr TBLPTRH;
__at(0x0FF8) __sfr TBLPTRU;

__at(0x0FF9) __sfr PC;
__at(0x0FF9) __sfr PCL;
__at(0x0FFA) __sfr PCLATH;
__at(0x0FFB) __sfr PCLATU;
__at(0x0FFC) __sfr STKPTR;

__at(0x0FFD) __sfr TOS;
__at(0x0FFD) __sfr TOSL;
__at(0x0FFE) __sfr TOSH;
__at(0x0FFF) __sfr TOSU;

//----------------------------------
