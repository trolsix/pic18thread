

/* fill stack */
#if FILLSTACK == 1
if(1){
	__asm
	movlw 0x55
	movwf 0x00, A
	movff _FSR1L,_FSR0L
	movff _FSR1H,_FSR0H
	
	FILLLMEM:
	movf 0x00,W
	movwf _POSTDEC0
	movff _FSR0L,_PRODL
	movff _FSR0H,_PRODH
	movlw low(THSTACADRFF+1)
	subwf _PRODL, F, A
	movlw ((THSTACADRFF+1)>>8)
	subwfb _PRODH, F, A
	bnc FILLEND
	goto FILLLMEM
	
	FILLEND:
	__endasm;
}
#endif

/* clr stack hard */
#if FILLHARDWSTACK == 1
if(1){
	uint8_t stmem;
	stmem = STKPTR;
	while(1){
		STKPTR += 1;
		TOSL = 0;
		TOSH = 0;
		TOSU = 0;
		if(STKPTR==31)break;
	}
	STKPTR = stmem;
}
#endif
