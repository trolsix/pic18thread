/* ---------------------------------------------------------- */
/*                                                            */
/*                   is string b in     a                     */
/*                                                            */
/* ---------------------------------------------------------- */

#include <stdint.h>
#include <stdio.h>

#include "ptcutil.h"

/* ---------------------------------------------------------- */
/*                                                            */
/*                                                            */
/*                                                            */
/* ---------------------------------------------------------- */


unsigned char hashexdigit ( unsigned char tmp2 ) {
	if ( ( tmp2 >= '0' ) && ( tmp2 <= '9' ) ) return tmp2 - ('0'-0);
	if ( ( tmp2 >= 'a' ) && ( tmp2 <= 'f' ) ) return tmp2 - ('a'-10);
	if ( ( tmp2 >= 'A' ) && ( tmp2 <= 'F' ) ) return tmp2 - ('A'-10);
	return 0x80;
}

/* ---------------------------------------------------------- */
/*                                                            */
/*         change to upper                                    */
/*                                                            */
/* ---------------------------------------------------------- */

void setupper ( char * dst, char * src ) {
	
	while(src){
		char a;
		a = *src++;
		if ( (a>='a') && (a<='z') ) *dst++ = a-('a'-'A');
		else *dst++ = a;
		if(a==0) break;
	}
	
}

/* ---------------------------------------------------------- */
/*                                                            */
/*                   is string b in     a                     */
/*                                                            */
/* ---------------------------------------------------------- */

int szukaju ( char * a, char * b ) {

int i =0;

//if(a!=0)fprintf(  stdout , ".%c%c." , a[i], b[i] );
//fflush(stdout);

while ( ( *a != '\0' ) && ( *b != '\0' ) ) {

  for ( i=0; i>=0; i++ ) {
    if ( a[i] != b[i] ) {
      if ( ( a[i] != 0 ) && ( b[i] != 0 ) ) break;   /* nei ma szukamy dalej */    
      if ( ( a[i] == 0 ) && ( b[i] != 0 ) ) return -1; /* za dlugie b czylli nie ma */
      if ( b[i] == 0 ) return 0; /* jest b w a */
    }
    if ( b[i] == 0 ) return 0;   /* nei ma szukamy dalej */
  }

  a++;
}

return 1;
}


/* ---------------------------------------------------------- */
/*                                                            */
/*         find something and copy                            */
/*                                                            */
/* ---------------------------------------------------------- */

uint16_t getwathever (char *gdzie, char *co ) {
	uint16_t ile = 0;
		
	if(co==NULL) return 0;
	
	while(1) {
		if(*co>0x20) break;
		if(*co==0) return 0;
		++co;
	}
	
	while(1) {
		*gdzie = *co;
		if (*co<33) break;
		++gdzie;
		++co;
		++ile;
	}

	*gdzie = 0;
	
	return ile;	
}

/* ---------------------------------------------------------- */
/*                                                            */
/*         find something and copy                            */
/*                                                            */
/* ---------------------------------------------------------- */

char * getnwathever (char *gdzie, void *c, int max ) {
	uint16_t ile = 0;
	unsigned char *co = c;
	
	if(co==NULL) return NULL;
	if(max<2) return NULL;
	
	while(1) {
		if(*co==0) return NULL;
		if(*co>0x20) break;
		++co;
	}
	
	while(1) {
		*gdzie = *co;
		if (*co<33) break;
		++gdzie;
		++co;
		++ile;
		if( ile == max ) {
			*gdzie = 0;
			return NULL;
		};
	}

	*gdzie = 0;
	
	return (char*) co;
}


/* ---------------------------------------------------------- */
/*                                                            */
/*         find number                                        */
/*                                                            */
/* ---------------------------------------------------------- */

char * getnumnwathever ( void * dst, char * co, int max ) {
	uint8_t tmp, *ddd;
	uint32_t insnum;
	
	insnum = 0;
	
	while(1) {
		if(*co==0) return NULL;
		//if(*co>0x20) break;
		if( (*co>='0') && (*co<='9') ) break;
		if( (*co>='A') && (*co<='Z') ) break;
		if( (*co>='a') && (*co<='z') ) break;
		++co;
	}

	if((co[0]=='0')&&(co[1]=='x')){
		co += 2;
		while(1){
			tmp = hashexdigit ( *co );
			if(tmp>0x0F) break;
			insnum <<= 4;
			insnum += tmp;
			++co;
		}
	} else {
		while(1){
			tmp = hashexdigit ( *co );
			if(tmp>0x09) break;
			insnum *= 10;
			insnum += tmp;
			++co;
		}
	}
	
	ddd = dst;
	while(max--) {
		*ddd++ = insnum&0xFF;
		insnum >>= 8;
	}
	
	return co;
}

/* ---------------------------------------------------------- */
/*                                                            */
/*         find something and copy                            */
/*                                                            */
/* ---------------------------------------------------------- */

char * getsymnwathever (char *gdzie, char *co, int max ) {
	uint16_t ile = 0;
	
	if(co==NULL) return NULL;
	if(max<2) return NULL;
	
	while(1) {
		if(*co==0) return NULL;
		if( (*co>='0') && (*co<='9') ) break;
		if( (*co>='A') && (*co<='Z') ) break;
		if( (*co>='a') && (*co<='z') ) break;
		if(*co>='_') break;
		++co;
	}
	
	while(1) {
		*gdzie = *co;
		while(1){
			if( (*co>='0') && (*co<='9') ) break;
			if( (*co>='A') && (*co<='Z') ) break;
			if( (*co>='a') && (*co<='z') ) break;
			if(*co>='_') break;
			*gdzie = 0;
			return co;
		}
		++gdzie;
		++co;
		++ile;
		if( ile == max ) {
			*gdzie = 0;
			return NULL;
		};
	}

	*gdzie = 0;
	
	return (char*) co;
}

/* ---------------------------------------------------------- */
/*                                                            */
/*                                                            */
/*                                                            */
/* ---------------------------------------------------------- */

int readstr ( char * a, char * b, size_t s ) {

size_t i = 0;

while ( ++i < s ) {

  if ( *b == 0 )
    break;

  *a++ = *b++;
}

*a = 0;

return i - 1;
}

/* ---------------------------------------------------------- */
/*            end file                                        */
/* ---------------------------------------------------------- */
