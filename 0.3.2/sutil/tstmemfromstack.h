

#if TEST_RAM_MEM == 1
if(1) {
	uint8_t badm[2];
	uint8_t value;
	
/* test memory from pointer fsr1 */
//fsr0
	__asm
		movff _bad, 0x01
		
		movlw 255
		movwf 0x02 //ile testuw
		movwf 0x03 //wartosc wpisywana
		
		BIGTM:
		clrwdt
		
		movff 0x02, 0x03
		movff _FSR1L,_FSR0L
		movff _FSR1H,_FSR0H
		
		PTLMEMTESTSAVE:
		movf 0x03, W
		//save until FSR0 > 0x0F
		movwf _POSTDEC0
		addlw 0x11
		movwf _POSTDEC0
		addlw 0x11
		movwf _POSTDEC0
		addlw 0x11
		movwf _POSTDEC0
		addlw 0x11
		
		movwf 0x03
		
		movff _FSR0L,_PRODL
		movff _FSR0H,_PRODH
		
		movlw 0x0F
		subwf _PRODL, F, A
		movlw 0x00
		subwfb _PRODH, F, A
		bnc TESTREAD
		goto PTLMEMTESTSAVE
		
		TESTREAD:
		
		movff 0x02, 0x03
		movff _FSR1L,_FSR0L
		movff _FSR1H,_FSR0H
		
		PTLMEMTESTREAD:
		movf 0x03, W
		
		xorwf _POSTDEC0, F, A
		bnz TESTBAD
		addlw 0x11
		xorwf _POSTDEC0, F, A
		bnz TESTBAD
		addlw 0x11
		xorwf _POSTDEC0, F, A
		bnz TESTBAD
		addlw 0x11
		xorwf _POSTDEC0, F, A
		bnz TESTBAD
		addlw 0x11
		xorwf _POSTDEC0, F, A
		bnz TESTBAD
		addlw 0x11
		xorwf _POSTDEC0, F, A
		bnz TESTBAD
		addlw 0x11
		xorwf _POSTDEC0, F, A
		bnz TESTBAD
		addlw 0x11
		xorwf _POSTDEC0, F, A
		bnz TESTBAD
		addlw 0x11

		movwf 0x03
		//read until FSR0 > 0x0F
		movff _FSR0L,_PRODL
		movff _FSR0H,_PRODH
		movlw 0x0F
		subwf _PRODL, F, A
		movlw 0x00
		subwfb _PRODH, F, A
		bnc TESTENDRQ
		goto PTLMEMTESTREAD
				
		TESTENDRQ:
		decfsz 0x02
		goto BIGTM
		
		//tutaj znaczy OK zerujemy
		clrf 0x00, A
		movff 0x00 , _value
		movff 0x00,(_badm+0)
		movff 0x00,(_badm+1)
		movff 0x00, _bad
		goto TESTEND
		
		TESTBAD:
		movwf 0x00
		movff 0x00 , _value
		movff _FSR0L,(_badm+0)
		movff _FSR0H,(_badm+1)
		movff 0x01, _bad
		
		TESTEND:
		
	__endasm;

}
#endif
