

#if TEST_STACK_MEM == 1
if(0){
	uint8_t bad;
	uint8_t stmem;
	
	//on tx 25q10
	TX1STAbits.TXEN = 1; //rson
	while ( !( PIR3 & 0x10 ) ); TX1REG = 's';	
	
/* test stosu */
	i = 0;
	bad  = 0;
	stmem = STKPTR;

	do {
		uint8_t a,b,c,i;
		--i;
		a = i;
		b = ~i;
		c = i+0x55;
		c &= 0x1F;
		while (!( PIR3 & 0x10 ) ); TX1REG = '.';
		STKPTR = stmem;
		while(1){
			//while (!( PIR3 & 0x10 ) ); TX1REG = '?';
			STKPTR += 1;
			TOSL = a;
			TOSH = b;
			TOSU = c;
			if(STKPTR==31)break;
			a += 0x11;
			b += 0x33;
			c += 0x35;
			c &= 0x1F;
		}
		STKPTR = stmem;
		a = i;
		b = ~i;
		c = i+0x55;
		c &= 0x1F;
		//while (!( PIR3 & 0x10 ) );	TX1REG = '.';
		while(1){
			//while (!( PIR3 & 0x10 ) ); TX1REG = '!';
			STKPTR += 1;
			bad = 0;
			if ( TOSL ^ a ) bad |= 1;
			if ( TOSH ^ b ) bad |= 2;
			if ( TOSU ^ c ) bad |= 4;
			if ( STKPTR==31 ) break;
			a += 0x11;
			b += 0x33;
			c += 0x35;
			c &= 0x1F;
		}
		
		STKPTR = stmem;
		CLRWDT();
	} while(i);

	//25q10
	while (!( PIR3 & 0x10 ) );
	TX1REG = bad+'0';
//only for simulator
//	TXREG = 10;
	STKPTR = stmem;
	
		while(1){
			STKPTR += 1;
			TOSL = 0;
			TOSH = 0;
			TOSU = 0;
			if(STKPTR==31)break;
		}
	
	STKPTR = stmem;
//	TXREG = 10;
}
#endif

