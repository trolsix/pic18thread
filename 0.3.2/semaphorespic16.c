/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/
/*-----------------------------------------------*/

#include <stdint.h>

/*-----------------------------------------------*/

#include "Bheadb.h"
#include "threadpic16.h"
#include "threadpic16conf.h"

#include "semaphorespic16.h"

extern uint8_t thactiv;
extern volatile uint8_t thstate[];
extern volatile uint16_t thsem[];

#define DEBUGRSSEM     0
#define FUNSEMASMPIC   0

/*-----------------------------------------------*/

#if DEBUGRSSEM == 1
void RSoutP (unsigned char a) _MYMODFUN_;
#endif

/*-----------------------------------------------*/

#if FUNSEMASMPIC==0
uint8_t sem_init ( sem_t * sem, uint8_t initvalue ) __reentrant {
//uint8_t sem_init ( sem_t * sem, uint8_t initvalue ) {

	if ( initvalue > SEMVALUSEMAX )
		return -1;
	
	sem->val = initvalue;
	sem->rut = 0;
	return 0;
}

#endif

#if FUNSEMASMPIC==1

uint8_t sem_init_GL ( sem_t * sem, uint8_t initvalue ) __naked {

	sem;
	initvalue;
	
	__asm
	global _sem_init
	
	_sem_init:
	call _irq_off
	movff _PREINC1, _FSR0L
	movff _PREINC1, _FSR0H
	movf _PREINC1, F, A ;skip uper adr
	
	movlw SEMVALUSEMAX ;max-val
	subwf _PREINC1, W, A ;
	bnc _mut_03
	
	setf _PRODH, A ;return 0xFF
	goto _sem_init_endposter
	
	_mut_03:
	movff _INDF1, _POSTINC0
	clrf _POSTINC0, A
	clrf _PRODH, A
	
	_sem_init_endposter:
	movlw 4
	subwf FSR1L, F, A
	movf _PRODH, W, A
	
	call _irq_restore ;return from set stack
	
	return
	__endasm;
}

#endif

/*-----------------------------------------------*/
/* because mutex */
//extern sem_t mut1; //bed

#if FUNSEMASMPIC==0

//uint8_t sem_wait ( sem_t * semp ) __reentrant {
uint8_t sem_wait ( sem_t * semp ) {
	
	uint8_t nth, sem;

	irq_off();
	
	sem = semp->val;
	nth = thactiv;
	
	#if DEBUGRSSEM == 1
	RSoutP ('W'); RSoutP (nth+'0');
	#endif
	
	/* sem with 7 bit are mutex */
	/* always sleep  */
	
	if(sem==0x80) goto FAST_WAIT;
	if(sem&0x80) {
		semp->val = 0x80;
		goto FAST_WAIT_02;
	}
	
	if ( sem > SEMVALUSEMAX ) {
		irq_restore();
		return -1;
	}
	
	if ( sem == 0 ) {
		FAST_WAIT:
		/* wait */
		thsem[nth] = (uint16_t)semp;
		thstate[nth] = THST_WAIT;
		
		/* set interrupt */
		#if defined(__PIC18F25Q10_H__) || defined(_PIC18F25Q10_H_)
		PIR0 |= 1<<5;
		#else
		INTCONbits.TMR0IF=1;
		#endif
		
		#if DEBUGRSSEM == 1
		RSoutP ('S');
		#endif
		irq_restore();
		/* in this point we should stop */
		/* but if no stay wait check */
		while( thstate[nth] != THST_RUN ) {
			#if DEBUGRSSEM == 1
			RSoutP ('.');
			RSoutP (nth+'0');
			#endif
		}
		
	} else {
		semp->val = sem-1;
		FAST_WAIT_02:
		irq_restore();
	}
	
	//irq_off();
	#if DEBUGRSSEM == 1
	sem = semp->val;
	RSoutP ('A');
	RSoutP (nth+'0');
	RSoutP (sem+'0');
	#endif
	//irq_restore();
	
	return 0;
}

#endif

#if FUNSEMASMPIC==1

uint8_t sem_wait_GL ( sem_t * sem ) __reentrant __naked {

	sem;
	
	__asm
	global _sem_wait
	extern _thstate
	extern _save_point_stack
	
	_sem_wait:
	call _irq_off ;return from set stack
	movff _PREINC1, _FSR0L
	movff _PREINC1, _FSR0H ;fsr1->point to adr sem
	movf _POSTDEC1, W, A ;
	movf _POSTDEC1, W, A ;
	movff _FSR0L, _PRODL
	movff _FSR0H, _PRODH
	
	;movf _INDF0, W, A ;->value
	
	;if(sem==0x80) goto FAST_WAIT;
	MOVLW 0x80
	xorwf _INDF0, W, A ;zero ?
	bz _sem_wait_10
	
	btfss _INDF0, 7, A ;
	bra _sem_wait_03
	movlw 0x80
	andwf _INDF0, F, A ;
	bra _sem_wait_end
	
	_sem_wait_03:	
	MOVLW SEMVALUSEMAX
	SUBWF _INDF0, W, A
	bc _sem_wait_rb
	
	movf _INDF0, F, A ;zero ?
	bnz _sem_wait_dec

	_sem_wait_10:
	BANKSEL _thactiv
	clrf _FSR0H, A
	rlncf _thactiv, W, B ;size x2
	addlw low(_thsem)
	movwf _FSR0L, A
	movlw high(_thsem)
	addwfc _FSR0H, F, A

;save pinter
	movff _PRODL, _POSTINC0
	movff _PRODH, _POSTINC0
	
	clrf _FSR0H, A
	movf _thactiv, W, B ;size x1
	addlw low(_thstate)
	movwf _FSR0L, A
	movlw high(_thstate)
	addwfc _FSR0H, F, A

	movlw THST_WAIT
	movwf _INDF0, A
	
	;bsf _INTCON, _TMR0IF, A
	
	#if defined(__PIC18F25Q10_H__) || defined(_PIC18F25Q10_H_)
	BANKSEL _PIR0
	bsf _PIR0, 5, B;
	#else
	bsf _INTCON, 2, A
	#endif
	
	
	call _irq_restore
	
	_sem_wait_ptl:
	movf _INDF0, W, A
	xorlw THST_RUN
	bnz _sem_wait_ptl
	return
	
	
	_sem_wait_dec:
	decf _INDF0, F, A
	_sem_wait_end:
	call _irq_restore
	movlw 0
	return
	
	_sem_wait_rb:
	movlw 0xFF
	return
	
	__endasm;
}

#endif

/*-----------------------------------------------*/
/* pointer to eh smeafore is just semaphere */

#if FUNSEMASMPIC==0

//uint8_t sem_post ( sem_t * sem ) __reentrant {
uint8_t sem_post ( sem_t * sem ) {
	
	uint8_t i, j, semt;
	//uint16_t *ws16;
	
	/* this must be atomic */
	
	irq_off();
	
	semt = sem->val;
	if(semt==0x80) goto FAST_POST;
	if(semt&0x80) {
		sem->val = 0x81;
		goto THRRUNAGAIN;
	}
	
	#if DEBUGRSSEM == 1
	RSoutP ('P');
	RSoutP (thactiv+'0');
	#endif
	
	if ( semt < SEMVALUSEMAX ) {
		j = 7;
		/* if value 0 find number thread */
		if(semt == 0) {
			FAST_POST:
			//ws16 = thsem;
			i = sem->rut; //test for max threa??
			for(j=_THREADNUM;j&0xFF;--j) {
				if( ++i >= _THREADNUM ) i = 0;
				if(thsem[i]==(uint16_t)sem) {
					sem->rut = i;
					thsem[i] = 0;
					thstate[i] = THST_RUN;
					#if DEBUGRSSEM == 1
					RSoutP (i+'0');
					#endif
					goto THRRUNAGAIN;
				}
			}
		}
		sem->val += 1;
		
		THRRUNAGAIN:
		irq_restore();
		return 0;
	}

	irq_restore();
	
	return -1;
}
#endif

#if FUNSEMASMPIC==1

uint8_t sem_post_GL ( sem_t * sem ) __reentrant __naked {
	
	sem;
	
	__asm
	global _sem_post
	extern _thstate
	extern _save_point_stack
	
	_sem_post:
	call _irq_off ;return from set stack
	movff _PREINC1, _FSR0L
	movff _PREINC1, _FSR0H ;fsr1->point to adr sem
	
	movf _INDF0, W, A ;->value
	
	;if(semt==0x80) goto FAST_POST;
	MOVLW 0x80
	xorwf _INDF0, W, A
	bz _sem_mupost
	
	btfss _INDF0, 7, A
	bra _sem_post_02
	
	movlw 0x81
	movwf _INDF0, A
	bra _sem_post_end_40
	
	_sem_post_02:
	MOVLW SEMVALUSEMAX
	SUBWF _INDF0, W, A
	bc _sem_post_rb

	movf _INDF0, F, A ;zero ?
	bnz _sem_post_end_post

	_sem_mupost:
	movff _FSR2L, _save_point_stack
	movff _FSR2H, _save_point_stack+1
	
	;find thread
	movlw 1 
	;movf _PLUSW0, W, A ;->rut
	;movwf _PRODL, A
	movff _PLUSW0, _PRODL
	;clrf _PRODL, A
	
	;test for max thread??
	movlw _THREADNUM
	movwf _TABLAT, A ;ptl
	
	_sem_post_ptl_00:
	incf _PRODL, F, A
	movlw _THREADNUM
	subwf _PRODL, W, A
	bnc _sem_post_nrthok
	clrf _PRODL, A
	
	_sem_post_nrthok:
	clrf _FSR2H, A
	rlncf _PRODL, W, A ;size x2
	addlw low(_thsem)
	movwf _FSR2L, A
	movlw high(_thsem)
	addwfc _FSR2H, F, A
	;movwf _FSR2H, A
	
	;check FSR1 -> h pointer
	movff _POSTDEC1, _PRODH ;H
	movf _POSTINC1, W, A ;L
	xorwf _POSTINC2, W, A ;tst L
	bnz _sem_post_ptl_04
	movf _PRODH, W, A ; tst H
	xorwf _INDF2, W, A
	bz _sem_post_ptl_05
	
	_sem_post_ptl_04:
	decfsz _TABLAT, F, A
	goto _sem_post_ptl_00
	
	;movf _TABLAT, W, A
	;addlw '0'
	;not find return 0
	movff _save_point_stack, _FSR2L
	movff _save_point_stack+1, _FSR2H
	movlw 0
	bra _sem_post_end_post
	
	;je find thread _PRODL
	;clr adr
		;set state
	_sem_post_ptl_05:
	clrf _POSTDEC2, A ;H
	clrf _POSTDEC2, A ;L
	
	clrf _FSR2H, A
	movlw low(_thstate)
	addwf _PRODL, W, A
	movwf _FSR2L, A
	movlw high(_thstate)
	addwfc _FSR2H, F, A
	
	movlw THST_RUN
	movwf _INDF2, A

	;save _prodl rut
	movff _POSTDEC1,_FSR2H
	;movff _POSTDEC1, _FSR0L ;fsr1->point to adr sem
	incf POSTDEC1, W, A
	movwf _FSR2L, A
	bnz _sem_post_09
	incf _FSR2H, F, A
_sem_post_09:
	;movff _INDF2, _PRODL
	movff _PRODL, _INDF2
	movff _save_point_stack, _FSR2L
	movff _save_point_stack+1, _FSR2H
	movlw 0 ;find return 0
	bra _sem_post_end

	
	_sem_post_rb:
	movlw 0xFF
	bra _sem_post_end_40

	_sem_post_end_post:
	incf _INDF0, F, A ;inc sem
	_sem_post_end_40:
	movf _POSTDEC1, F, A ;stacak is reswtore upeer fun
	movf _POSTDEC1, F, A ;stacak is reswtore upeer fun
	_sem_post_end:
	call _irq_restore ;return from set stack
	return
	
	__endasm;
}

#endif

/*-----------------------------------------------*/

#if FUNSEMASMPIC==0

//uint8_t sem_getvalue ( sem_t *sem, uint8_t * valuep ) __reentrant {
uint8_t sem_getvalue ( sem_t *sem, uint8_t * valuep ) {
	
	*valuep = sem->val;
	
	return 0;
}

#endif

#if FUNSEMASMPIC==1

uint8_t sem_getvalue_GL ( sem_t *sem, uint8_t * valuep ) __reentrant __naked {
	
	sem;
	valuep;
	
	__asm
	global _sem_getvalue
	
	_sem_getvalue:
	call _irq_off ;return from set stack
	movff _PREINC1, _FSR0L
	movff _PREINC1, _FSR0H
	movf _PREINC1, F, A ;skip uper adr
	
	movf _INDF0, W, A
	
	movff _PREINC1, _FSR0L
	movff _PREINC1, _FSR0H
	movwf _INDF0, A
	;movf _PREINC1, F, A ;skip uper adr
	
	movlw 5
	subwf FSR1L, F, A
	
	call _irq_restore ;return from set stack
	
	return
	__endasm;
}

#endif

/*-----------------------------------------------*/
/* what do with thread with it ? */

uint8_t sem_destroy ( sem_t *sem ) {
	
	irq_off();
	sem->val = 0xFF;
	irq_restore();
	
	return 0;
}


/*-----------------------------------------------

-----------------------------------------------*/
