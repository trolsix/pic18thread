/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/

#if defined(_PIC18F1220_H_) || defined(__PIC18F1220_H__)
#ifdef _GETSTACK_
#pragma stack 200 55
#endif

#define THSTACSIZ00 32
#define THSTACSIZ01 24
#define THSTACSIZ02 10
#define THSTACSIZ03 10
#define THSTACSIZ04 10
#define THSTACSIZ05 0
#define THSTACSIZIH 16

#define THSTACADR00 255
#define THSTACADR01 (THSTACADR00-THSTACSIZ00)
#define THSTACADR02 (THSTACADR01-THSTACSIZ01)
#define THSTACADR03 (THSTACADR02-THSTACSIZ02)
#define THSTACADR04 (THSTACADR03-THSTACSIZ03)
#define THSTACADR05 (THSTACADR04-THSTACSIZ04)
#define THSTACADRIH (THSTACADR05-THSTACSIZ05)
#define THSTACADRFF (THSTACADRIH-THSTACSIZIH)

#else

#ifdef _GETSTACK_
//#pragma stack 774 250
//#pragma stack 362 150
#pragma stack 382 130
#endif

#define THSTACSIZ00 32
#define THSTACSIZ01 12
#define THSTACSIZ02 24
#define THSTACSIZ03 12
#define THSTACSIZ04 12
#define THSTACSIZ05 0
#define THSTACSIZIH 16

//#define THSTACADR00 1023
#define THSTACADR00 511
//#define THSTACADR00 255
#define THSTACADR01 (THSTACADR00-THSTACSIZ00)
#define THSTACADR02 (THSTACADR01-THSTACSIZ01)
#define THSTACADR03 (THSTACADR02-THSTACSIZ02)
#define THSTACADR04 (THSTACADR03-THSTACSIZ03)
#define THSTACADR05 (THSTACADR04-THSTACSIZ04)
#define THSTACADRIH (THSTACADR05-THSTACSIZ05)
#define THSTACADRFF (THSTACADRIH-THSTACSIZIH)

#endif