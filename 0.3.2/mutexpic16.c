/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/
/*-----------------------------------------------*/

#include <stdint.h>

/*-----------------------------------------------*/

#include "Bheadb.h"

#include "mutexpic16.h"
#include "semaphorespic16.h"

void RSoutP (unsigned char a) _MYMODFUN_;

/*-----------------------------------------------*/

uint8_t mutex_init ( mutex_t * sem ) __reentrant {
	
	sem->val = 0x81;
	sem->rut = 0;
	return 0;
}
	
/*-----------------------------------------------*/
/* like sem id 1 -> 0 */

//uint8_t sem_wait ( sem_t * semp ) __reentrant;

/*
uint8_t mutex_lock ( mutex_t *sem ) __reentrant __naked {
	uint8_t a;
	irq_off();
	//RSoutP ('L');
	//RSoutP (sem->val+'0');
	a = sem_wait ( (void*)sem );
	irq_restore();
	return a;
}*/

/*-----------------------------------------------*/
/* it seems like sem post but set always 1 */

uint8_t mutex_unlock ( mutex_t *sem ) __reentrant __naked {
	
	//first post
	sem;
	
	__asm
	extern _sem_post
	extern _irq_off
	extern _irq_restore

;	call _irq_off
	call _sem_post ;modyfi for always 1 or lock for dont check
;	call _irq_restore
	return

	__endasm;
	
	//second set 1
	//irq_off();
	//RSoutP ('U');
	//RSoutP (sem->val+'0');
	//sem->val = 1;
	//irq_restore();
	//return 0;	
}

/*-----------------------------------------------*/

uint8_t mutex_destroy ( mutex_t *sem ) __reentrant __naked {

	sem;
	
	__asm
	extern _sem_destroy
	goto _sem_destroy
	__endasm;
}

/*-----------------------------------------------*/


