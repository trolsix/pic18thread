#<meta http-equiv="content-type" content="text/html; charset=utf-8">
#<?xml version='1.0' encoding='utf-8'?>

------------------------

Wersja testowa na pic18f1220 z blokową pamięcią zewnętrzną

Dla kwarcu 24MHz -> 6MHz cykle rozkazowe

------------------------

W tej chwili kod zakłada że bloki są 512 bajtowe, a magistrala 4-bit.

------------------------
konfiguracja:
------------------------

plik threadpic16conf.h

włączamy wsparcie dla pamięci zewnętrznej
#define SAVE_IN_RAM_EXT  1

określić ilość rejestrów (taka sama dla wszystkich wątków) np:
#define SIZREGSAVE 16

prescaler
#define _PRESCALER_T0 128

plik threadpic16stckconf.h

stos na całość (max na jeden wątek)(start rozmiar) np:
#pragma stack 196 60

górny adres stosu (start+rozmiar-1)
#define THSTACADR00 255

------------------------


