/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/

/*-----------------------------------------------*/

#include "Bheadb.h"
#include <stdint.h>

/*-----------------------------------------------*/

#define UART1BUF 16
#define UART1MSK (UART1BUF-1)
#define UART1FIL (UART1BUF-2)

/*-----------------------------------------------*/

static volatile uint8_t wsrsbufin;
static volatile uint8_t wsrsbufou;
static char rsbuf[UART1BUF];

/*-----------------------------------------------*/
/* 1 high 2 low*/
//https://sourceforge.net/p/sdcc/bugs/2202/

#define THREAD_USE_LOW_PRIORITY 1
#if THREAD_USE_LOW_PRIORITY==1

void tc_int (void) __shadowregs __interrupt 1 {
//void tc_int (void) __interrupt 1 {
	
#if defined(__PIC18F25Q10_H__) || defined(_PIC18F25Q10_H_)
	if ( PIR3 & 0x10 ) {
		/* bufor empty disble */
		if ( wsrsbufin == wsrsbufou ) {
			PIE3 &= ~(0x10);
		} else {
			TX1REG = rsbuf[wsrsbufou];
			wsrsbufou = (1+wsrsbufou) & UART1MSK;
		}
	}
#endif
#if defined(_PIC18F1220_H_) || defined(__PIC18F1220_H__) || \
  defined(_PIC18F26K22_H_) || defined(__PIC18F26K22_H__) || \
  defined(_PIC18F4431_H_) || defined(__PIC18F4431_H__)

	//if ( ( PIE1bits.TXIE ) && ( PIR1bits.TXIF ) ) {
	if(PIR1bits.TMR1IF) {
		PIR1bits.TMR1IF=0;
	}
	//PIE1bits.TXIE = 0;
	//TXSTAbits.TXEN = 0; //rsoff
	if ( PIR1bits.TXIF ) {
		if ( wsrsbufin == wsrsbufou ) {
			PIE1bits.TXIE = 0;
		} else {
			TXREG = rsbuf[wsrsbufou];
			wsrsbufou = (1+wsrsbufou) & UART1MSK;
		}
	}
#endif
}

#endif

/*-----------------------------------------------*/

void setrs ( void) {
	wsrsbufin = 0;
	wsrsbufou = 0;
#if defined(__PIC18F25Q10_H__) || defined(_PIC18F25Q10_H_)
	SP1BRGL = 103;
	SP1BRGH = 0;
	
	RC1STA = 0b10010000; //spen cren
	TX1STA = 0b00000100; // txen off BRGH
	BAUD1CON = 0b00001000; // brg16
#endif
}

/*-----------------------------------------------*/

void RSoutP (unsigned char a) _MYMODFUN_ {
	while(1){
		uint8_t howfill;
		howfill = (wsrsbufin-wsrsbufou) & UART1MSK;
		if ( howfill < UART1FIL ) break;
	}
	rsbuf[wsrsbufin] = a;
	wsrsbufin = (1+wsrsbufin) & UART1MSK;
#if defined(_PIC18F1220_H_) || defined(__PIC18F1220_H__) || \
	defined(_PIC18F26K22_H_) || defined(__PIC18F26K22_H__) || \
  defined(_PIC18F4431_H_) || defined(__PIC18F4431_H__)
	PIE1bits.TXIE = 1;
	TXSTAbits.TXEN = 1; //rson
#endif
#if defined(__PIC18F25Q10_H__) || defined(_PIC18F25Q10_H_)	
	PIE3 |= 0x10;
	TX1STAbits.TXEN = 1; //rson
	//TX1STA = 0b00000100; // txen off BRGH
#endif
}

/*-----------------------------------------------*/

void RSouttbl ( const unsigned char *tblf, unsigned char siz) __reentrant {
	while(siz) { RSoutP ( tblf[--siz] ); if(siz==0) return; }
	while(*tblf) { RSoutP (*tblf); ++tblf; }
}	

/*-----------------------------------------------*/
