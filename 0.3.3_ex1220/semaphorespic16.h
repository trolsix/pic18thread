/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/
/*-----------------------------------------------*/

#include <stdint.h>

/*-----------------------------------------------*/

#include "Bheadb.h"
#include "threadpic16.h"
//#include "threadpic16conf.h"

/*-----------------------------------------------*/

#define SEMAPHORES_MAX      4
#define SEMVALUSEMAX      120

//typedef uint8_t sem_t;

typedef struct {
	uint8_t val;
	uint8_t rut;
} sem_t;

/* value, ruting */

/*-----------------------------------------------*/

uint8_t sem_init ( sem_t * sem, uint8_t initvalue ) __reentrant;
//uint8_t sem_init ( sem_t * sem, uint8_t initvalue );
//uint8_t sem_wait ( sem_t *sem ) __reentrant;
uint8_t sem_wait ( sem_t *sem );
//uint8_t sem_post ( sem_t *sem ) __reentrant;
uint8_t sem_post ( sem_t *sem );
//uint8_t sem_getvalue ( sem_t *sem, uint8_t * valuep ) __reentrant;
uint8_t sem_getvalue ( sem_t *sem, uint8_t * valuep );
uint8_t sem_destroy ( sem_t *sem );

/*
void sem_sys_init ( void );
uint8_t sem_init ( sem_t * sem, uint8_t initvalue );
uint8_t sem_wait ( sem_t sem ) _MYMODFUN_ __reentrant;
uint8_t sem_post ( sem_t sem ) _MYMODFUN_ __reentrant;
uint8_t sem_getvalue ( sem_t sem, uint8_t * valuep ) __reentrant;
uint8_t sem_destroy ( sem_t sem ) _MYMODFUN_;
*/
//uint8_t sem_post_jp ( sem_t * semp ) __reentrant;

/*-----------------------------------------------

-----------------------------------------------*/
