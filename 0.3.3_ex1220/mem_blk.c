/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/

/*-----------------------------------------------*/

#include "Bheadb.h"
#include <stdint.h>

#define _CLRCBIT 0x01
#define _WRITBIT 0x02
#define _SBLKBIT 0x04
#define _IN48BIT 0x04
#define _ADRTBIT 0x08

//void regset ( void ) { LATB |= 1<<0; }
//void regclr ( void ) { LATB &= ~(1<<0); }
//void clkset ( void ) { LATB |= 1<<2; }
//void clkclr ( void ) { LATB &= ~(1<<2); }
//void phim ( void ) { TRISB = (1<<1) | (1<<4) | _RB3 | (1<<5) | (1<<6) | (1<<7); }
//void plim ( void ) { TRISB = (1<<1) | (1<<4); }

#define regset() { LATB |= 1<<2; }
#define regclr() { LATB &= ~(1<<2); }
#define clkset() { LATB |= 1<<0; }
#define clkclr() { LATB &= ~(1<<0); }
#define phim() { TRISB = (1<<1) | (1<<4) | _RB3 | (1<<5) | (1<<6) | (1<<7); }
#define plim() { TRISB = (1<<1) | (1<<4); }

/*-----------------------------------------------*/
/*   hehe RB 7653 2 0 */
/* ADR */

void setdata4 ( uint8_t d ) _MYMODFUN_ {

uint8_t tmp;
	
	d <<= 4;
	if(d&0x10) d |= 0x08;

	tmp = LATB;	
	tmp &= 0b00010111;
	d &= 0b11101111;
	
	tmp |= d;
	LATB = tmp;
}
/*
void setdata4clk ( uint8_t d ) _MYMODFUN_ {
	uint8_t tmp;
		
	tmp = LATB;
	tmp &= 0b00010111;
	if(d&0x01) tmp |= 0x08;
	d &= 0b00001110;
		d <<= 4;
	tmp |= d;
	LATB = tmp;

	clkset();
	clkclr();
}
*/

/*-----------------------------------------------*/

static void clkclk (void) {
	clkset();
	clkclr();
}

/*-----------------------------------------------*/

#define SHOWMEME   0
#define TESTINRAM  0

/*-----------------------------------------------*/

#if TESTINRAM==1
//static uint8_t __at(0x150) sium[80];
static uint8_t sium[80];
static uint8_t wsd;
#endif

/*-----------------------------------------------*/

void sendhex (unsigned char a) _MYMODFUN_ ;
void RSoutP (unsigned char a) _MYMODFUN_;

/*-----------------------------------------------*/

void blkset ( uint8_t blkadr ) _MYMODFUN_ {

	regset ();
	plim ();
	setdata4 ( _WRITBIT );
	clkclk();
	setdata4 ( _ADRTBIT | (blkadr>>6 ) );
	clkclk();
	setdata4 ( _ADRTBIT | (blkadr>>3 ) );
	clkclk();
	setdata4 ( _ADRTBIT | (blkadr>>0 ) );
	clkclk();
	
	setdata4 ( 0 );
	phim();
	regclr();

#if TESTINRAM==1
	wsd = 0;
#endif
}

/*-----------------------------------------------*/
/* with clr counter */
void readset (void){
	regset();
	plim();
	setdata4 ( _CLRCBIT );
	clkclk();
	phim();
	setdata4 ( 0 );
	regclr();
}

/*-----------------------------------------------*/

void writeset(void) {
	regset();
	plim();
	setdata4 ( _WRITBIT | _CLRCBIT );
	clkclk();
	setdata4 ( 0 );
	regclr();
}

/*-----------------------------------------------*/

uint8_t ramread ( void ) {
	
	uint8_t d,f;
	
#if TESTINRAM==1
	if ( wsd >= 80 ) {
		RSoutP ('W');
		return 0;
	}

	d = sium[wsd++];
#if SHOWMEME==1
	sendhex (d);
#endif
	return d;
	
#else
	
/*	8-bit
	if(flagram&0x01) {
		clkset();
		d = PINC & 0x0F;
		d <<= 4;
		d |= PINB & 0x0F;
		clkclr();
		return d;
	}*/
	
//	++ble;
//	return ble;
	
	d = PORTB;
	clkset(); //need delay beetween read f
	
	if(d&0x08) d |= 0x10;
	else d &= ~0x10;
	
	f = PORTB;
	if(f&0x08) f |= 0x10;
	else f &= ~0x10;
	
	f >>= 4;
	d &= 0xF0;
	d |= f;
	clkclr();

#if SHOWMEME==1
	sendhex (d);
#endif
	return d;
#endif
}

/*-----------------------------------------------*/

void ramwrit ( uint8_t dw ) _MYMODFUN_ {
	
/*	8-bit	
	if(flagram&0x01) {
		PORTB = (PORTB&0xF0) | ((d)&0x0F);
		PORTC = (PORTC&0xF0) | ((d>>4)&0x0F);
		clkset();
		delay
		clkclr();
		return;
	}
	*/
#if TESTINRAM==1
	if ( wsd >= 80 ) {
		RSoutP ('W');
		return;
		}
#if SHOWMEME==1
	RSoutP (';');
	sendhex (dw);
#endif
	sium[wsd++] = dw;
#else

#if SHOWMEME==1
	sendhex (dw);
#endif

	setdata4 ( dw >> 4 );
	clkset();
	setdata4 ( dw ); /* from set 6 cycles */

	/* delay time for write for 10MHz 200ns 5 cycles */
	/*
	__asm
		nop
		nop
		nop
		nop
		nop
	__endasm;
	*/
	clkclr();
#endif
}

/*-----------------------------------------------*/

