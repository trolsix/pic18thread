/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/

/*-----------------------------------------------
       thread example
-----------------------------------------------*/

#include "Bheadb.h"

/*-----------------------------------------------*/

#include <stdio.h>
#include <stdint.h>

#include "nothing.h"
#include "threadpic16.h"
#include "threadpic16conf.h"
#include "semaphorespic16.h"
#include "mutexpic16.h"
#include "threadpic16stckconf.h"

#include "mem_blk.h"

/*-----------------------------------------------*/
/*    config   fuse  hardware uC                 */
/*-----------------------------------------------*/

#include "Bconf.h"

/*-----------------------------------------------*/

void setrs ( void);
void RSoutP (unsigned char a) _MYMODFUN_;
void RSoutB (unsigned char a) _MYMODFUN_;
void initport (void);

/*-----------------------------------------------*/

void RSouttbl ( const unsigned char *tblf , unsigned char siz ) __reentrant;

/*-----------------------------------------------*/

//void binbcd1 ( char * tbl, uint32_t a07 ) __reentrant;
uint8_t binbcd1 ( void );
void sendhex (unsigned char a) _MYMODFUN_;

/*-----------------------------------------------*/

sem_t sembcd;
sem_t semmain;

/*-----------------------------------------------*/

//uint16_t timer[2];
static char tbl[11];
volatile uint8_t thrend;
//uint16_t timthr;
//uint32_t timsum[2];

/*-----------------------------------------------*/
/*
#if DUMPSTACKH || DUMPSATCKS
uint8_t stack_dump[93];
#endif
*/
/*-----------------------------------------------*/

static uint16_t tickf1[10];

/*-----------------------------------------------*/

/*-----------------------------------------------*/
/* example */

extern uint8_t thactiv;
extern volatile uint8_t thstate[];
extern volatile uint16_t thsem[];

static uint8_t myfun1 ( void ) __naked __reentrant {
	
	while(1) {
		uint8_t wskdata;
		
		wskdata = thactiv - 2;
		tickf1[wskdata] += 1;

		/* make some times something */
		for(wskdata=0;wskdata<240;++wskdata) {
			EEADR;
			EEDATA;
		}
		
		if(thrend) break;
	}
	
	INTCON &= ~0x40;
	RSoutP ('-');
	RSoutP (thactiv+'0');
	RSoutP ('-');
	INTCON |= 0x40;
	__asm
		movlw 0x00
		RETURN
	__endasm;
	//return 1;
}

/*-----------------------------------------------*/

void RSoutB (unsigned char a) _MYMODFUN_ {
	uint8_t i;
	
	for ( i=0x80; i; i >>= 1 ) {
		if(a&i) RSoutP ('1');
		else RSoutP ('0');
	}
	RSoutP (' ');
}

/*-----------------------------------------------*/
/* send stack with portion  50 */
#if DUMPSATCKS
void sendmem ( uint8_t * wsk ) {
	uint8_t i;
	
	INTCON &= ~0x80;
	for(i=0;i<50;++i ) {
		stack_dump[i] = *wsk;
		--wsk;
	}
	INTCON |= 0x80;
	
	//send
	//RSoutP (10);
	for(i=0;i<50;){
		sendhex (stack_dump[i]);
		RSoutP (' ');
		++i;
		if(0==(i&0x1F))RSoutP (10);
	}
	RSoutP (10);
}
#endif


volatile uint8_t blkadr;

/*-----------------------------------------------*/

extern uint32_t ul;
extern char * tbll;

#define BINASI(PARAM1,PARAM2) INTCON &= ~0x40; ul=PARAM2; tbll=PARAM1; rungetbcd()

/*-----------------------------------------------*/
/* call void funkcion is efective */

void runbcd ( void ) {
	sem_post(&sembcd);
	INTCON|=0x40;
}

void rungetbcd ( void ) {
	sem_post(&sembcd);
	INTCON|=0x40;
	sem_wait(&semmain);
}

/*-----------------------------------------------*/
//const char tstop[10]   = "TH1 STOP\n";
//const char tstart[11]  = "TH1 START\n";
//const char t1eg[11]    = "TH1 EGAIN\n";

const char what[]    = "\nExample v 0.3.3\n";

volatile unsigned char * wskchar;

/*-----------------------------------------------*/

void RStbl10 (void) {
	RSouttbl ( tbl, 10 );
}

void binbcd2 ( char * tbl, uint32_t ul );

/*-----------------------------------------------*/

int main ( void ) {

	static uint32_t tick;
	uint8_t blknr, i;

	TMR0L = 0;
	TMR1L = 0;
	TMR1H = 0;
	blknr = 0;
	
	for ( i=0; i<sizeof(tickf1)/sizeof(tickf1[0]); ++i )
		tickf1[i] = 0;

	tick = 0;
	
	initport();
	setrs();
	initth();
	irq_on();

	CLRWDT();

	RSouttbl (what,0);
	
	thrend = sem_init ( &semmain, 0 );
	thrend = sem_init ( &sembcd, 0 );
	
	thrend = 0;
	//INTCON &= ~0x40;
	if( 1==startthvoidn ( &binbcd1, 1 )) RSoutP ('1');
	else RSoutP ('B');
	
	for ( i=2; i<2+sizeof(tickf1)/sizeof(tickf1[0]); ++i ) {
		if( i == startthvoidn ( &myfun1, i )) RSoutP (i+'0');
		else RSoutP ('B');
	}
	RSoutP ('\n');
	INTCON |= 0x40;
	
	while(1) {
		CLRWDT();
		#if DEBUG_ADRFSR1
		if(FSR1L!=(0xFF&THSTACADR00)) {
			INTCON &= ~0x40;
			RSoutP ('*');
			RSoutP ('0');
			RSoutP ('*');
			RSoutP ('\n');
			while(1);
		}
		#endif
		
		++tick;
	
		//dumpstackwithirq
		//a mamy amlo memeory a chcemy znac stos
		#define DUMPSTACKHWIRQ 0
		#define DUMPSTACKSFIRQ 0
		if ( (0x7FFF & tick) == 1) {
			#include "sutil/dumphwstacksend.h"
		}
		
		if ( (0x7FFF & tick) == 2) {
			#include "sutil/dumpsfstacksend.h"
		}
		
		if ( (0x7FFF & tick) == 10) {
			BINASI ( tbl, tick );
			INTCON &= ~0x40;
			RSoutP ('T'); RSoutP ('0'); RSoutP (':');
			RStbl10();
			RSoutP (' ');
			INTCON |= 0x40;
			
			for( i=0; i<sizeof(tickf1)/sizeof(tickf1[0]); ++i ) {
				BINASI ( tbl, tickf1[i] );
				INTCON &= ~0x40;
				RSoutP ('Z'); RSoutP (i+'0'); RSoutP (':');
				RSouttbl ( tbl, 7 );
				RSoutP (' ');
				INTCON |= 0x40;
			}
			RSoutP ('\n');

		}
	}
	
//	while(1);
}

/*-----------------------------------------------*/

void sendhex (unsigned char a) _MYMODFUN_ {
	uint8_t tmp;
	
	tmp = a>>4;
	tmp &= 0x0F;
	if(tmp>9) tmp += 'a' - 10;
	else tmp += '0';
	RSoutP (tmp);
	
	tmp = a & 0x0F;
	if(tmp>9) tmp += 'a' - 10;
	else tmp += '0';
	RSoutP (tmp);
}


/*-----------------------------------------------*/

