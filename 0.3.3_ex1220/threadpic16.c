/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/
/*-----------------------------------------------

 pic18**** thread

-----------------------------------------------*/

#include <stdint.h>

#define GETTIMEIRQL    0
#define FUNASMPIC      0
#define _GETSTACK_     1

/*-----------------------------------------------*/

#define REGISSTACK (18-ADDREGSTACK)
#define REGISSAVE ADDREGSTACK

#define DONT_SAVE_U_HS   0
#define SHOW_DBG_MEMEXT  0

/*-----------------------------------------------*/

#include "Bheadb.h"

#include "threadpic16stckconf.h"
#include "threadpic16conf.h"
#include "threadpic16.h"
#include "semaphorespic16.h"

#include "mem_blk.h"

/*-----------------------------------------------*/
#if GETTIMEIRQL==1
extern uint16_t timer[2];
extern uint32_t timsum[2];
#endif
void startthvoid1end(void);

/*-----------------------------------------------*/

#if THREAD_USE_LOW_PRIORITY==0
void  tch_int (void) __interrupt 1 __naked {
#else
void  tch_int (void) __interrupt 2 __naked {
#endif
	__asm
	goto _tch_int_jump
	__endasm;
}

/*-----------------------------------------------*/

void RSoutP (unsigned char a) _MYMODFUN_;
void RSoutB (unsigned char a) _MYMODFUN_;

void thstop_endfun ( void );

/*-----------------------------------------------*/
/* for space placed this into flash in low cost version*/

#if STACKADR_IN_FLASH == 0
#error config STACKADR_IN_FLASH - now stack can be only in flash
#if _THREADNUM==2
uint16_t thstckadr[_THREADNUM] =  { THSTACADR00, THSTACADR01 };
#endif
#if _THREADNUM==3
uint16_t thstckadr[_THREADNUM] =  { THSTACADR00, THSTACADR01, THSTACADR02 };
#endif
#else
uint8_t thstckadr;
#endif

//uint8_t thnumregsave[_THREADNUM];
//uint8_t thdump[_THREADNUM];
//uint8_t threturn[_THREADNUM];
//uint8_t thnumber;

#if SAVE_IN_RAM_EXT==0
static uint8_t thdump00[SIZEDNMPLACES];
#endif
uint8_t thactiv ;

volatile uint8_t thstate[_THREADNUM];

/* adr sem wait */
volatile uint16_t thsem[_THREADNUM];

static uint8_t thflag;

/*
0 irq semaphores
1 SAVE or RESTORE in irq
6 copy intcon state
7 copy intcon state
*/

//static uint8_t stksav;
static uint8_t registcount;

/* use temporaty for stack or other things */
uint16_t save_point_stack;

/*
#if STACK_FOR_IRQ == 1
uint16_t save_point_stack;
#endif
*/
/*-----------------------------------------------*/

static uint8_t setset ( void ) __naked;
void cpp0to2 ( void ) _MYMODFUN_ __naked;

/*-----------------------------------------------*/
/*
	testt with stack for irqh
*/

void sendhex (unsigned char a) _MYMODFUN_;

void  tch_int_jump (void) __naked {

#include "z_OFF_IRQ.inc"

	__asm
		clrwdt
	__endasm;

	__asm
	#if GETTIMEIRQL==1
		bcf _T1CON,0, 0
		MOVFF	_TMR1L, _timer
		MOVFF	_TMR1H, _timer+1
		bsf _T1CON,0, 0
	#endif

/* ;lock irq for this 3 */
		MOVFF	_STATUS, _POSTDEC1
		MOVFF	_BSR, _POSTDEC1
		MOVWF	_POSTDEC1
		MOVFF	_FSR0L, _POSTDEC1
		MOVFF	_FSR0H, _POSTDEC1
		MOVFF _TABLAT, _POSTDEC1
		MOVFF _TBLPTRL, _POSTDEC1
		MOVFF _TBLPTRH, _POSTDEC1
		MOVFF _TBLPTRU, _POSTDEC1
		MOVFF _PRODL, _POSTDEC1
		MOVFF _PRODH, _POSTDEC1
		MOVFF _PCLATH, _POSTDEC1
		MOVFF _PCLATU, _POSTDEC1
		MOVFF	_FSR2L, _POSTDEC1
		MOVFF	_FSR2H, _POSTDEC1

		
	;save stack main for show it
	;	movf _thactiv,F
	;	bnz only_0
	;	MOVFF _FSR1L, _swsk
	;	MOVFF _FSR1H, (_swsk+1)	
	;	only_0:

	#if STACK_FOR_IRQ==1 && SAVE_IN_RAM_EXT==0
		;beckup pointer
		MOVFF	_FSR1L, _save_point_stack
		MOVFF	_FSR1H, _save_point_stack+1
		;set new
		MOVLW LOW(THSTACADRIH)
		movwf _FSR1L
		MOVLW HIGH(THSTACADRIH)
		movwf _FSR1H
	#endif

	__endasm;


#include "z_ON_IRQ.inc"

#if SHOW_DBG_MEMEXT==1
	RSoutP ('\n');
	sendhex ( thactiv );
	sendhex ( FSR1L );
	sendhex ( STKPTR );
	RSoutP ('<');
	while(PIE1bits.TXIE);
#endif
	

	/* time for threads */
	#if defined(__PIC18F25Q10_H__) || defined(_PIC18F25Q10_H_)
	if(PIR0&(1<<5)) {
		PIR0 &= ~(1<<5);
	#else
	if(INTCONbits.TMR0IF) {
		INTCONbits.TMR0IF = 0;
	#endif
	
	#if SAVE_IN_RAM_EXT==1
		__asm
			BANKSEL _registcount
			movlw (SIZREGSAVE)
			movwf _registcount, B
			goto _JEJE01
			_RJEJE01:
		__endasm;
	#else

		/* save kontest */
		__asm
			movff _thactiv, _TABLAT
			call _setset ;W <- NUMPARAM00
			BANKSEL _registcount
			addlw (256-REGISSAVE)
			movwf _registcount, B
		__endasm;


		__asm
	#if STACK_FOR_IRQ==0
			MOVFF _FSR1L, _POSTINC0
			MOVFF _FSR1H, _POSTINC0
	#else
			MOVFF _save_point_stack, _POSTINC0
			MOVFF _save_point_stack+1, _POSTINC0
	#endif
		__endasm;
	
		
		__asm
			movf _STKPTR, W, A
			movwf _POSTINC0, A
			;MOVFF _PCLATH, _POSTINC0
			;MOVFF _PCLATU, _POSTINC0
			;MOVFF _PRODL, _POSTINC0
			;MOVFF _PRODH, _POSTINC0
			
			movff _FSR0L, _FSR2L
			movff _FSR0H, _FSR2H
			clrf _FSR0L
			clrf _FSR0H
			call _cpp0to2

		__endasm;
#endif

#if SHOW_DBG_MEMEXT==1
		RSoutP ('\n');
#endif
		
		while(1){
			if ( ++thactiv >= _THREADNUM ) {
				thactiv = 0;
			}
			TMR0L = 0;
			//RSoutP (' ');
			//sendhex ( thactiv );
			//sendhex ( thstate[thactiv] );
			if ( thstate[thactiv] == THST_RUN ) break;
			
		}
		//RSoutP ('\n');
		thflag |= 0x02;
	}
	

	#if defined(__PIC18F25Q10_H__) || defined(_PIC18F25Q10_H_)
	#else
	if(PIR1bits.TMR1IF) {
		PIR1bits.TMR1IF=0;
	}
	#endif
	
	/* ****************************************************** */
	
#if FUN_IRQ_THREAD_ALLOWED==1	

	/* in this place you can past call void funkcjon, example:

	switch_test();

	*/







	
	
	
	
#endif

	/* ****************************************************** */	

	if( thflag & 0x02 ) {
		thflag &= ~0x02;

#if SHOW_DBG_MEMEXT==1
		RSoutP ('\n');
		sendhex ( thactiv );
		sendhex ( FSR1L );
		sendhex ( STKPTR );
		RSoutP ('>');
		while(PIE1bits.TXIE);
#endif
		
#if SAVE_IN_RAM_EXT==1
	__asm
		BANKSEL _registcount
		movlw (SIZREGSAVE)
		movwf _registcount, B
		goto _JEJE02
		_RJEJE02:
	__endasm;
#else
	/* restore kontest */
	__asm
		movff _thactiv, _TABLAT
		call _setset
		BANKSEL _registcount
		addlw (256-REGISSAVE)
		movwf _registcount, B
	__endasm;

#include "z_OFF_IRQ.inc"
#if STACK_FOR_IRQ == 0
	__asm
		MOVFF _POSTINC0, _FSR1L
		MOVFF _POSTINC0, _FSR1H
		MOVFF _POSTINC0, _STKPTR
	__endasm;
#endif
#if STACK_FOR_IRQ==1
	__asm
		MOVFF _POSTINC0, _save_point_stack
		MOVFF _POSTINC0, _save_point_stack+1
		MOVFF _POSTINC0, _STKPTR
		__endasm;
#endif	
#include "z_ON_IRQ.inc"

	__asm	
		;MOVFF _POSTINC0, _PCLATH
		;MOVFF _POSTINC0, _PCLATU
		;MOVFF _POSTINC0, _PRODL
		;MOVFF _POSTINC0, _PRODH

		clrf _FSR2L
		clrf _FSR2H
		
		call _cpp0to2
		
	__endasm;
#endif

#if SHOW_DBG_MEMEXT==1
	RSoutP ('\n');
#endif

	TMR0L = 0;
	INTCONbits.TMR0IF = 0;
	}
	
#include "z_OFF_IRQ.inc"

	__asm
		
	#if STACK_FOR_IRQ==1 && SAVE_IN_RAM_EXT==0
		;restore stack
		MOVFF _save_point_stack, _FSR1L
		MOVFF _save_point_stack+1, _FSR1H
	#endif

		MOVFF _PREINC1, _FSR2H
		MOVFF _PREINC1, _FSR2L	
		MOVFF _PREINC1, _PCLATU
		MOVFF _PREINC1, _PCLATH
		MOVFF _PREINC1, _PRODH
		MOVFF _PREINC1, _PRODL
		MOVFF _PREINC1, _TBLPTRU
		MOVFF _PREINC1, _TBLPTRH
		MOVFF _PREINC1, _TBLPTRL
		MOVFF _PREINC1, _TABLAT
		MOVFF _PREINC1, _FSR0H
		MOVFF _PREINC1, _FSR0L
		
		#if GETTIMEIRQL==1
		bcf _T1CON,0
		BANKSEL _timer
		;MOVFF	_TMR1L, _timer+2
		;MOVFF	_TMR1H, _timer+3
		comf _timer,F,B
		comf _timer+1,F,B ;should + 1 yet but ... ;)
		MOVF	_TMR1L, W
		addwf _timer,F,B
		MOVF	_TMR1H, W
		addwfc _timer+1,F,B
		
		movlw low(_timsum)
		movwf _FSR2L, A
		movlw high(_timsum)
		movwf _FSR2H, A
		
		movlw 1
		addwf _POSTINC2,F,A
		movlw 0
		addwfc _POSTINC2,F,A
		addwfc _POSTINC2,F,A
		addwfc _POSTINC2,F,A
		
		MOVF _timer, W
		addwf _POSTINC2,F,A
		MOVF _timer+1, W
		addwfc _POSTINC2,F,A
		movlw 0
		addwfc _POSTINC2,F,A
		addwfc _POSTINC2,F,A
		
		bsf _T1CON,0		
		#endif

		MOVF  _PREINC1, W
		MOVFF _PREINC1, _BSR
		MOVFF _PREINC1, _STATUS
	__endasm;

#include "z_ON_IRQ.inc"
	__asm
		RETFIE
	__endasm;
}

/*-----------------------------------------------*/

void initth ( void ) {
	
	thactiv = 0;
	thflag = 0;

	thstate[0] = THST_RUN;
	thsem[0] = 0;
#if SAVE_IN_RAM_EXT==0
#if _THREADNUM>1
	thstate[1] = 0xFF;
	thsem[1] = 0;
#endif
#if _THREADNUM>2
	thstate[2] = 0xFF;
	thsem[2] = 0;
#endif
#if _THREADNUM>3
	thstate[3] = 0xFF;
	thsem[3] = 0;
#endif
#if _THREADNUM>4
	thstate[4] = 0xFF;
	thsem[4] = 0;
#endif
#if _THREADNUM>5
	thstate[5] = 0xFF;
	thsem[5] = 0;
#endif
#else
	{
		uint8_t i;
		for ( i=1; i<_THREADNUM; ++i ) {
			thstate[i] = 0xFF;
			thsem[i] = 0;
		}
	}
#endif


#if defined(__PIC18F25Q10_H__) || defined(_PIC18F25Q10_H_)
#if _PRESCALER_T0 == 16
	T0CON1 = 0b01000100;
#endif
#if _PRESCALER_T0 == 32
	T0CON1 = 0b01000101;
#endif
#if _PRESCALER_T0 == 64
	T0CON1 = 0b01000110;
#endif
#if _PRESCALER_T0 == 128
	T0CON1 = 0b01000111;
#endif
#if _PRESCALER_T0 == 256
	T0CON1 = 0b01001000;
#endif

//enable timer 8 bit
	T0CON0 = 0b10100000;
//8 bit mode is reset
	TMR0H = 255;
#if THREAD_USE_LOW_PRIORITY==1
	IPR0 &= ~(1<<5); //low prioryty
#endif
	PIE0 |= 1<<5; //interrupt enable

#else

// 4431
#if defined(_PIC18F4431_H_) || defined(__PIC18F4431_H__)
#define _T08BIT _T016BIT 
#endif

//timer 0, prescaler 8
//	T0CON = _TMR0ON | _T0PS1 | _T08BIT;
#if _PRESCALER_T0 == 16
//timer 0, prescaler 16 irqcykl 4096 588 dla 2MHz
	T0CON = _TMR0ON | _T0PS1 | _T0PS0 | _T08BIT;
#endif
#if _PRESCALER_T0 == 32
//timer 0, prescaler 32 irqcykl 8192 244 dla 2MHz
	T0CON = _TMR0ON | _T0PS2 | _T08BIT;	
#endif
#if _PRESCALER_T0 == 64
//timer 0, prescaler 64 irqcykl 16384 122 dla 2MHz
	T0CON = _TMR0ON | _T0PS2 | _T0PS0 | _T08BIT;
#endif
#if _PRESCALER_T0 == 128
//timer 0, prescaler 128 irqcykl 32k 60 dla 2MHz
	T0CON = _TMR0ON | _T0PS2 | _T0PS1 | _T08BIT;
#endif
#if _PRESCALER_T0 == 256
//timer 0, prescaler 256 irqcykl 64k 30 dla 2MHz
	T0CON = _TMR0ON | _T0PS2 | _T0PS1 | _T0PS0 | _T08BIT;
#endif

	INTCONbits.TMR0IE = 1;
#if THREAD_USE_LOW_PRIORITY==1
	INTCON2bits.TMR0IP = 0; //low prioryty
#endif

#endif
}

/*-----------------------------------------------*/

#if FUNASMPIC==0

uint8_t threadstop ( uint8_t nth ) _MYMODFUN_ {
	
	if ( ( nth >= _THREADNUM ) ||
	   ( nth == 0 ) ||
	/* thread with bit 7 are not change */
	   ( thstate[nth] & 0x80 ) ) return 0xFF;
	
	thstate[nth] = THST_STOP;
	return 0;
}

/*-----------------------------------------------*/

uint8_t threadrun ( uint8_t nth ) _MYMODFUN_ {
	
	if ( ( nth >= _THREADNUM ) ||
	/* thread with bit 7 are not change */
	   ( thstate[nth] & 0x80 ) ) return 0xFF;
	
	thstate[nth] = THST_RUN;
	return 0;
}

#endif

#if FUNASMPIC==1

uint8_t threadrun_GL ( uint8_t nth ) _MYMODFUN_ __naked {

	nth;
	
	__asm
	global _threadrun
	global _threadstop
	
	_instgraderuttest00:
		;zero non stop
		xorlw 0x00
		bz thrun_008

	_instgraderuttest01:
		movwf _FSR0L
		clrf _FSR0H, A
		movlw _THREADNUM
		SUBWF	_FSR0L, W, A
		bnc thrun_010
	thrun_008:
		SETF	_WREG
		bcf _STATUS, 2, A ;set z or not set
		return
	
	thrun_010:
		MOVLW LOW(_thstate)
		ADDWF _FSR0L, F, A
		MOVLW HIGH(_thstate)
		ADDWFC _FSR0H, F, A
		btfsc _INDF0, 7, A
		bra thrun_008
		bsf _STATUS, 2, A ;set z or not set
		return

	_threadstop:
		rcall _instgraderuttest00
		bnz _threadrun01
		MOVLW THST_STOP
		bra _threadrun04
		
	_threadrun:
		rcall _instgraderuttest01
		
	_threadrun01:
		bz _threadrun00
		return
		
	_threadrun00:
		MOVLW THST_RUN
	_threadrun04:
		MOVWF _INDF0
		CLRF _WREG
		return
	__endasm;
}
#endif

/*-----------------------------------------------*/

void irq_on ( void ) {
#if defined(__PIC18F25Q10_H__) || defined(_PIC18F25Q10_H_)
	//on interrupt and prioryty
	INTCON = 0b11100111;
	/*__asm
		MOVLW	0xe7
		MOVWF	_INTCON, A
	__endasm;*/
#else
#if THREAD_USE_LOW_PRIORITY==1
	RCONbits.IPEN = 1;
#endif
#endif
	__asm
		bsf _INTCON,6,A
		bsf _INTCON,7,A
	__endasm;
}

/*-----------------------------------------------*/

void thstop_endfun ( void ) __naked {

	/* return value */
	__asm
;		MOVWF _POSTDEC1
;		MOVLW	LOW(_threturn)
;		MOVWF	_FSR2L
;		MOVLW	HIGH(_threturn)
;		MOVWF	_FSR2H
;		BANKSEL	_thactiv
;		MOVF	_thactiv, W, B
;		ADDWF _FSR2L,F
;		MOVLW 0
;		ADDWFC _FSR2H,F
;		MOVFF _PREINC1,_INDF2
	__endasm;
	
	/* set state stop */
	__asm
		BANKSEL	_thactiv ;number thread activ
		clrf _FSR0H, A
		movf _thactiv, W, B ;size x1
		addlw low(_thstate)
		movwf _FSR0L, A
		movlw high(_thstate)
		addwfc _FSR0H, F, A
		
		movlw THST_END
		movwf _INDF0, A
	
	__endasm;


	while(1) {
#if defined(__PIC18F25Q10_H__) || defined(_PIC18F25Q10_H_)
		PIR0 |= 1<<5;
#else
		INTCONbits.TMR0IF=1;
#endif
	}


//	END_TH_FUN();
}


/*-----------------------------------------------*/

void irq_off ( void ) __naked {
	__asm
;version with stack
;		movff _INTCON, _POSTDEC1
;		bcf _INTCON,7,A
;		bcf _INTCON,6,A
	
;		BANKSEL _thflag
;		movf _PREINC1, F
;		bsf _thflag,6,B
;		bsf _thflag,7,B
		
;		btfss _INDF1,7,A
;		bcf _thflag,7,B
;		btfss _INDF1,6,A
;		bcf _thflag,6,B

;		RETURN
	__endasm;
	
	__asm
;version with PRODL
;if irq off dont
		movff _INTCON, _PRODL
		
		;btfss _INTCON, 7, A
		;return
		bcf _INTCON,7,A
		
		BANKSEL _thflag
		;btfsc _INTCON, 5, A
		;return
		
		bcf _INTCON,6,A
		
		;bsf _thflag,5,B ;remember state
		bsf _thflag,6,B
		bsf _thflag,7,B
		
		btfss _PRODL,7,A
		bcf _thflag,7,B
		btfss _PRODL,6,A
		bcf _thflag,6,B

		RETURN
	__endasm;
}

/*-----------------------------------------------*/

void irq_restore ( void ) __naked {
	__asm
		BANKSEL _thflag
		;btfss _thflag,5,B ;remember state
		;return
		;bcf _thflag,5,B ;remember state
		
		btfsc _thflag,6,B
		bsf _INTCON,6,A
		btfsc _thflag,7,B
		bsf _INTCON,7,A
		RETURN
	__endasm;
}

/*-----------------------------------------------*/
/* ;save reg stack soft stack hard in sram */

#if SAVE_IN_RAM_EXT==1

uint8_t stckha[4];

void cpp0tosram ( void ) _MYMODFUN_ __naked {

	__asm
	extern _RSoutP
	extern _ramwrit
	extern _writeset
	
	_JEJE01:
		BANKSEL	_thactiv
		RLNCF	_thactiv, W, B
		CALL	_blkset
		CALL	_writeset
	
	__endasm;
	
	//blkset ( thactiv );
	//blkset ( thactiv<<1 ); //for block 512
	//writeset();
	
	__asm
	
		clrf _FSR2L, A
		clrf _FSR2H, A
		
	/* ;save register */
		BANKSEL _registcount
		movf _registcount,F,B
		bz cp_reg_loop_sram_10	
		
	cp_reg_loop_sram:
		BTFSC	_PIE1bits, 4
		BRA	cp_reg_loop_sram
		
		movf _POSTINC2, W
		rcall _ramwrit
		BANKSEL _registcount
		decfsz _registcount,F,B
		bra cp_reg_loop_sram
		
	cp_reg_loop_sram_10:
		clrwdt

#if SHOW_DBG_MEMEXT==1
		movlw 'Y'
		rcall _RSoutP
	ererer:
		BTFSC	_PIE1bits, 4
		BRA	ererer
#endif

	/* ;save stack harwdare from this point to 0 */
		/* ;save number stck */
	
		BANKSEL _stckha
		movf _STKPTR, W, A
		movwf _PRODL, A ;beckup
		rcall _ramwrit
		/* ;now need irq of */
	__endasm;

#include "z_OFF_IRQ.inc"

	__asm

	/*
	cp_hs_sram:
	
#if SHOW_DBG_MEMEXT==1	
		bsf _INTCON,7,A
		nop
	WAITFORRS0s:
		BTFSC	_PIE1bits, 4
		BRA	WAITFORRS0s
		bcf _INTCON,7,A
		nop
#endif

		BANKSEL _stckha
		movf _TOSL, W, A
		movwf _stckha+0, B
		movf _TOSH, W, A
		movwf _stckha+1, B
		movf _TOSU, W, A
		movwf _stckha+2, B
		
		movf _stckha+0, W, B
		rcall _ramwrit
		BANKSEL _stckha
		movf _stckha+1, W, B
		rcall _ramwrit
#if DONT_SAVE_U_HS==0
		BANKSEL _stckha
		movf _stckha+2, W, B
		rcall _ramwrit
#endif
		decfsz _STKPTR, F, A
		bra cp_hs_sram
*/

	cp_hs_sram:
	
#if SHOW_DBG_MEMEXT==1
		bsf _INTCON,7,A
		nop
	WAITFORRS0s:
		BTFSC	_PIE1bits, 4
		BRA	WAITFORRS0s
		bcf _INTCON,7,A
		nop
#endif

		movf _TOSL, W, A
		rcall _ramwrit
		movf _TOSH, W, A
		rcall _ramwrit
#if DONT_SAVE_U_HS==0		
		movf _TOSU, W, A
		rcall _ramwrit
#endif
		decfsz _STKPTR, F, A
		bra cp_hs_sram
		
		;movff _PRODL,_STKPTR ;in this point dont need this
		
	__endasm;
	
#include "z_ON_IRQ.inc"


	__asm

#if SHOW_DBG_MEMEXT==1
		movlw '_'
		rcall _RSoutP
#endif
	
	/* ;save stack software */
	/* ;form this point to top */
		/*; sub pointer */
		
		bsf _STATUS, _C, A
		movlw low(THSTACADR00)
		movf _FSR1L, W, A
		sublw low(THSTACADR00)
		movwf _PRODL, A
		/* ;becuse stack propably never < 254 dont need this */
		;movlw high(THSTACADR00)
		;subfwb _FSR1H, W, A
		;movf _FSR0H
		
		/* ;we just have in WREG */
		rcall _ramwrit
	
	cp_ss_sram:
		movf _PREINC1, W, A
		rcall _ramwrit
		decfsz _PRODL, F, A
		bra cp_ss_sram

	__endasm;
	__asm
		goto _RJEJE01
		return
	__endasm;
}
#endif

/*-----------------------------------------------*/

#if SAVE_IN_RAM_EXT==1

uint8_t startthvoidn ( uint8_t (*thfun)(void), uint8_t nth ) {
	
	uint8_t i;
	
	if ( ( nth == thactiv ) ||
		( nth >= _THREADNUM ) ||
		( nth == 0 ) ) return -1;
	
	irq_off();
	//INTCON &= ~0x40;
	//blkset ( nth );
	blkset ( nth<<1 );//for block 512
	writeset();
	
	/* ;save register empty */
	for(i=SIZREGSAVE; i&0xFF; --i ) {
		ramwrit(0x00);
	}
	
	/* ;save stack return */
	ramwrit(0x02);
	ramwrit((uint32_t)thfun);
	ramwrit(((uint32_t)thfun)>>8);
#if DONT_SAVE_U_HS==0
	ramwrit(((uint32_t)thfun)>>16);
#endif	
	ramwrit((uint32_t)thstop_endfun);
	ramwrit(((uint16_t)thstop_endfun)>>8);
#if DONT_SAVE_U_HS==0
	ramwrit(((uint32_t)thstop_endfun)>>16);
#endif

	/* ;save stack soft */
	ramwrit(15);
	ramwrit(FSR2H);
	
	irq_restore();
	//INTCON |= 0x40;
	
	thstate[nth] = THST_RUN;
	
	return nth;
}
#endif

/*-----------------------------------------------*/
/* ;read reg stack soft stack hard from sram */

#if SAVE_IN_RAM_EXT==1
	
void cppsramto0 ( void ) _MYMODFUN_ __naked {

	__asm
	extern _RSoutP
	extern _readset
	extern _blkset
	
		_JEJE02:
		BANKSEL	_thactiv
		RLNCF	_thactiv, W, B
		CALL	_blkset
		CALL	_readset
	__endasm;
	
	__asm
		extern _ramread
		extern _sendhex
	
		clrf _FSR2L, A
		clrf _FSR2H, A
		
	/* ;save register */
		BANKSEL _registcount
		movf _registcount,F,B
		bz cp_sram_reg_loop_10
	cp_sram_reg_loop:
		BTFSC	_PIE1bits, 4
		BRA	cp_sram_reg_loop
		
		rcall _ramread
		movwf _POSTINC2, A
		BANKSEL _registcount
		decfsz _registcount,F,B
		bra cp_sram_reg_loop
		
	cp_sram_reg_loop_10:
		clrwdt

#if SHOW_DBG_MEMEXT==1
		movlw 'Y'
		rcall _RSoutP
	TTTTTTTT:
		BTFSC	_PIE1bits, 4
		BRA	TTTTTTTT
#endif

	/* ;read stack harwdare from this point to 0 */
	/* ;read number stck */
		rcall _ramread
		BANKSEL _stckha
		movwf _PRODL, A ;beckup
		movwf _stckha+3, B
		
		/* ;now need irq of */
		__endasm;

#include "z_OFF_IRQ.inc"
	__asm
		BANKSEL _stckha
	cp_sram_hs:
		movff _PRODL,_STKPTR
		rcall _ramread
		BANKSEL _stckha
		movwf _stckha, B
		rcall _ramread
		BANKSEL _stckha
		movwf _stckha+1, B
#if DONT_SAVE_U_HS==0
		rcall _ramread
		BANKSEL _stckha
		movwf _stckha+2, B
#endif

#if SHOW_DBG_MEMEXT==1
		bsf _INTCON,7,A
		nop		
	WAITFORRSs0:
		BTFSC	_PIE1bits, 4
		BRA	WAITFORRSs0
		bcf _INTCON,7,A
		nop
#endif

		movff _stckha+3,_STKPTR
		movff _stckha+0,_TOSL
		movff _stckha+1,_TOSH
#if DONT_SAVE_U_HS==0
		movff _stckha+2,_TOSU
#endif
		decfsz _stckha+3, F, B
		bra cp_sram_hs
		movff _PRODL,_STKPTR

	__endasm;

#include "z_ON_IRQ.inc"
		
	__asm
	
#if SHOW_DBG_MEMEXT==1
		movlw '_'
		rcall _RSoutP
#endif

	/* ;read stack software */
	/* ;form this point to top */

		/*; sub pointer */
		movlw low(THSTACADR00)
		movwf _FSR2L, A
		;movlw high(THSTACADR00) ;dont need if dont cross 255
		;movwf _FSRLH, A
		;but copy
		;movff _FSR1H, _FSR2H
		
		rcall _ramread /* ; how much stack */
		movwf _PRODL, A /* ;remember for loop */
		subwf _FSR2L, F, A

		/* ;need new point */
		movf _FSR2L, W
		movwf _FSR1L, A
		movff _FSR1H, _FSR2H

	cp_sram_ss:
		rcall _ramread
		movwf _PREINC2, A
		decfsz _PRODL, F, A
		bra cp_sram_ss

	__endasm;
	__asm
		goto _RJEJE02
		return
	__endasm;
	
	__asm
		return
	__endasm;
}
#endif

/*-----------------------------------------------*/

#if SAVE_IN_RAM_EXT==0

/*-----------------------------------------------*/
/*  FSR0 -> FSR2  siz -> _registcount*/

void cpp0to2 ( void ) _MYMODFUN_ __naked {

	__asm

	#if SPEEDUPSAVE>1
		rrncf _registcount, F, B
	#endif
	#if SPEEDUPSAVE>2
		rrncf _registcount, F, B
	#endif
	#if SPEEDUPSAVE>4
		rrncf _registcount, F, B
	#endif
	
;loop 5*16=80 7*8+1=57 11*4+2=46 19*2+3=41
		movf _registcount,F,B
		bz cp_reg_loop_10
		cp_reg_loop:
		MOVFF	_POSTINC0, _POSTINC2
	#if SPEEDUPSAVE>1
		MOVFF	_POSTINC0, _POSTINC2
	#endif
	#if SPEEDUPSAVE>2
		MOVFF	_POSTINC0, _POSTINC2
		MOVFF	_POSTINC0, _POSTINC2
	#endif
	#if SPEEDUPSAVE>4
		MOVFF	_POSTINC0, _POSTINC2
		MOVFF	_POSTINC0, _POSTINC2
		MOVFF	_POSTINC0, _POSTINC2
		MOVFF	_POSTINC0, _POSTINC2
	#endif
		decfsz _registcount,F,B
		bra cp_reg_loop
		
		cp_reg_loop_10:			
		clrwdt
		
		return
			
	__endasm;
}

/*-----------------------------------------------*/
/* data in TABLAT */
/* adres if the same size of register */
/*
uint8_t setset ( void ) __naked {
	__asm
	movlw SIZREGSAVE
	mulwf _TABLAT, A

	movlw LOW(_thdump00)
	addwf _PRODL, W
	movwf _FSR0L, A
	movlw HIGH(_thdump00)
	addwfc _PRODH, W
	movwf _FSR0H, A
	
	return
	__endasm;
}
*/

/* read and set adres where dump register in memory */
uint8_t setset ( void ) __naked {
	
	__asm
	MOVLW	LOW(_tblwskthr)
	movwf _TBLPTRL, A
	MOVLW	HIGH(_tblwskthr)
	movwf _TBLPTRH, A
	MOVLW	UPPER(_tblwskthr)
	movwf _TBLPTRU, A
	
	;mul x3
	global _setset_03
	_setset_03:
	rlncf _TABLAT, W, A
	addwf _TABLAT, W, A
	
	addwf _TBLPTRL, F, A
	clrw
	addwfc _TBLPTRH, F, A
	addwfc _TBLPTRU, F, A

	TBLRD*+
	MOVFF _TABLAT, _FSR0L
	TBLRD*+
	MOVFF _TABLAT, _FSR0H
	TBLRD*+
	MOVF _TABLAT, W
	
	return
	
	;table is for six thread for aligned 
	_tblwskthr:
	DB LOW(_thdump00), HIGH(_thdump00)
	DB LOW(NUMPARAM00), LOW(_thdump00+DNMPLACES01)
	DB HIGH(_thdump00+DNMPLACES01), LOW(NUMPARAM01)
#if _THREADNUM>2
	DB LOW(_thdump00+DNMPLACES02), HIGH(_thdump00+DNMPLACES02)
	DB LOW(NUMPARAM02), LOW(_thdump00+DNMPLACES03)
	DB HIGH(_thdump00+DNMPLACES03), LOW(NUMPARAM03)
#endif
#if _THREADNUM>4
	DB LOW(_thdump00+DNMPLACES04), HIGH(_thdump00+DNMPLACES04)
	DB LOW(NUMPARAM04), LOW(_thdump00+DNMPLACES05)
	DB HIGH(_thdump00+DNMPLACES05), LOW(NUMPARAM05)
#endif	
	__endasm;
}

/*-----------------------------------------------*/

uint8_t startthvoidn ( uint8_t (*thfun)(void), uint8_t num ) __naked {

	thfun;
	num;
	
	__asm
	;global _startthvoidn
	;global _startthvoid1end
	
	MOVFF	_FSR2L, _POSTDEC1
	MOVFF	_FSR1L, _FSR2L
	MOVFF	_FSR2H, _POSTDEC1

	goto _startthvoidnum
	;we have return value in WREG

	_startthvoid1end:
	MOVFF	_PREINC1, _FSR2H
	MOVFF	_PREINC1, _FSR2L
	RETURN
	
	__endasm;
}

/*-----------------------------------------------*/

uint8_t startthvoidnum ( void ) __naked {

	__asm
	;extern _startthvoid1end
	;movlw 5
	;movff _PLUSW2,_PRODH ;number thread
	
	;other way but count FSR
	movlw 6
	movff _PLUSW1,_TABLAT ;number thread

	;dont run 0 always main
	movf _TABLAT, F, A ;zero?
	bz _TH_DONT_START
	
	;if number is the activ thread return
	movff _thactiv,_WREG
	xorwf _TABLAT, W, A
	bz _TH_DONT_START
	
	;if number is max thread
	movlw _THREADNUM
	subwf _TABLAT, W, A
	bnc _TH_START
	
_TH_DONT_START:
	SETF	_WREG
	bra startthvoidnumend
	
	;table is for six thread for aligned 
	_tblwskstck:
	DB LOW(THSTACADR00), HIGH(THSTACADR00)
	DB THSTACKPOINT00, LOW(THSTACADR01)
	DB HIGH(THSTACADR01), THSTACKPOINT01
#if _THREADNUM>2
	DB LOW(THSTACADR02), HIGH(THSTACADR02)
	DB THSTACKPOINT02, LOW(THSTACADR03)
	DB HIGH(THSTACADR03), THSTACKPOINT03
#endif
#if _THREADNUM>4
	DB LOW(THSTACADR04), HIGH(THSTACADR04)
	DB THSTACKPOINT04, LOW(THSTACADR05)
	DB HIGH(THSTACADR05), THSTACKPOINT05
#endif
	
_TH_START:
	MOVLW	LOW(_tblwskstck)
	movwf _TBLPTRL, A
	MOVLW	HIGH(_tblwskstck)
	movwf _TBLPTRH, A
	MOVLW	UPPER(_tblwskstck)
	movwf _TBLPTRU, A
	
	call _setset_03

	stackisset:
	call _irq_off
	MOVFF	_STKPTR, _POSTDEC1
	MOVWF	_STKPTR

;pop return adres
	MOVLW	LOW(_thstop_endfun)
	MOVWF	_TOSL
	MOVLW	HIGH(_thstop_endfun)
	MOVWF	_TOSH
	MOVLW	UPPER(_thstop_endfun)
	MOVWF	_TOSU
	INCF	_STKPTR, F
	
;pinter tu fun
	movf _PREINC2,W
	movf _PREINC2,W
	movwf _TOSL
	movf _PREINC2,W
	movwf _TOSH
	movf _PREINC2,W
	movwf _TOSU
	movff _PREINC2,_TABLAT ;number thread setset2
	
;save thnumber
	MOVFF _STKPTR, _PRODL ;remember stack
	MOVFF	_PREINC1, _STKPTR
	call _irq_restore ;return from set stack
	
	movff _FSR0L, _FSR2L
	movff _FSR0H, _FSR2H
	
;set FSR0 dump
	call _setset

;FSR2 - stack

;pop point to stack and frame point high
	MOVLW REGISSTACK ;number other register on stack
	SUBWF _FSR2L,F
	clrw
	SUBWFB _FSR2H,F ;dont need if stack and baund size 256
	MOVFF _FSR2L, _POSTINC0
	MOVFF _FSR2H, _POSTINC0
	;MOVLW	7 ;new wersian wiych tabl
	;MOVLW	(REGISSTACK-4) ;place of FSR2H
	;MOVFF _FSR2H, _PLUSW2
	movf _FSR2H, W
	movwf _PREINC2,A
	
;pop hardware stack
	MOVFF _PRODL, _POSTINC0
	
;state thnumber
	movlw 6
	movff _PLUSW1,  _FSR2L ;number thread
	;movff _PRODL, _FSR2L
	clrf _FSR2H
	MOVLW	LOW(_thstate)
	ADDWF _FSR2L, F
	MOVLW	HIGH(_thstate)
	ADDWFC _FSR2H, F
	movlw THST_RUN
	movwf _INDF2

	movlw 6
	movf _PLUSW1, W ;number thread

startthvoidnumend:
	goto _startthvoid1end
	__endasm;
	
}

/*-----------------------------------------------*/

#endif

/*-----------------------------------------------*/

