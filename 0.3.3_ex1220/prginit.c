/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/
/*-----------------------------------------------*/

#include <stdint.h>

/*-----------------------------------------------*/

#include "Bheadb.h"

/*-----------------------------------------------*/

void initport (void) {

#if defined(__PIC18F25Q10_H__) || defined(_PIC18F25Q10_H_)

	TRISB = 0x04; //rb2 input
	ANSELB = 0; //digital
	WPUB = 0x04;
		
	TRISC = 0x00; //
	ANSELC = 0; //digital
	
	TRISA = 0x01; //ra0 input
	ANSELA = 0x01; //ra0 analog
	
	LATC = 0;
	LATB = 0x00; //0x04 rb2 input
	
	//ppsunlock();
	RB0PPS = 0x0F; //out T0 //dla testu
	RB1PPS = 0x09; //out Tx
	//ppslock();
	
	RX1PPS = (1<<3) | (2);
	
//fosc//4
	TMR1CLK = 0x01;
	T1CON = 0x11; //2 presc	on

#endif

#if defined(_PIC18F1220_H_) || defined(__PIC18F1220_H__) || \
  defined(_PIC18F26K22_H_) || defined(__PIC18F26K22_H__) || \
  defined(_PIC18F4431_H_) || defined(__PIC18F4431_H__)
	LATA = 0x00;
	LATB = 0x00;
	//ADCON1 = 0x7F;
	TRISA = _RA4 | _RA1 | _RA0 | _RA7 | _RA2; //input
//; konfig UART
	//TRISB = 1<<1 | 1<<4 | _PORTB_INT0 | _RB2 | _RB3;
	TRISB = 1<<1 | 1<<4 | 1<<0 | _RB2 | _RB3;
//to ammy gdzie indziej an0 an1 analog
	ADCON1 = 1<<5 | 1<<6 | 1<<2 | 1<<4 | 1<<3;	
//fosc 8mhz baud = fosc /4/(n+1) = 500khz
//	SPBRG = 0x03;
//fosc 8mhz baud = fosc /4/(n+1) = 19k2hz
//	SPBRG = 103;
//fosc 8mhz baud = fosc /4/(n+1) = 9k6hz
//	SPBRG = 207;
// SPBRGH = 0x00;
//fosc 24mhz baud = fosc /4/(n+1) = 9k6hz
	SPBRG = 624 & 0xFF;
	SPBRGH = 624 >> 8;
//enalbe 16 bit
	
//enalbe 16 bit
	BAUDCTL = _BRG16;
//enable hi speed
//	TXSTA = _TXEN | _BRGH;
	TXSTA = _BRGH;
//nvale port i receive
	RCSTA = _SPEN | _CREN;
//enable intrerupt RCIE
//	bsf PIE1,RCIE,A	 ;f0 a0


//OSSCON - prescaler
	OSCCONbits.IRCF = 7;


//timer 1, prescaler 8
//	T1CON = _TMR1ON | _T1CKPS1 | _T1CKPS0;
//timer 1, prescaler 4
//	T1CON = _TMR1ON | _T1CKPS1;
//timer 1, prescaler 2
	T1CON = _TMR1ON | _T1CKPS0;
//int 1na 0

//#if ADCMEAS
	//ADCON2 = 0x02; //left 0acqu fosc/32
	ADCON2 = 0x12 | 0x80; //right 4acqu fosc/32
	//ADCON0 = 0x09; //AN2 ADON
	ADCON0 = 0x01; //AN0 ADON
//#endif
	PIE1bits.TMR1IE = 1;
	//IPR1 = 0x77;
	//IPR1 = 0xFF;
	//IPR1bits.TXIP = 1; //default sftert reset
#endif

	return;
}


