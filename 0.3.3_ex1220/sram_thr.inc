/*
register default
SIZREGSAVE

stack hardw start

(in ram how much 1)
2 - point fun
1 - point return
0 - restart

stack soft start FSR1 and FSR2

(in ram how much 1)
stack = top -> 15
stack high byte (FSR2H)

	__asm
		clrwdt

;lock irq for this 3

		MOVFF	_STATUS, _POSTDEC1
		MOVFF	_BSR, _POSTDEC1
		MOVWF	_POSTDEC1
		MOVFF	_FSR0L, _POSTDEC1
		MOVFF	_FSR0H, _POSTDEC1
		MOVFF _TABLAT, _POSTDEC1
		MOVFF _TBLPTRL, _POSTDEC1
		MOVFF _TBLPTRH, _POSTDEC1
		MOVFF _TBLPTRU, _POSTDEC1
		MOVFF _PRODL, _POSTDEC1
		MOVFF _PRODH, _POSTDEC1
		MOVFF _PCLATH, _POSTDEC1
		MOVFF _PCLATU, _POSTDEC1
		MOVFF	_FSR2L, _POSTDEC1
		MOVFF	_FSR2H, _POSTDEC1
*/
