

#if DUMPSTACKSFIRQ == 1

if(1) {
	volatile unsigned char * wsk;
	uint8_t i;
	wsk = (void*) (THSTACADR00|0x800000);
	INTCON &= ~0x40;
	RSoutP ('\n');
	for ( i=0; i<( THSTACADR00 - THSTACADRFF ); --wsk ) {
		sendhex (*wsk);
		RSoutP (' ');
		++i;
		if(0==(i&0x1F))RSoutP (10);
	}
	RSoutP (10);
	INTCON |= 0x40;
}

#endif
