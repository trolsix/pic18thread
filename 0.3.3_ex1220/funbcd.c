/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/

#include "threadpic16.h"
#include <stdint.h>

#include "Bheadb.h"
#include "semaphorespic16.h"

void RSoutP (unsigned char a) _MYMODFUN_;

/*-----------------------------------------------*/

char * tbll;
uint32_t ul;

extern sem_t semmain;
extern sem_t sembcd;

/* naked (15+15)*2 = 60 cycles less */
/* size 528-406 = 122 bytes less */

static const uint32_t dectbl[9] = {  1,
                         10,       100,
                       1000,     10000,
                     100000,   1000000,
                   10000000, 100000000 };

//void binbcd1 ( char * tbl, uint32_t ul ) __reentrant {
uint8_t binbcd1 ( void ) __naked {
	
	uint32_t in2;
	unsigned char wsz, a;
	
	while(1){
		sem_wait(&sembcd);

		tbll[10] = 0;
		wsz = 0;

		while ( ul > 999999999 ) {
			++wsz;
			ul -= 1000000000;
		}
		tbll[9] = wsz+'0';
		
		for( wsz=8; wsz&0xFF; --wsz ) {
			in2 = dectbl[wsz];
			a = 0;
			while (1) {
				ul -= in2;
				if ( ul & 0x80000000 )
					break;
				++a;
			}
			ul += in2;
			tbll[wsz] = a+'0';
		}
		
		tbll[0] = ul+'0';
		
		a = sem_post(&semmain);
	}
	
}

/*-----------------------------------------------*/
