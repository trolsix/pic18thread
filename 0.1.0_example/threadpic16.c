/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/
/*-----------------------------------------------

 pic18**** thread

-----------------------------------------------*/

#ifdef __SDCC_pic16	
#include <pic16/pic18fregs.h>
#endif

#include <stdint.h>

/*-----------------------------------------------*/

#include "threadpic16.h"
#include "threadpic16conf.h"

/*-----------------------------------------------*/

//extern volatile uint16_t swsk, swskl;
extern volatile uint16_t swsk;

/*-----------------------------------------------*/

#if THREAD_USE_LOW_PRIORITY==0
void  tch_int (void) __interrupt 1 __naked {
#else
void  tch_int (void) __interrupt 2 __naked {
#endif
	__asm
	goto _tch_int_jump
	__endasm;
}

/*-----------------------------------------------*/

#if _THREADNUM==2
uint16_t thstckadr[_THREADNUM] =  { THSTACADR00, THSTACADR01 };
#endif
#if _THREADNUM==3
uint16_t thstckadr[_THREADNUM] =  { THSTACADR00, THSTACADR01, THSTACADR02 };
#endif

uint8_t thdump[_THREADNUM][_THNUMPARAM];
uint8_t thactiv;
uint8_t thnumber;
uint8_t threturn[_THREADNUM];
uint8_t thstate[_THREADNUM];

/*
0 irq semaphores
1 SAVE or RESTORE in irq
6 copy intcon state
7 copy intcon state
*/
uint8_t thflag;

/*-----------------------------------------------*/

void irq_on ( void ) {
#if THREAD_USE_LOW_PRIORITY==1
	RCONbits.IPEN = 1;
#endif
	irq_onasm();
}

/*-----------------------------------------------*/

void initth ( void ) {

	thactiv = 0;
	thnumber = 1;
	thflag = 0;	
//timer 0, prescaler 8
//	T0CON = _TMR0ON | _T0PS1 | _T08BIT;
//timer 0, prescaler 16
//	T0CON = _TMR0ON | _T0PS1 | _T0PS0 | _T08BIT;
//timer 0, prescaler 32 irqcykl 8192 244 dla 2MHz
//	T0CON = _TMR0ON | _T0PS2 | _T08BIT;	
//timer 0, prescaler 64 irqcykl 16384 122 dla 2MHz
	T0CON = _TMR0ON | _T0PS2 | _T0PS0 | _T08BIT;
//timer 0, prescaler 128 irqcykl 32k 60 dla 2MHz
//	T0CON = _TMR0ON | _T0PS2 | _T0PS1 | _T08BIT;
//timer 0, prescaler 256 irqcykl 64k 30 dla 2MHz
//	T0CON = _TMR0ON | _T0PS2 | _T0PS1 | _T0PS0 | _T08BIT;	
	
	INTCONbits.TMR0IE = 1;
#if THREAD_USE_LOW_PRIORITY==1
	INTCON2bits.TMR0IP = 0; //low prioryty
#endif
}

/*-----------------------------------------------*/

void thstop ( void ) __naked {
	__asm
	MOVWF _POSTDEC1
	MOVLW	LOW(_threturn)
	MOVWF	_FSR2L
	MOVLW	HIGH(_threturn)
	MOVWF	_FSR2H
	BANKSEL	_thactiv	
	MOVF	_thactiv, W, B
	ADDWF _FSR2L,F
	MOVLW 0
	ADDWFC _FSR2H,F
	MOVFF _PREINC1,_INDF2
	__endasm;

	END_TH_FUN();
}

/*-----------------------------------------------*/

void save_register(void);
void restore_register(void);
static uint8_t registcount;

void  tch_int_jump (void) __naked {

	__asm
	MOVFF	_STATUS, _POSTDEC1
	MOVFF	_BSR, _POSTDEC1
	MOVWF	_POSTDEC1
	MOVFF	_FSR2L, _POSTDEC1
	MOVFF	_FSR2H, _POSTDEC1
	MOVFF	_FSR0L, _POSTDEC1
	MOVFF	_FSR0H, _POSTDEC1
	MOVFF	_PRODL, _POSTDEC1
	MOVFF	_PRODH, _POSTDEC1
	__endasm;
	
//	save_register();
	//uint8_t thdump[_THREADNUM][_THNUMPARAM];_THNUMPARAM
	/* save kontest */
	__asm
	BANKSEL _thactiv
	MOVLW _THNUMPARAM
	MULWF _thactiv,B
	MOVFF _PRODL,_FSR2L
	MOVFF _PRODH,_FSR2H
	MOVLW LOW(_thdump)
	ADDWF _FSR2L,F
	MOVLW HIGH(_thdump)
	ADDWFC _FSR2H,F
	
	MOVFF _STKPTR, _POSTINC2
	MOVFF _FSR1L, _POSTINC2
	MOVFF _FSR1H, _POSTINC2
	MOVFF _PCLATH, _POSTINC2
	MOVFF _PCLATU, _POSTINC2
	
#if SAVE_TBLCONT==1
	MOVFF _TABLAT, _POSTINC2
	MOVFF _TBLPTRL, _POSTINC2
	MOVFF _TBLPTRH, _POSTINC2
	MOVFF _TBLPTRU, _POSTINC2
#endif

	;movlw low(r0x00)
	movlw 0
	movwf _FSR0L
	;movlw high(r0x00)
	movwf _FSR0H
	;movlw 8 ;8 reg x 2 = 16
	movlw (NUMBREGSAVE>>1)
	BANKSEL _registcount
	movwf _registcount
	save_reg_loop:
	MOVFF	_POSTINC0, _POSTINC2
	MOVFF	_POSTINC0, _POSTINC2
	decfsz _registcount,F
	goto save_reg_loop

	movf _thactiv,F
	bnz only_0
	BANKSEL _swsk
	MOVFF _FSR1L, _swsk
	MOVFF _FSR1H, (_swsk+1)	
	only_0:
	__endasm;


	if(PIR1bits.TMR1IF) {
		PIR1bits.TMR1IF=0;
	}
	
	/* contest timer */
	if(INTCONbits.TMR0IF) {
		INTCONbits.TMR0IF = 0;
		/* we have thread ? */
		if ( ++thactiv >= thnumber ) thactiv = 0;
	}
	
	
	/* ****************************************************** */	
	
#if FUN_IRQ_THREAD_ALLOWED==1	

	/* in this place you can past call void funkcjon, example:

	switch_test();

	*/
		
	
	
#endif

	/* ****************************************************** */	
	
	/* restore kontest */
	__asm
	BANKSEL _thactiv
	MOVLW _THNUMPARAM
	MULWF _thactiv,B
	MOVFF _PRODL,_FSR2L
	MOVFF _PRODH,_FSR2H
	MOVLW LOW(_thdump)
	ADDWF _FSR2L,F
	MOVLW HIGH(_thdump)
	ADDWFC _FSR2H,F
	__endasm;
	
#if THREAD_USE_LOW_PRIORITY==1
	INTCONbits.GIE = 0;
#endif
	__asm
	MOVFF _POSTINC2, _STKPTR
	MOVFF _POSTINC2, _FSR1L
	MOVFF _POSTINC2, _FSR1H
	__endasm;
#if THREAD_USE_LOW_PRIORITY==1
	INTCONbits.GIE = 1;
#endif

	__asm	
	MOVFF _POSTINC2, _PCLATH
	MOVFF _POSTINC2, _PCLATU	
	
#if SAVE_TBLCONT==1
	MOVFF _POSTINC2, _TABLAT
	MOVFF _POSTINC2, _TBLPTRL
	MOVFF _POSTINC2, _TBLPTRH
	MOVFF _POSTINC2, _TBLPTRU
#endif	
	
	;movlw low(r0x00)
	movlw 0
	movwf _FSR0L
	;movlw high(r0x00)
	movwf _FSR0H
	movlw 8 ;8 reg x 2 = 16
	BANKSEL _registcount
	movwf _registcount
	restore_reg_loop:
	MOVFF	_POSTINC2,_POSTINC0
	MOVFF	_POSTINC2,_POSTINC0
	decfsz _registcount,F
	goto restore_reg_loop
	__endasm;
	

	__asm
	MOVFF _PREINC1, _PRODH
	MOVFF _PREINC1, _PRODL
	MOVFF _PREINC1, _FSR0H
	MOVFF _PREINC1, _FSR0L
	MOVFF _PREINC1, _FSR2H
	MOVFF _PREINC1, _FSR2L
	MOVF  _PREINC1, W
	MOVFF _PREINC1, _BSR
	MOVFF _PREINC1, _STATUS
	RETFIE
	__endasm;
}

/*-----------------------------------------------*/


