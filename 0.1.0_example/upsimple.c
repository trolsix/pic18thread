/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/
/*-----------------------------------------------

pic18f1220 thread example

-----------------------------------------------*/

#ifdef __SDCC_pic16
#include <pic16/pic18fregs.h>
#endif

/*-----------------------------------------------*/

#include <stdio.h>
#include <stdint.h>

/*-----------------------------------------------*/

#include "threadpic16.h"

/*-----------------------------------------------*/

#if defined(SDCC) && defined (__SDCC_pic16)
#define _MYMODFUN_ __wparam
#else
#define _MYMODFUN_
#endif

/*-----------------------------------------------*/
/* configuracje fuse i configi hardware uC */
/*-----------------------------------------------*/

#include "Bconf.h"

/*-----------------------------------------------*/

void RSoutP (unsigned char a) _MYMODFUN_;
void CLRWDT(void);
void binbcd1 ( char tbl[], uint32_t a07 );
void initport (void);

/*-----------------------------------------------*/

static uint8_t RStim;
static void RRSout (unsigned char a) _MYMODFUN_;
static void RSouttbl ( unsigned char il, unsigned char *tbl );
	
/*-----------------------------------------------*/

volatile char tbl[12];
volatile char tbk[12];
volatile uint16_t swsk;

/*-----------------------------------------------*/

volatile uint8_t wsrsbufin;
volatile uint8_t wsrsbufou;
volatile char rsbuf[16];
//static uint8_t tickf1, tickf2;
static uint16_t tickf1, tickf2;

/*-----------------------------------------------*/
/* example */

uint8_t myfun1 ( void ) {
	while(1) {
		if ( !( ++tickf1 & 0xFFF ) ) {
			INTCON &= ~0x40;
			RSoutP ('T');
			RSoutP ('1');
			RSoutP (' ');
			binbcd1 ( tbl, swsk );
			//binbcd1 ( tbl, tickf1 );
			RSouttbl ( 5, tbl );
			RSoutP ('\n');
			INTCON |= 0x40;
		}
	}
	return 1;
}

uint8_t myfun2 ( void ) {
	while(1){
		binbcd1 ( tbk, (TMR1H<<8) + TMR1L );
		if ( !( ++tickf2 & 0x3FF ) ) {
			INTCON &= ~0x40;
			RSoutP ('T');
			RSoutP ('2');
			RSoutP (' ');
			RSouttbl ( 5, tbk );
			RSoutP ('\n');
			INTCON |= 0x40;
		}
	}
	return 2;
}

/*-----------------------------------------------*/

void RSoutP (unsigned char a) _MYMODFUN_ {
	
	if ( (( wsrsbufin - wsrsbufou ) & 0x0F) < 0x0E ) {
	//if ( wsrsbufin != wsrsbufou ) {
	//PIE1bits.TXIE = 0;
	//if ( ((wsrsbufin+1)&0x0F) == wsrsbufou )
		rsbuf[wsrsbufin] = a;
		wsrsbufin = (++wsrsbufin) & 0x0F;
		PIE1bits.TXIE = 1;
		TXSTAbits.TXEN = 1; //rson
	}
}

/*-----------------------------------------------*/

static void RSouttbl ( unsigned char il, unsigned char *tbl )  {
	if ( !il ) {
		while(1) {
			RSoutP ( *tbl );
			++tbl;
			if ( *tbl == 0 ) break;
		}
		return;
	}
	while(il) RSoutP ( tbl[--il] );
}

/*-----------------------------------------------*/

int main ( void ) {
	
	uint32_t tick;
	uint8_t thret;
	
	swsk = 0xFFFF;
	initport();
	initth();
	
	wsrsbufin = 0;
	wsrsbufou = 0;
	
	CLRWDT();
	
	tickf1 = 1;
	tickf2 = 120;
	
	//thret = startthvoid ( &myfun1 );
	thret = startthvoid ( &myfun2 );	
	
	RSouttbl ( 0, "START\n" );

	tick = 0;
	irq_on();
	//INTCON &= ~0x40;
	
	while(1) {
		
		CLRWDT();
		++tick;
		binbcd1 ( tbl, tick );
		
		if ( (0x3FF & tick) == 0) {
			INTCON &= ~0x40;
			RSoutP ('T');
			RSoutP ('0');
			RSoutP (' ');
			RSouttbl ( 9, tbl );
			RSoutP ('\n');
			INTCON |= 0x40;
		}
	}
	
//	while(1);
}


