
/*-----------------------------------------------*/

/* number threads */
#define _THREADNUM 5

/* if 1 all stack will be safe   */
/* another stack will be divided beetwen thread */
#define SAVE_HARDW_STACK 0

/* if stack adr wille be in flash save   ram */
/* but threads stack will be always the same */
#define STACKADR_IN_FLASH 1

/* speedup save-restore contest x 1,2,4,8 */
#define SPEEDUPSAVE   2

/* how many register save */
/* this must be aligned to SPEEDUPSAVE */
//#define NUMBREGSAVE 16

/* set for each thread */
#define NUMBREGSAVE00 12
#define NUMBREGSAVE01 6
#define NUMBREGSAVE02 8
#define NUMBREGSAVE03 6
#define NUMBREGSAVE04 6
#define NUMBREGSAVE05 0

/* prescaler T0 16 32 64 128 256 */
#define _PRESCALER_T0 32

/* irq switch stack to THSTACADRIH and save ram for other stack */
#define STACK_FOR_IRQ 1

/*-----------------------------------------------*/
/*  the way thread ends  */
//#define END_TH_FUN() while(1)
//#define END_TH_FUN() {while(1) {INTCONbits.TMR0IF=1;}}
//#define END_TH_FUN() {while(1) {INTCONbits.TMR0IF=1;}}

/*-----------------------------------------------*/
/*     very dangerous     better dont change    */

/* body of places allowed past void funkcion */
#define FUN_IRQ_THREAD_ALLOWED 1

/* dont change this if you dont know what do
this change thread priority
*/
#define THREAD_USE_LOW_PRIORITY 1


#if _THREADNUM==1
#define THSTACKPOINT00 0
#define THSTACKPOINT01 31
#endif
#if _THREADNUM==2
#define THSTACKPOINT00 0
#define THSTACKPOINT01 16
#define THSTACKPOINT02 31
#endif
#if _THREADNUM==3
#define THSTACKPOINT00 0
#define THSTACKPOINT01 10
#define THSTACKPOINT02 20
#define THSTACKPOINT03 31
#endif

#if _THREADNUM==4
#define THSTACKPOINT00 0
#define THSTACKPOINT01 9
#define THSTACKPOINT02 16
#define THSTACKPOINT03 24
#endif

#if _THREADNUM==5
#define THSTACKPOINT00 0
#define THSTACKPOINT01 9
#define THSTACKPOINT02 14
#define THSTACKPOINT03 20
#define THSTACKPOINT04 25
#define THSTACKPOINT05 31
#endif
#if _THREADNUM==6
#define THSTACKPOINT00 0
#define THSTACKPOINT01 6
#define THSTACKPOINT02 11
#define THSTACKPOINT03 16
#define THSTACKPOINT04 21
#define THSTACKPOINT05 26
#endif

/*-----------------------------------------------*/
/* dont change below */
/*-----------------------------------------------*/

#define ADDREGSTACK 7

/* future set for each thread */
#define NUMPARAM00 (NUMBREGSAVE00+ADDREGSTACK)
#define NUMPARAM01 (NUMBREGSAVE01+ADDREGSTACK)
#define NUMPARAM02 (NUMBREGSAVE02+ADDREGSTACK)
#define NUMPARAM03 (NUMBREGSAVE03+ADDREGSTACK)
#define NUMPARAM04 (NUMBREGSAVE04+ADDREGSTACK)
#define NUMPARAM05 (NUMBREGSAVE05+ADDREGSTACK)

#define DNMPLACES00 0
#define DNMPLACES01 NUMPARAM00
#define DNMPLACES02 (DNMPLACES01+NUMPARAM01)
#define DNMPLACES03 (DNMPLACES02+NUMPARAM02)
#define DNMPLACES04 (DNMPLACES03+NUMPARAM03)
#define DNMPLACES05 (DNMPLACES04+NUMPARAM04)
#define DNMPLACES06 (DNMPLACES05+NUMPARAM05)

#if _THREADNUM<2
#define SIZEDNMPLACES DNMPLACES01
#endif
#if _THREADNUM==2
#define SIZEDNMPLACES DNMPLACES02
#endif
#if _THREADNUM==3
#define SIZEDNMPLACES DNMPLACES03
#endif
#if _THREADNUM==4
#define SIZEDNMPLACES DNMPLACES04
#endif
#if _THREADNUM==5
#define SIZEDNMPLACES DNMPLACES05
#endif
#if _THREADNUM==6
#define SIZEDNMPLACES DNMPLACES06
#endif

/*-----------------------------------------------*/

void irq_onasm (void);

/*-----------------------------------------------*/

