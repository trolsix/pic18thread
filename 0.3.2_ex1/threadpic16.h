/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/

#ifndef _threadpic_h
#define _threadpic_h

#include <stdint.h>

/*-----------------------------------------------*/

#if defined(SDCC) && defined (__SDCC_pic16)
#define _MYMODFUN_ __wparam
#else
#define _MYMODFUN_
#endif

/*-----------------------------------------------*/

#define THST_RUN  0x01
#define THST_STOP 0x02
#define THST_WAIT 0x03
#define THST_END  0xFE
#define THST_CLR  0xFF

/*-----------------------------------------------*/
/*   thread */
/*-----------------------------------------------*/

void irq_on(void);
void irq_off(void);
void irq_restore(void);
void initth ( void );
void thstop ( void );

//uint8_t startthvoid ( uint8_t (*thfun)(void) );
uint8_t startthvoidn ( uint8_t (*thfun)(void), uint8_t num );

//uint8_t threadrun ( uint8_t nth ) _MYMODFUN_;
//uint8_t threadrun ( uint8_t nth ) __naked;
//uint8_t threadrun ( uint8_t nth ) _MYMODFUN_ __naked;
uint8_t threadrun ( uint8_t nth ) _MYMODFUN_;
uint8_t threadstop ( uint8_t nth ) _MYMODFUN_;

/*-----------------------------------------------*/

#endif

/*-----------------------------------------------*/