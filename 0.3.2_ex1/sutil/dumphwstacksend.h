

#if DUMPSTACKHWIRQ == 1

if(1){
	uint8_t i, stmem, t[3];
	INTCON &= ~0x40;
	RSoutP ('\n');
	for(i=0;i<31;++i ){
		INTCON &= ~0x80;
		stmem = STKPTR;
		STKPTR = i;
		t[0] = TOSL;
		t[1] = TOSH;
		t[2] = TOSU;
		STKPTR = stmem;
		INTCON |= 0x80;
		sendhex (t[2]);
		sendhex (t[1]);
		sendhex (t[0]);
		RSoutP (' ');
	}
	RSoutP (10);
	INTCON |= 0x40;
}

#endif
