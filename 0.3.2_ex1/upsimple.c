/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/

/*-----------------------------------------------
       thread example
-----------------------------------------------*/

#include "Bheadb.h"

/*-----------------------------------------------*/

#include <stdio.h>
#include <stdint.h>

#include "nothing.h"
#include "threadpic16.h"
#include "threadpic16conf.h"
#include "semaphorespic16.h"
#include "mutexpic16.h"

#include "threadpic16stckconf.h"

/*-----------------------------------------------*/
/*    config   fuse  hardware uC                 */
/*-----------------------------------------------*/

#include "Bconf.h"

/*-----------------------------------------------*/

void setrs ( void);
void RSoutP (unsigned char a) _MYMODFUN_;
void RSoutB (unsigned char a) _MYMODFUN_;
void initport (void);

/*-----------------------------------------------*/

void RSouttbl ( unsigned char siz, const unsigned char *tbl ) __reentrant;

/*-----------------------------------------------*/

//void binbcd1 ( char * tbl, uint32_t a07 ) __reentrant;
uint8_t binbcd1 ( void );
void sendhex (unsigned char a) _MYMODFUN_;

/*-----------------------------------------------*/

sem_t sembcd;
sem_t semmain;

/*-----------------------------------------------*/

//uint16_t timer[2];
static char tbl[11];
volatile uint8_t thrend;
//uint16_t timthr;
//uint32_t timsum[2];

/*-----------------------------------------------*/
/*
#if DUMPSTACKH || DUMPSATCKS
uint8_t stack_dump[93];
#endif
*/
/*-----------------------------------------------*/

static uint16_t tickf1[3];

/*-----------------------------------------------*/

/*-----------------------------------------------*/
/* example */

extern uint8_t thactiv;
extern volatile uint8_t thstate[];
extern volatile uint16_t thsem[];

static uint8_t myfun1 ( void ) __naked {
	
	while(1) {
		uint8_t wskdata;
	
		//wskdata = 0;
		
		if(thactiv==1) { wskdata = 0; }
		else if(thactiv==3) { wskdata = 1; }
		else if(thactiv==4) { wskdata = 2; }
		else { RSoutP (thactiv+'0'); break;} //have not data for this	

		if ( !( tickf1[wskdata] & 0x0FFF ) ) {
			//irq off save stack in low irq
			//and make atomic for system
			INTCON &= ~0x40;
			RSoutP ('T');
			RSoutP (thactiv+'0');
			RSoutP ('\n');
			//RSoutP (' ');
			INTCON |= 0x40;
		}
		
		tickf1[wskdata] += 1;

		/* make some times something */
		for(wskdata=0;wskdata<180;++wskdata) {
			EEADR;
			EEDATA;
		}
		
		if(thrend) break;
	}
	
	INTCON &= ~0x40;
	RSoutP ('-');
	RSoutP (thactiv+'0');
	RSoutP ('-');
	INTCON |= 0x40;
	__asm
		movlw 0x00
		RETURN
	__endasm;
	//return 1;
}

/*-----------------------------------------------*/

void RSoutB (unsigned char a) _MYMODFUN_ {
	uint8_t i;
	
	for ( i=0x80; i; i >>= 1 ) {
		if(a&i) RSoutP ('1');
		else RSoutP ('0');
	}
	RSoutP (' ');
}

/*-----------------------------------------------*/
/* send stack with portion  50 */
#if DUMPSATCKS
void sendmem ( uint8_t * wsk ) {
	uint8_t i;
	
	INTCON &= ~0x80;
	for(i=0;i<50;++i ) {
		stack_dump[i] = *wsk;
		--wsk;
	}
	INTCON |= 0x80;
	
	//send
	//RSoutP (10);
	for(i=0;i<50;){
		sendhex (stack_dump[i]);
		RSoutP (' ');
		++i;
		if(0==(i&0x1F))RSoutP (10);
	}
	RSoutP (10);
}
#endif
/*-----------------------------------------------*/

extern uint32_t ul;
extern char * tbll;

#define BINASI(PARAM1,PARAM2) INTCON &= ~0x40; ul=PARAM2; tbll=PARAM1; \
rungetbcd()

/*-----------------------------------------------*/
/* call void funkcion is efective */

void waitsemmain ( void ) {
	sem_wait(&semmain);
}

void runbcd ( void ) {
	sem_post(&sembcd);
	INTCON|=0x40;
}

void rungetbcd ( void ) {
	sem_post(&sembcd);
	INTCON|=0x40;
	sem_wait(&semmain);
}

/*-----------------------------------------------*/
//const char tstop[10]   = "TH1 STOP\n";
//const char tstart[11]  = "TH1 START\n";
//const char t1eg[11]    = "TH1 EGAIN\n";

const char what[]    = "Example v 0.3.2\n\n";
const char t0start[9]  = "\n\nSTART\n";

//extern uint8_t thflag;

volatile unsigned char * wskchar;

int main ( void ) {
	
	static uint32_t tick;

	initport();
	setrs();
	
	TMR0L = 0;
	TMR1L = 0;
	TMR1H = 0;
	
	initth();

	CLRWDT();

	tickf1[0] = 0;
	tickf1[1] = 0;
	tickf1[2] = 0;
	tick = 0;

	setrs();
	irq_on();
	INTCON &= ~0x40;
	
	RSouttbl (0,t0start);
	RSouttbl (0,what);
	
	thrend = sem_init ( &semmain, 0 );
	thrend = sem_init ( &sembcd, 0 );
	INTCON &= ~0x40;
	
	while(1) { //chwilka
		CLRWDT();
		++tick;
		if(tick>50000){
			tick = 0;
			break;
		}
	}
	
	
	thrend = 0;
	
	tbl[0] = startthvoidn ( &myfun1, 1 );
	if(tbl[0]==1) RSoutP ('1');
	else RSoutP ('B');

	tbl[0] = startthvoidn ( &binbcd1, 2 );
	if(tbl[0]==2) RSoutP ('2');
	else  RSoutP ('B');
	
	tbl[0] = startthvoidn ( &myfun1, 3 );
	if(tbl[0]==3) RSoutP ('3');
	else  RSoutP ('B');
	
	tbl[0] = startthvoidn ( &myfun1, 4 );
	if(tbl[0]==4) RSoutP ('4');
	else  RSoutP ('B');

	RSoutP ('\n');
	
	while(1) {
		CLRWDT();
		#if DEBUG_ADRFSR1
		if(FSR1L!=(0xFF&THSTACADR00)) {
			INTCON &= ~0x40;
			RSoutP ('*');
			RSoutP ('0');
			RSoutP ('*');
			RSoutP ('\n');
			while(1);
		}
		#endif
		
		++tick;
	
		//dumpstackwithirq
		//a mamy amlo memeory a chcemy znac stos
		#define DUMPSTACKHWIRQ 0
		#define DUMPSTACKSFIRQ 0
		if ( (0x3FFF & tick) == 1) {
			#include "sutil/dumphwstacksend.h"
		}
		
		if ( (0x3FFF & tick) == 2) {
			#include "sutil/dumpsfstacksend.h"
		}
		
		if ( (0x3FFF & tick) == 10) {
			BINASI ( tbl, tick );
			INTCON &= ~0x40;
			RSoutP ('T'); RSoutP ('0'); RSoutP (' ');
			//RSoutP (mut1.val+'0');
			RSouttbl ( 10, tbl );
			RSoutP ('\n');
			INTCON |= 0x40;
			
			BINASI ( tbl, tickf1[0] );
			INTCON &= ~0x40;
			RSoutP ('Z'); RSoutP ('0'); RSoutP (' ');
			RSouttbl ( 10, tbl );
			RSoutP ('\n');
			INTCON |= 0x40;
			
			BINASI ( tbl, tickf1[1] );
			INTCON &= ~0x40;
			RSoutP ('Z'); RSoutP ('1'); RSoutP (' ');
			RSouttbl ( 10, tbl );
			RSoutP ('\n');
			INTCON |= 0x40;
			
			BINASI ( tbl, tickf1[2] );
			INTCON &= ~0x40;
			RSoutP ('Z'); RSoutP ('2'); RSoutP (' ');
			RSouttbl ( 10, tbl );
			RSoutP ('\n');
			INTCON |= 0x40;
		}
		
	}
	
//	while(1);
}

/*-----------------------------------------------*/

void sendhex (unsigned char a) _MYMODFUN_ {
	uint8_t tmp;
	
	tmp = a>>4;
	tmp &= 0x0F;
	if(tmp>9) tmp += 'a' - 10;
	else tmp += '0';
	RSoutP (tmp);
	
	tmp = a & 0x0F;
	if(tmp>9) tmp += 'a' - 10;
	else tmp += '0';
	RSoutP (tmp);
}


/*-----------------------------------------------*/

