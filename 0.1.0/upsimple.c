/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/
/*-----------------------------------------------

pic18f1220 thread example

-----------------------------------------------*/

#ifdef __SDCC_pic16
#include <pic16/pic18fregs.h>
#endif

/*-----------------------------------------------*/

#include <stdio.h>
#include <stdint.h>

/*-----------------------------------------------*/

#include "threadpic16.h"

/*-----------------------------------------------*/

#if defined(SDCC) && defined (__SDCC_pic16)
#define _MYMODFUN_ __wparam
#else
#define _MYMODFUN_
#endif

/*-----------------------------------------------*/
/* configuracje fuse i configi hardware uC */
/*-----------------------------------------------*/

#include "Bconf.h"

/*-----------------------------------------------*/

void CLRWDT(void);

/*-----------------------------------------------*/

int main ( void ) {
	
	//initport();
	//initth();
	CLRWDT();
	
	//thret = startthvoid ( &myfun1 );
	//irq_on();
	
	while(1) {
		CLRWDT();
	}

}
