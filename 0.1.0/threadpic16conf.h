
/*-----------------------------------------------*/
/* number threads */
#define _THREADNUM 2
/* how many register save */
#define NUMBREGSAVE 16
/* save tbl ragister or not */
#define SAVE_TBLCONT 1

/* dont change this if you dont know what do
this change thread priority
*/
#define THREAD_USE_LOW_PRIORITY 1

/*-----------------------------------------------*/

#define FUN_IRQ_THREAD_ALLOWED 1

/*-----------------------------------------------*/

#if SAVE_TBLCONT==1
#define _THNUMPARAM (NUMBREGSAVE+11)
#endif
#if SAVE_TBLCONT==0
#define _THNUMPARAM (NUMBREGSAVE+7)
#endif

#if _THREADNUM==2
#define THSTACKPOINT01 16
#define THSTACKPOINT02 31

#pragma stack 156 100
#define THSTACADR00 255
#define THSTACADR01 205
#define THSTACADR02 155
/*
#pragma stack 600 100
#define THSTACADR00 700
#define THSTACADR01 650
#define THSTACADR02 600*/

#endif
/* 30 bytes for each stack */
#if _THREADNUM==3
#define THSTACKPOINT01 11
#define THSTACKPOINT02 21
#pragma stack 300 150

#define THSTACADR00 450
#define THSTACADR01 400
#define THSTACADR02 350
#define THSTACADRFF 300

#endif

/*-----------------------------------------------*/

//#define END_TH_FUN() while(1)
#define END_TH_FUN() {while(1) {INTCONbits.TMR0IF=1;}}

/*-----------------------------------------------*/

void irq_onasm (void);

/*-----------------------------------------------*/
